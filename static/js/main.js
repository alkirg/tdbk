"use strict";

(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(['exports'], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports);
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports);
    global.bodyScrollLock = mod.exports;
  }
})(window, function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  function _toConsumableArray(arr) {
    if (Array.isArray(arr)) {
      for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) {
        arr2[i] = arr[i];
      }

      return arr2;
    } else {
      return Array.from(arr);
    }
  } // Older browsers don't support event options, feature detect it.
  // Adopted and modified solution from Bohdan Didukh (2017)
  // https://stackoverflow.com/questions/41594997/ios-10-safari-prevent-scrolling-behind-a-fixed-overlay-and-maintain-scroll-posi


  var hasPassiveEvents = false;

  if (typeof window !== 'undefined') {
    var passiveTestOptions = {
      get passive() {
        hasPassiveEvents = true;
        return undefined;
      }

    };
    window.addEventListener('testPassive', null, passiveTestOptions);
    window.removeEventListener('testPassive', null, passiveTestOptions);
  }

  var isIosDevice = typeof window !== 'undefined' && window.navigator && window.navigator.platform && /iP(ad|hone|od)/.test(window.navigator.platform);
  var locks = [];
  var documentListenerAdded = false;
  var initialClientY = -1;
  var previousBodyOverflowSetting = void 0;
  var previousBodyPaddingRight = void 0; // returns true if `el` should be allowed to receive touchmove events

  var allowTouchMove = function allowTouchMove(el) {
    return locks.some(function (lock) {
      if (lock.options.allowTouchMove && lock.options.allowTouchMove(el)) {
        return true;
      }

      return false;
    });
  };

  var preventDefault = function preventDefault(rawEvent) {
    var e = rawEvent || window.event; // For the case whereby consumers adds a touchmove event listener to document.
    // Recall that we do document.addEventListener('touchmove', preventDefault, { passive: false })
    // in disableBodyScroll - so if we provide this opportunity to allowTouchMove, then
    // the touchmove event on document will break.

    if (allowTouchMove(e.target)) {
      return true;
    } // Do not prevent if the event has more than one touch (usually meaning this is a multi touch gesture like pinch to zoom)


    if (e.touches.length > 1) return true;
    if (e.preventDefault) e.preventDefault();
    return false;
  };

  var setOverflowHidden = function setOverflowHidden(options) {
    // Setting overflow on body/documentElement synchronously in Desktop Safari slows down
    // the responsiveness for some reason. Setting within a setTimeout fixes this.
    setTimeout(function () {
      // If previousBodyPaddingRight is already set, don't set it again.
      if (previousBodyPaddingRight === undefined) {
        var _reserveScrollBarGap = !!options && options.reserveScrollBarGap === true;

        var scrollBarGap = window.innerWidth - document.documentElement.clientWidth;

        if (_reserveScrollBarGap && scrollBarGap > 0) {
          previousBodyPaddingRight = document.body.style.paddingRight;
          document.body.style.paddingRight = scrollBarGap + 'px';
        }
      } // If previousBodyOverflowSetting is already set, don't set it again.


      if (previousBodyOverflowSetting === undefined) {
        previousBodyOverflowSetting = document.body.style.overflow;
        document.body.style.overflow = 'hidden';
      }
    });
  };

  var restoreOverflowSetting = function restoreOverflowSetting() {
    // Setting overflow on body/documentElement synchronously in Desktop Safari slows down
    // the responsiveness for some reason. Setting within a setTimeout fixes this.
    setTimeout(function () {
      if (previousBodyPaddingRight !== undefined) {
        document.body.style.paddingRight = previousBodyPaddingRight; // Restore previousBodyPaddingRight to undefined so setOverflowHidden knows it
        // can be set again.

        previousBodyPaddingRight = undefined;
      }

      if (previousBodyOverflowSetting !== undefined) {
        document.body.style.overflow = previousBodyOverflowSetting; // Restore previousBodyOverflowSetting to undefined
        // so setOverflowHidden knows it can be set again.

        previousBodyOverflowSetting = undefined;
      }
    });
  }; // https://developer.mozilla.org/en-US/docs/Web/API/Element/scrollHeight#Problems_and_solutions


  var isTargetElementTotallyScrolled = function isTargetElementTotallyScrolled(targetElement) {
    return targetElement ? targetElement.scrollHeight - targetElement.scrollTop <= targetElement.clientHeight : false;
  };

  var handleScroll = function handleScroll(event, targetElement) {
    var clientY = event.targetTouches[0].clientY - initialClientY;

    if (allowTouchMove(event.target)) {
      return false;
    }

    if (targetElement && targetElement.scrollTop === 0 && clientY > 0) {
      // element is at the top of its scroll
      return preventDefault(event);
    }

    if (isTargetElementTotallyScrolled(targetElement) && clientY < 0) {
      // element is at the top of its scroll
      return preventDefault(event);
    }

    event.stopPropagation();
    return true;
  };

  var disableBodyScroll = exports.disableBodyScroll = function disableBodyScroll(targetElement, options) {
    if (isIosDevice) {
      // targetElement must be provided, and disableBodyScroll must not have been
      // called on this targetElement before.
      if (!targetElement) {
        // eslint-disable-next-line no-console
        console.error('disableBodyScroll unsuccessful - targetElement must be provided when calling disableBodyScroll on IOS devices.');
        return;
      }

      if (targetElement && !locks.some(function (lock) {
        return lock.targetElement === targetElement;
      })) {
        var lock = {
          targetElement: targetElement,
          options: options || {}
        };
        locks = [].concat(_toConsumableArray(locks), [lock]);

        targetElement.ontouchstart = function (event) {
          if (event.targetTouches.length === 1) {
            // detect single touch
            initialClientY = event.targetTouches[0].clientY;
          }
        };

        targetElement.ontouchmove = function (event) {
          if (event.targetTouches.length === 1) {
            // detect single touch
            handleScroll(event, targetElement);
          }
        };

        if (!documentListenerAdded) {
          document.addEventListener('touchmove', preventDefault, hasPassiveEvents ? {
            passive: false
          } : undefined);
          documentListenerAdded = true;
        }
      }
    } else {
      setOverflowHidden(options);
      var _lock = {
        targetElement: targetElement,
        options: options || {}
      };
      locks = [].concat(_toConsumableArray(locks), [_lock]);
    }
  };

  var clearAllBodyScrollLocks = exports.clearAllBodyScrollLocks = function clearAllBodyScrollLocks() {
    if (isIosDevice) {
      // Clear all locks ontouchstart/ontouchmove handlers, and the references
      console.log(locks);
      locks.forEach(function (lock) {
        lock.targetElement.ontouchstart = null;
        lock.targetElement.ontouchmove = null;
      });

      if (documentListenerAdded) {
        document.removeEventListener('touchmove', preventDefault, hasPassiveEvents ? {
          passive: false
        } : undefined);
        documentListenerAdded = false;
      }

      locks = []; // Reset initial clientY

      initialClientY = -1;
    } else {
      restoreOverflowSetting();
      locks = [];
    }
  };

  var enableBodyScroll = exports.enableBodyScroll = function enableBodyScroll(targetElement) {
    if (isIosDevice) {
      if (!targetElement) {
        // eslint-disable-next-line no-console
        console.error('enableBodyScroll unsuccessful - targetElement must be provided when calling enableBodyScroll on IOS devices.');
        return;
      }

      targetElement.ontouchstart = null;
      targetElement.ontouchmove = null;
      locks = locks.filter(function (lock) {
        return lock.targetElement !== targetElement;
      });

      if (documentListenerAdded && locks.length === 0) {
        document.removeEventListener('touchmove', preventDefault, hasPassiveEvents ? {
          passive: false
        } : undefined);
        documentListenerAdded = false;
      }
    } else if (locks.length === 1 && locks[0].targetElement === targetElement) {
      restoreOverflowSetting();
      locks = [];
    } else {
      locks = locks.filter(function (lock) {
        return lock.targetElement !== targetElement;
      });
    }
  };
});
"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/*!
 * Flickity PACKAGED v2.2.0
 * Touch, responsive, flickable carousels
 *
 * Licensed GPLv3 for open source use
 * or Flickity Commercial License for commercial use
 *
 * https://flickity.metafizzy.co
 * Copyright 2015-2018 Metafizzy
 */
!function (e, i) {
  "function" == typeof define && define.amd ? define("jquery-bridget/jquery-bridget", ["jquery"], function (t) {
    return i(e, t);
  }) : "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports ? module.exports = i(e, require("jquery")) : e.jQueryBridget = i(e, e.jQuery);
}(window, function (t, e) {
  "use strict";

  var d = Array.prototype.slice,
      i = t.console,
      u = void 0 === i ? function () {} : function (t) {
    i.error(t);
  };

  function n(h, s, c) {
    (c = c || e || t.jQuery) && (s.prototype.option || (s.prototype.option = function (t) {
      c.isPlainObject(t) && (this.options = c.extend(!0, this.options, t));
    }), c.fn[h] = function (t) {
      if ("string" != typeof t) return n = t, this.each(function (t, e) {
        var i = c.data(e, h);
        i ? (i.option(n), i._init()) : (i = new s(e, n), c.data(e, h, i));
      }), this;
      var e,
          o,
          r,
          a,
          l,
          n,
          i = d.call(arguments, 1);
      return r = i, l = "$()." + h + '("' + (o = t) + '")', (e = this).each(function (t, e) {
        var i = c.data(e, h);

        if (i) {
          var n = i[o];

          if (n && "_" != o.charAt(0)) {
            var s = n.apply(i, r);
            a = void 0 === a ? s : a;
          } else u(l + " is not a valid method");
        } else u(h + " not initialized. Cannot call methods, i.e. " + l);
      }), void 0 !== a ? a : e;
    }, o(c));
  }

  function o(t) {
    !t || t && t.bridget || (t.bridget = n);
  }

  return o(e || t.jQuery), n;
}), function (t, e) {
  "function" == typeof define && define.amd ? define("ev-emitter/ev-emitter", e) : "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports ? module.exports = e() : t.EvEmitter = e();
}("undefined" != typeof window ? window : void 0, function () {
  function t() {}

  var e = t.prototype;
  return e.on = function (t, e) {
    if (t && e) {
      var i = this._events = this._events || {},
          n = i[t] = i[t] || [];
      return -1 == n.indexOf(e) && n.push(e), this;
    }
  }, e.once = function (t, e) {
    if (t && e) {
      this.on(t, e);
      var i = this._onceEvents = this._onceEvents || {};
      return (i[t] = i[t] || {})[e] = !0, this;
    }
  }, e.off = function (t, e) {
    var i = this._events && this._events[t];

    if (i && i.length) {
      var n = i.indexOf(e);
      return -1 != n && i.splice(n, 1), this;
    }
  }, e.emitEvent = function (t, e) {
    var i = this._events && this._events[t];

    if (i && i.length) {
      i = i.slice(0), e = e || [];

      for (var n = this._onceEvents && this._onceEvents[t], s = 0; s < i.length; s++) {
        var o = i[s];
        n && n[o] && (this.off(t, o), delete n[o]), o.apply(this, e);
      }

      return this;
    }
  }, e.allOff = function () {
    delete this._events, delete this._onceEvents;
  }, t;
}), function (t, e) {
  "function" == typeof define && define.amd ? define("get-size/get-size", e) : "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports ? module.exports = e() : t.getSize = e();
}(window, function () {
  "use strict";

  function m(t) {
    var e = parseFloat(t);
    return -1 == t.indexOf("%") && !isNaN(e) && e;
  }

  var i = "undefined" == typeof console ? function () {} : function (t) {
    console.error(t);
  },
      y = ["paddingLeft", "paddingRight", "paddingTop", "paddingBottom", "marginLeft", "marginRight", "marginTop", "marginBottom", "borderLeftWidth", "borderRightWidth", "borderTopWidth", "borderBottomWidth"],
      b = y.length;

  function E(t) {
    var e = getComputedStyle(t);
    return e || i("Style returned " + e + ". Are you running this code in a hidden iframe on Firefox? See https://bit.ly/getsizebug1"), e;
  }

  var S,
      C = !1;

  function x(t) {
    if (function () {
      if (!C) {
        C = !0;
        var t = document.createElement("div");
        t.style.width = "200px", t.style.padding = "1px 2px 3px 4px", t.style.borderStyle = "solid", t.style.borderWidth = "1px 2px 3px 4px", t.style.boxSizing = "border-box";
        var e = document.body || document.documentElement;
        e.appendChild(t);
        var i = E(t);
        S = 200 == Math.round(m(i.width)), x.isBoxSizeOuter = S, e.removeChild(t);
      }
    }(), "string" == typeof t && (t = document.querySelector(t)), t && "object" == _typeof(t) && t.nodeType) {
      var e = E(t);
      if ("none" == e.display) return function () {
        for (var t = {
          width: 0,
          height: 0,
          innerWidth: 0,
          innerHeight: 0,
          outerWidth: 0,
          outerHeight: 0
        }, e = 0; e < b; e++) {
          t[y[e]] = 0;
        }

        return t;
      }();
      var i = {};
      i.width = t.offsetWidth, i.height = t.offsetHeight;

      for (var n = i.isBorderBox = "border-box" == e.boxSizing, s = 0; s < b; s++) {
        var o = y[s],
            r = e[o],
            a = parseFloat(r);
        i[o] = isNaN(a) ? 0 : a;
      }

      var l = i.paddingLeft + i.paddingRight,
          h = i.paddingTop + i.paddingBottom,
          c = i.marginLeft + i.marginRight,
          d = i.marginTop + i.marginBottom,
          u = i.borderLeftWidth + i.borderRightWidth,
          f = i.borderTopWidth + i.borderBottomWidth,
          p = n && S,
          g = m(e.width);
      !1 !== g && (i.width = g + (p ? 0 : l + u));
      var v = m(e.height);
      return !1 !== v && (i.height = v + (p ? 0 : h + f)), i.innerWidth = i.width - (l + u), i.innerHeight = i.height - (h + f), i.outerWidth = i.width + c, i.outerHeight = i.height + d, i;
    }
  }

  return x;
}), function (t, e) {
  "use strict";

  "function" == typeof define && define.amd ? define("desandro-matches-selector/matches-selector", e) : "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports ? module.exports = e() : t.matchesSelector = e();
}(window, function () {
  "use strict";

  var i = function () {
    var t = window.Element.prototype;
    if (t.matches) return "matches";
    if (t.matchesSelector) return "matchesSelector";

    for (var e = ["webkit", "moz", "ms", "o"], i = 0; i < e.length; i++) {
      var n = e[i] + "MatchesSelector";
      if (t[n]) return n;
    }
  }();

  return function (t, e) {
    return t[i](e);
  };
}), function (e, i) {
  "function" == typeof define && define.amd ? define("fizzy-ui-utils/utils", ["desandro-matches-selector/matches-selector"], function (t) {
    return i(e, t);
  }) : "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports ? module.exports = i(e, require("desandro-matches-selector")) : e.fizzyUIUtils = i(e, e.matchesSelector);
}(window, function (h, o) {
  var c = {
    extend: function extend(t, e) {
      for (var i in e) {
        t[i] = e[i];
      }

      return t;
    },
    modulo: function modulo(t, e) {
      return (t % e + e) % e;
    }
  },
      e = Array.prototype.slice;
  c.makeArray = function (t) {
    return Array.isArray(t) ? t : null == t ? [] : "object" == _typeof(t) && "number" == typeof t.length ? e.call(t) : [t];
  }, c.removeFrom = function (t, e) {
    var i = t.indexOf(e);
    -1 != i && t.splice(i, 1);
  }, c.getParent = function (t, e) {
    for (; t.parentNode && t != document.body;) {
      if (t = t.parentNode, o(t, e)) return t;
    }
  }, c.getQueryElement = function (t) {
    return "string" == typeof t ? document.querySelector(t) : t;
  }, c.handleEvent = function (t) {
    var e = "on" + t.type;
    this[e] && this[e](t);
  }, c.filterFindElements = function (t, n) {
    t = c.makeArray(t);
    var s = [];
    return t.forEach(function (t) {
      if (t instanceof HTMLElement) if (n) {
        o(t, n) && s.push(t);

        for (var e = t.querySelectorAll(n), i = 0; i < e.length; i++) {
          s.push(e[i]);
        }
      } else s.push(t);
    }), s;
  }, c.debounceMethod = function (t, e, n) {
    n = n || 100;
    var s = t.prototype[e],
        o = e + "Timeout";

    t.prototype[e] = function () {
      var t = this[o];
      clearTimeout(t);
      var e = arguments,
          i = this;
      this[o] = setTimeout(function () {
        s.apply(i, e), delete i[o];
      }, n);
    };
  }, c.docReady = function (t) {
    var e = document.readyState;
    "complete" == e || "interactive" == e ? setTimeout(t) : document.addEventListener("DOMContentLoaded", t);
  }, c.toDashed = function (t) {
    return t.replace(/(.)([A-Z])/g, function (t, e, i) {
      return e + "-" + i;
    }).toLowerCase();
  };
  var d = h.console;
  return c.htmlInit = function (a, l) {
    c.docReady(function () {
      var t = c.toDashed(l),
          s = "data-" + t,
          e = document.querySelectorAll("[" + s + "]"),
          i = document.querySelectorAll(".js-" + t),
          n = c.makeArray(e).concat(c.makeArray(i)),
          o = s + "-options",
          r = h.jQuery;
      n.forEach(function (e) {
        var t,
            i = e.getAttribute(s) || e.getAttribute(o);

        try {
          t = i && JSON.parse(i);
        } catch (t) {
          return void (d && d.error("Error parsing " + s + " on " + e.className + ": " + t));
        }

        var n = new a(e, t);
        r && r.data(e, l, n);
      });
    });
  }, c;
}), function (e, i) {
  "function" == typeof define && define.amd ? define("flickity/js/cell", ["get-size/get-size"], function (t) {
    return i(e, t);
  }) : "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports ? module.exports = i(e, require("get-size")) : (e.Flickity = e.Flickity || {}, e.Flickity.Cell = i(e, e.getSize));
}(window, function (t, e) {
  function i(t, e) {
    this.element = t, this.parent = e, this.create();
  }

  var n = i.prototype;
  return n.create = function () {
    this.element.style.position = "absolute", this.element.setAttribute("aria-hidden", "true"), this.x = 0, this.shift = 0;
  }, n.destroy = function () {
    this.unselect(), this.element.style.position = "";
    var t = this.parent.originSide;
    this.element.style[t] = "";
  }, n.getSize = function () {
    this.size = e(this.element);
  }, n.setPosition = function (t) {
    this.x = t, this.updateTarget(), this.renderPosition(t);
  }, n.updateTarget = n.setDefaultTarget = function () {
    var t = "left" == this.parent.originSide ? "marginLeft" : "marginRight";
    this.target = this.x + this.size[t] + this.size.width * this.parent.cellAlign;
  }, n.renderPosition = function (t) {
    var e = this.parent.originSide;
    this.element.style[e] = this.parent.getPositionValue(t);
  }, n.select = function () {
    this.element.classList.add("is-selected"), this.element.removeAttribute("aria-hidden");
  }, n.unselect = function () {
    this.element.classList.remove("is-selected"), this.element.setAttribute("aria-hidden", "true");
  }, n.wrapShift = function (t) {
    this.shift = t, this.renderPosition(this.x + this.parent.slideableWidth * t);
  }, n.remove = function () {
    this.element.parentNode.removeChild(this.element);
  }, i;
}), function (t, e) {
  "function" == typeof define && define.amd ? define("flickity/js/slide", e) : "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports ? module.exports = e() : (t.Flickity = t.Flickity || {}, t.Flickity.Slide = e());
}(window, function () {
  "use strict";

  function t(t) {
    this.parent = t, this.isOriginLeft = "left" == t.originSide, this.cells = [], this.outerWidth = 0, this.height = 0;
  }

  var e = t.prototype;
  return e.addCell = function (t) {
    if (this.cells.push(t), this.outerWidth += t.size.outerWidth, this.height = Math.max(t.size.outerHeight, this.height), 1 == this.cells.length) {
      this.x = t.x;
      var e = this.isOriginLeft ? "marginLeft" : "marginRight";
      this.firstMargin = t.size[e];
    }
  }, e.updateTarget = function () {
    var t = this.isOriginLeft ? "marginRight" : "marginLeft",
        e = this.getLastCell(),
        i = e ? e.size[t] : 0,
        n = this.outerWidth - (this.firstMargin + i);
    this.target = this.x + this.firstMargin + n * this.parent.cellAlign;
  }, e.getLastCell = function () {
    return this.cells[this.cells.length - 1];
  }, e.select = function () {
    this.cells.forEach(function (t) {
      t.select();
    });
  }, e.unselect = function () {
    this.cells.forEach(function (t) {
      t.unselect();
    });
  }, e.getCellElements = function () {
    return this.cells.map(function (t) {
      return t.element;
    });
  }, t;
}), function (e, i) {
  "function" == typeof define && define.amd ? define("flickity/js/animate", ["fizzy-ui-utils/utils"], function (t) {
    return i(e, t);
  }) : "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports ? module.exports = i(e, require("fizzy-ui-utils")) : (e.Flickity = e.Flickity || {}, e.Flickity.animatePrototype = i(e, e.fizzyUIUtils));
}(window, function (t, e) {
  var i = {
    startAnimation: function startAnimation() {
      this.isAnimating || (this.isAnimating = !0, this.restingFrames = 0, this.animate());
    },
    animate: function animate() {
      this.applyDragForce(), this.applySelectedAttraction();
      var t = this.x;

      if (this.integratePhysics(), this.positionSlider(), this.settle(t), this.isAnimating) {
        var e = this;
        requestAnimationFrame(function () {
          e.animate();
        });
      }
    },
    positionSlider: function positionSlider() {
      var t = this.x;
      this.options.wrapAround && 1 < this.cells.length && (t = e.modulo(t, this.slideableWidth), t -= this.slideableWidth, this.shiftWrapCells(t)), this.setTranslateX(t, this.isAnimating), this.dispatchScrollEvent();
    },
    setTranslateX: function setTranslateX(t, e) {
      t += this.cursorPosition, t = this.options.rightToLeft ? -t : t;
      var i = this.getPositionValue(t);
      this.slider.style.transform = e ? "translate3d(" + i + ",0,0)" : "translateX(" + i + ")";
    },
    dispatchScrollEvent: function dispatchScrollEvent() {
      var t = this.slides[0];

      if (t) {
        var e = -this.x - t.target,
            i = e / this.slidesWidth;
        this.dispatchEvent("scroll", null, [i, e]);
      }
    },
    positionSliderAtSelected: function positionSliderAtSelected() {
      this.cells.length && (this.x = -this.selectedSlide.target, this.velocity = 0, this.positionSlider());
    },
    getPositionValue: function getPositionValue(t) {
      return this.options.percentPosition ? .01 * Math.round(t / this.size.innerWidth * 1e4) + "%" : Math.round(t) + "px";
    },
    settle: function settle(t) {
      this.isPointerDown || Math.round(100 * this.x) != Math.round(100 * t) || this.restingFrames++, 2 < this.restingFrames && (this.isAnimating = !1, delete this.isFreeScrolling, this.positionSlider(), this.dispatchEvent("settle", null, [this.selectedIndex]));
    },
    shiftWrapCells: function shiftWrapCells(t) {
      var e = this.cursorPosition + t;

      this._shiftCells(this.beforeShiftCells, e, -1);

      var i = this.size.innerWidth - (t + this.slideableWidth + this.cursorPosition);

      this._shiftCells(this.afterShiftCells, i, 1);
    },
    _shiftCells: function _shiftCells(t, e, i) {
      for (var n = 0; n < t.length; n++) {
        var s = t[n],
            o = 0 < e ? i : 0;
        s.wrapShift(o), e -= s.size.outerWidth;
      }
    },
    _unshiftCells: function _unshiftCells(t) {
      if (t && t.length) for (var e = 0; e < t.length; e++) {
        t[e].wrapShift(0);
      }
    },
    integratePhysics: function integratePhysics() {
      this.x += this.velocity, this.velocity *= this.getFrictionFactor();
    },
    applyForce: function applyForce(t) {
      this.velocity += t;
    },
    getFrictionFactor: function getFrictionFactor() {
      return 1 - this.options[this.isFreeScrolling ? "freeScrollFriction" : "friction"];
    },
    getRestingPosition: function getRestingPosition() {
      return this.x + this.velocity / (1 - this.getFrictionFactor());
    },
    applyDragForce: function applyDragForce() {
      if (this.isDraggable && this.isPointerDown) {
        var t = this.dragX - this.x - this.velocity;
        this.applyForce(t);
      }
    },
    applySelectedAttraction: function applySelectedAttraction() {
      if (!(this.isDraggable && this.isPointerDown) && !this.isFreeScrolling && this.slides.length) {
        var t = (-1 * this.selectedSlide.target - this.x) * this.options.selectedAttraction;
        this.applyForce(t);
      }
    }
  };
  return i;
}), function (r, a) {
  if ("function" == typeof define && define.amd) define("flickity/js/flickity", ["ev-emitter/ev-emitter", "get-size/get-size", "fizzy-ui-utils/utils", "./cell", "./slide", "./animate"], function (t, e, i, n, s, o) {
    return a(r, t, e, i, n, s, o);
  });else if ("object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports) module.exports = a(r, require("ev-emitter"), require("get-size"), require("fizzy-ui-utils"), require("./cell"), require("./slide"), require("./animate"));else {
    var t = r.Flickity;
    r.Flickity = a(r, r.EvEmitter, r.getSize, r.fizzyUIUtils, t.Cell, t.Slide, t.animatePrototype);
  }
}(window, function (n, t, e, a, i, r, s) {
  var l = n.jQuery,
      o = n.getComputedStyle,
      h = n.console;

  function c(t, e) {
    for (t = a.makeArray(t); t.length;) {
      e.appendChild(t.shift());
    }
  }

  var d = 0,
      u = {};

  function f(t, e) {
    var i = a.getQueryElement(t);

    if (i) {
      if (this.element = i, this.element.flickityGUID) {
        var n = u[this.element.flickityGUID];
        return n.option(e), n;
      }

      l && (this.$element = l(this.element)), this.options = a.extend({}, this.constructor.defaults), this.option(e), this._create();
    } else h && h.error("Bad element for Flickity: " + (i || t));
  }

  f.defaults = {
    accessibility: !0,
    cellAlign: "center",
    freeScrollFriction: .075,
    friction: .28,
    namespaceJQueryEvents: !0,
    percentPosition: !0,
    resize: !0,
    selectedAttraction: .025,
    setGallerySize: !0
  }, f.createMethods = [];
  var p = f.prototype;
  a.extend(p, t.prototype), p._create = function () {
    var t = this.guid = ++d;

    for (var e in this.element.flickityGUID = t, (u[t] = this).selectedIndex = 0, this.restingFrames = 0, this.x = 0, this.velocity = 0, this.originSide = this.options.rightToLeft ? "right" : "left", this.viewport = document.createElement("div"), this.viewport.className = "flickity-viewport", this._createSlider(), (this.options.resize || this.options.watchCSS) && n.addEventListener("resize", this), this.options.on) {
      var i = this.options.on[e];
      this.on(e, i);
    }

    f.createMethods.forEach(function (t) {
      this[t]();
    }, this), this.options.watchCSS ? this.watchCSS() : this.activate();
  }, p.option = function (t) {
    a.extend(this.options, t);
  }, p.activate = function () {
    this.isActive || (this.isActive = !0, this.element.classList.add("flickity-enabled"), this.options.rightToLeft && this.element.classList.add("flickity-rtl"), this.getSize(), c(this._filterFindCellElements(this.element.children), this.slider), this.viewport.appendChild(this.slider), this.element.appendChild(this.viewport), this.reloadCells(), this.options.accessibility && (this.element.tabIndex = 0, this.element.addEventListener("keydown", this)), this.emitEvent("activate"), this.selectInitialIndex(), this.isInitActivated = !0, this.dispatchEvent("ready"));
  }, p._createSlider = function () {
    var t = document.createElement("div");
    t.className = "flickity-slider", t.style[this.originSide] = 0, this.slider = t;
  }, p._filterFindCellElements = function (t) {
    return a.filterFindElements(t, this.options.cellSelector);
  }, p.reloadCells = function () {
    this.cells = this._makeCells(this.slider.children), this.positionCells(), this._getWrapShiftCells(), this.setGallerySize();
  }, p._makeCells = function (t) {
    return this._filterFindCellElements(t).map(function (t) {
      return new i(t, this);
    }, this);
  }, p.getLastCell = function () {
    return this.cells[this.cells.length - 1];
  }, p.getLastSlide = function () {
    return this.slides[this.slides.length - 1];
  }, p.positionCells = function () {
    this._sizeCells(this.cells), this._positionCells(0);
  }, p._positionCells = function (t) {
    t = t || 0, this.maxCellHeight = t && this.maxCellHeight || 0;
    var e = 0;

    if (0 < t) {
      var i = this.cells[t - 1];
      e = i.x + i.size.outerWidth;
    }

    for (var n = this.cells.length, s = t; s < n; s++) {
      var o = this.cells[s];
      o.setPosition(e), e += o.size.outerWidth, this.maxCellHeight = Math.max(o.size.outerHeight, this.maxCellHeight);
    }

    this.slideableWidth = e, this.updateSlides(), this._containSlides(), this.slidesWidth = n ? this.getLastSlide().target - this.slides[0].target : 0;
  }, p._sizeCells = function (t) {
    t.forEach(function (t) {
      t.getSize();
    });
  }, p.updateSlides = function () {
    if (this.slides = [], this.cells.length) {
      var n = new r(this);
      this.slides.push(n);

      var s = "left" == this.originSide ? "marginRight" : "marginLeft",
          o = this._getCanCellFit();

      this.cells.forEach(function (t, e) {
        if (n.cells.length) {
          var i = n.outerWidth - n.firstMargin + (t.size.outerWidth - t.size[s]);
          o.call(this, e, i) || (n.updateTarget(), n = new r(this), this.slides.push(n)), n.addCell(t);
        } else n.addCell(t);
      }, this), n.updateTarget(), this.updateSelectedSlide();
    }
  }, p._getCanCellFit = function () {
    var t = this.options.groupCells;
    if (!t) return function () {
      return !1;
    };

    if ("number" == typeof t) {
      var e = parseInt(t, 10);
      return function (t) {
        return t % e != 0;
      };
    }

    var i = "string" == typeof t && t.match(/^(\d+)%$/),
        n = i ? parseInt(i[1], 10) / 100 : 1;
    return function (t, e) {
      return e <= (this.size.innerWidth + 1) * n;
    };
  }, p._init = p.reposition = function () {
    this.positionCells(), this.positionSliderAtSelected();
  }, p.getSize = function () {
    this.size = e(this.element), this.setCellAlign(), this.cursorPosition = this.size.innerWidth * this.cellAlign;
  };
  var g = {
    center: {
      left: .5,
      right: .5
    },
    left: {
      left: 0,
      right: 1
    },
    right: {
      right: 0,
      left: 1
    }
  };
  return p.setCellAlign = function () {
    var t = g[this.options.cellAlign];
    this.cellAlign = t ? t[this.originSide] : this.options.cellAlign;
  }, p.setGallerySize = function () {
    if (this.options.setGallerySize) {
      var t = this.options.adaptiveHeight && this.selectedSlide ? this.selectedSlide.height : this.maxCellHeight;
      this.viewport.style.height = t + "px";
    }
  }, p._getWrapShiftCells = function () {
    if (this.options.wrapAround) {
      this._unshiftCells(this.beforeShiftCells), this._unshiftCells(this.afterShiftCells);
      var t = this.cursorPosition,
          e = this.cells.length - 1;
      this.beforeShiftCells = this._getGapCells(t, e, -1), t = this.size.innerWidth - this.cursorPosition, this.afterShiftCells = this._getGapCells(t, 0, 1);
    }
  }, p._getGapCells = function (t, e, i) {
    for (var n = []; 0 < t;) {
      var s = this.cells[e];
      if (!s) break;
      n.push(s), e += i, t -= s.size.outerWidth;
    }

    return n;
  }, p._containSlides = function () {
    if (this.options.contain && !this.options.wrapAround && this.cells.length) {
      var t = this.options.rightToLeft,
          e = t ? "marginRight" : "marginLeft",
          i = t ? "marginLeft" : "marginRight",
          n = this.slideableWidth - this.getLastCell().size[i],
          s = n < this.size.innerWidth,
          o = this.cursorPosition + this.cells[0].size[e],
          r = n - this.size.innerWidth * (1 - this.cellAlign);
      this.slides.forEach(function (t) {
        t.target = s ? n * this.cellAlign : (t.target = Math.max(t.target, o), Math.min(t.target, r));
      }, this);
    }
  }, p.dispatchEvent = function (t, e, i) {
    var n = e ? [e].concat(i) : i;

    if (this.emitEvent(t, n), l && this.$element) {
      var s = t += this.options.namespaceJQueryEvents ? ".flickity" : "";

      if (e) {
        var o = l.Event(e);
        o.type = t, s = o;
      }

      this.$element.trigger(s, i);
    }
  }, p.select = function (t, e, i) {
    if (this.isActive && (t = parseInt(t, 10), this._wrapSelect(t), (this.options.wrapAround || e) && (t = a.modulo(t, this.slides.length)), this.slides[t])) {
      var n = this.selectedIndex;
      this.selectedIndex = t, this.updateSelectedSlide(), i ? this.positionSliderAtSelected() : this.startAnimation(), this.options.adaptiveHeight && this.setGallerySize(), this.dispatchEvent("select", null, [t]), t != n && this.dispatchEvent("change", null, [t]), this.dispatchEvent("cellSelect");
    }
  }, p._wrapSelect = function (t) {
    var e = this.slides.length;
    if (!(this.options.wrapAround && 1 < e)) return t;
    var i = a.modulo(t, e),
        n = Math.abs(i - this.selectedIndex),
        s = Math.abs(i + e - this.selectedIndex),
        o = Math.abs(i - e - this.selectedIndex);
    !this.isDragSelect && s < n ? t += e : !this.isDragSelect && o < n && (t -= e), t < 0 ? this.x -= this.slideableWidth : e <= t && (this.x += this.slideableWidth);
  }, p.previous = function (t, e) {
    this.select(this.selectedIndex - 1, t, e);
  }, p.next = function (t, e) {
    this.select(this.selectedIndex + 1, t, e);
  }, p.updateSelectedSlide = function () {
    var t = this.slides[this.selectedIndex];
    t && (this.unselectSelectedSlide(), (this.selectedSlide = t).select(), this.selectedCells = t.cells, this.selectedElements = t.getCellElements(), this.selectedCell = t.cells[0], this.selectedElement = this.selectedElements[0]);
  }, p.unselectSelectedSlide = function () {
    this.selectedSlide && this.selectedSlide.unselect();
  }, p.selectInitialIndex = function () {
    var t = this.options.initialIndex;
    if (this.isInitActivated) this.select(this.selectedIndex, !1, !0);else {
      if (t && "string" == typeof t) if (this.queryCell(t)) return void this.selectCell(t, !1, !0);
      var e = 0;
      t && this.slides[t] && (e = t), this.select(e, !1, !0);
    }
  }, p.selectCell = function (t, e, i) {
    var n = this.queryCell(t);

    if (n) {
      var s = this.getCellSlideIndex(n);
      this.select(s, e, i);
    }
  }, p.getCellSlideIndex = function (t) {
    for (var e = 0; e < this.slides.length; e++) {
      if (-1 != this.slides[e].cells.indexOf(t)) return e;
    }
  }, p.getCell = function (t) {
    for (var e = 0; e < this.cells.length; e++) {
      var i = this.cells[e];
      if (i.element == t) return i;
    }
  }, p.getCells = function (t) {
    t = a.makeArray(t);
    var i = [];
    return t.forEach(function (t) {
      var e = this.getCell(t);
      e && i.push(e);
    }, this), i;
  }, p.getCellElements = function () {
    return this.cells.map(function (t) {
      return t.element;
    });
  }, p.getParentCell = function (t) {
    var e = this.getCell(t);
    return e || (t = a.getParent(t, ".flickity-slider > *"), this.getCell(t));
  }, p.getAdjacentCellElements = function (t, e) {
    if (!t) return this.selectedSlide.getCellElements();
    e = void 0 === e ? this.selectedIndex : e;
    var i = this.slides.length;
    if (i <= 1 + 2 * t) return this.getCellElements();

    for (var n = [], s = e - t; s <= e + t; s++) {
      var o = this.options.wrapAround ? a.modulo(s, i) : s,
          r = this.slides[o];
      r && (n = n.concat(r.getCellElements()));
    }

    return n;
  }, p.queryCell = function (t) {
    if ("number" == typeof t) return this.cells[t];

    if ("string" == typeof t) {
      if (t.match(/^[#\.]?[\d\/]/)) return;
      t = this.element.querySelector(t);
    }

    return this.getCell(t);
  }, p.uiChange = function () {
    this.emitEvent("uiChange");
  }, p.childUIPointerDown = function (t) {
    "touchstart" != t.type && t.preventDefault(), this.focus();
  }, p.onresize = function () {
    this.watchCSS(), this.resize();
  }, a.debounceMethod(f, "onresize", 150), p.resize = function () {
    if (this.isActive) {
      this.getSize(), this.options.wrapAround && (this.x = a.modulo(this.x, this.slideableWidth)), this.positionCells(), this._getWrapShiftCells(), this.setGallerySize(), this.emitEvent("resize");
      var t = this.selectedElements && this.selectedElements[0];
      this.selectCell(t, !1, !0);
    }
  }, p.watchCSS = function () {
    this.options.watchCSS && (-1 != o(this.element, ":after").content.indexOf("flickity") ? this.activate() : this.deactivate());
  }, p.onkeydown = function (t) {
    var e = document.activeElement && document.activeElement != this.element;

    if (this.options.accessibility && !e) {
      var i = f.keyboardHandlers[t.keyCode];
      i && i.call(this);
    }
  }, f.keyboardHandlers = {
    37: function _() {
      var t = this.options.rightToLeft ? "next" : "previous";
      this.uiChange(), this[t]();
    },
    39: function _() {
      var t = this.options.rightToLeft ? "previous" : "next";
      this.uiChange(), this[t]();
    }
  }, p.focus = function () {
    var t = n.pageYOffset;
    this.element.focus({
      preventScroll: !0
    }), n.pageYOffset != t && n.scrollTo(n.pageXOffset, t);
  }, p.deactivate = function () {
    this.isActive && (this.element.classList.remove("flickity-enabled"), this.element.classList.remove("flickity-rtl"), this.unselectSelectedSlide(), this.cells.forEach(function (t) {
      t.destroy();
    }), this.element.removeChild(this.viewport), c(this.slider.children, this.element), this.options.accessibility && (this.element.removeAttribute("tabIndex"), this.element.removeEventListener("keydown", this)), this.isActive = !1, this.emitEvent("deactivate"));
  }, p.destroy = function () {
    this.deactivate(), n.removeEventListener("resize", this), this.allOff(), this.emitEvent("destroy"), l && this.$element && l.removeData(this.element, "flickity"), delete this.element.flickityGUID, delete u[this.guid];
  }, a.extend(p, s), f.data = function (t) {
    var e = (t = a.getQueryElement(t)) && t.flickityGUID;
    return e && u[e];
  }, a.htmlInit(f, "flickity"), l && l.bridget && l.bridget("flickity", f), f.setJQuery = function (t) {
    l = t;
  }, f.Cell = i, f.Slide = r, f;
}), function (e, i) {
  "function" == typeof define && define.amd ? define("unipointer/unipointer", ["ev-emitter/ev-emitter"], function (t) {
    return i(e, t);
  }) : "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports ? module.exports = i(e, require("ev-emitter")) : e.Unipointer = i(e, e.EvEmitter);
}(window, function (s, t) {
  function e() {}

  var i = e.prototype = Object.create(t.prototype);
  i.bindStartEvent = function (t) {
    this._bindStartEvent(t, !0);
  }, i.unbindStartEvent = function (t) {
    this._bindStartEvent(t, !1);
  }, i._bindStartEvent = function (t, e) {
    var i = (e = void 0 === e || e) ? "addEventListener" : "removeEventListener",
        n = "mousedown";
    s.PointerEvent ? n = "pointerdown" : "ontouchstart" in s && (n = "touchstart"), t[i](n, this);
  }, i.handleEvent = function (t) {
    var e = "on" + t.type;
    this[e] && this[e](t);
  }, i.getTouch = function (t) {
    for (var e = 0; e < t.length; e++) {
      var i = t[e];
      if (i.identifier == this.pointerIdentifier) return i;
    }
  }, i.onmousedown = function (t) {
    var e = t.button;
    e && 0 !== e && 1 !== e || this._pointerDown(t, t);
  }, i.ontouchstart = function (t) {
    this._pointerDown(t, t.changedTouches[0]);
  }, i.onpointerdown = function (t) {
    this._pointerDown(t, t);
  }, i._pointerDown = function (t, e) {
    t.button || this.isPointerDown || (this.isPointerDown = !0, this.pointerIdentifier = void 0 !== e.pointerId ? e.pointerId : e.identifier, this.pointerDown(t, e));
  }, i.pointerDown = function (t, e) {
    this._bindPostStartEvents(t), this.emitEvent("pointerDown", [t, e]);
  };
  var n = {
    mousedown: ["mousemove", "mouseup"],
    touchstart: ["touchmove", "touchend", "touchcancel"],
    pointerdown: ["pointermove", "pointerup", "pointercancel"]
  };
  return i._bindPostStartEvents = function (t) {
    if (t) {
      var e = n[t.type];
      e.forEach(function (t) {
        s.addEventListener(t, this);
      }, this), this._boundPointerEvents = e;
    }
  }, i._unbindPostStartEvents = function () {
    this._boundPointerEvents && (this._boundPointerEvents.forEach(function (t) {
      s.removeEventListener(t, this);
    }, this), delete this._boundPointerEvents);
  }, i.onmousemove = function (t) {
    this._pointerMove(t, t);
  }, i.onpointermove = function (t) {
    t.pointerId == this.pointerIdentifier && this._pointerMove(t, t);
  }, i.ontouchmove = function (t) {
    var e = this.getTouch(t.changedTouches);
    e && this._pointerMove(t, e);
  }, i._pointerMove = function (t, e) {
    this.pointerMove(t, e);
  }, i.pointerMove = function (t, e) {
    this.emitEvent("pointerMove", [t, e]);
  }, i.onmouseup = function (t) {
    this._pointerUp(t, t);
  }, i.onpointerup = function (t) {
    t.pointerId == this.pointerIdentifier && this._pointerUp(t, t);
  }, i.ontouchend = function (t) {
    var e = this.getTouch(t.changedTouches);
    e && this._pointerUp(t, e);
  }, i._pointerUp = function (t, e) {
    this._pointerDone(), this.pointerUp(t, e);
  }, i.pointerUp = function (t, e) {
    this.emitEvent("pointerUp", [t, e]);
  }, i._pointerDone = function () {
    this._pointerReset(), this._unbindPostStartEvents(), this.pointerDone();
  }, i._pointerReset = function () {
    this.isPointerDown = !1, delete this.pointerIdentifier;
  }, i.pointerDone = function () {}, i.onpointercancel = function (t) {
    t.pointerId == this.pointerIdentifier && this._pointerCancel(t, t);
  }, i.ontouchcancel = function (t) {
    var e = this.getTouch(t.changedTouches);
    e && this._pointerCancel(t, e);
  }, i._pointerCancel = function (t, e) {
    this._pointerDone(), this.pointerCancel(t, e);
  }, i.pointerCancel = function (t, e) {
    this.emitEvent("pointerCancel", [t, e]);
  }, e.getPointerPoint = function (t) {
    return {
      x: t.pageX,
      y: t.pageY
    };
  }, e;
}), function (e, i) {
  "function" == typeof define && define.amd ? define("unidragger/unidragger", ["unipointer/unipointer"], function (t) {
    return i(e, t);
  }) : "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports ? module.exports = i(e, require("unipointer")) : e.Unidragger = i(e, e.Unipointer);
}(window, function (o, t) {
  function e() {}

  var i = e.prototype = Object.create(t.prototype);
  i.bindHandles = function () {
    this._bindHandles(!0);
  }, i.unbindHandles = function () {
    this._bindHandles(!1);
  }, i._bindHandles = function (t) {
    for (var e = (t = void 0 === t || t) ? "addEventListener" : "removeEventListener", i = t ? this._touchActionValue : "", n = 0; n < this.handles.length; n++) {
      var s = this.handles[n];
      this._bindStartEvent(s, t), s[e]("click", this), o.PointerEvent && (s.style.touchAction = i);
    }
  }, i._touchActionValue = "none", i.pointerDown = function (t, e) {
    this.okayPointerDown(t) && (this.pointerDownPointer = e, t.preventDefault(), this.pointerDownBlur(), this._bindPostStartEvents(t), this.emitEvent("pointerDown", [t, e]));
  };
  var s = {
    TEXTAREA: !0,
    INPUT: !0,
    SELECT: !0,
    OPTION: !0
  },
      r = {
    radio: !0,
    checkbox: !0,
    button: !0,
    submit: !0,
    image: !0,
    file: !0
  };
  return i.okayPointerDown = function (t) {
    var e = s[t.target.nodeName],
        i = r[t.target.type],
        n = !e || i;
    return n || this._pointerReset(), n;
  }, i.pointerDownBlur = function () {
    var t = document.activeElement;
    t && t.blur && t != document.body && t.blur();
  }, i.pointerMove = function (t, e) {
    var i = this._dragPointerMove(t, e);

    this.emitEvent("pointerMove", [t, e, i]), this._dragMove(t, e, i);
  }, i._dragPointerMove = function (t, e) {
    var i = {
      x: e.pageX - this.pointerDownPointer.pageX,
      y: e.pageY - this.pointerDownPointer.pageY
    };
    return !this.isDragging && this.hasDragStarted(i) && this._dragStart(t, e), i;
  }, i.hasDragStarted = function (t) {
    return 3 < Math.abs(t.x) || 3 < Math.abs(t.y);
  }, i.pointerUp = function (t, e) {
    this.emitEvent("pointerUp", [t, e]), this._dragPointerUp(t, e);
  }, i._dragPointerUp = function (t, e) {
    this.isDragging ? this._dragEnd(t, e) : this._staticClick(t, e);
  }, i._dragStart = function (t, e) {
    this.isDragging = !0, this.isPreventingClicks = !0, this.dragStart(t, e);
  }, i.dragStart = function (t, e) {
    this.emitEvent("dragStart", [t, e]);
  }, i._dragMove = function (t, e, i) {
    this.isDragging && this.dragMove(t, e, i);
  }, i.dragMove = function (t, e, i) {
    t.preventDefault(), this.emitEvent("dragMove", [t, e, i]);
  }, i._dragEnd = function (t, e) {
    this.isDragging = !1, setTimeout(function () {
      delete this.isPreventingClicks;
    }.bind(this)), this.dragEnd(t, e);
  }, i.dragEnd = function (t, e) {
    this.emitEvent("dragEnd", [t, e]);
  }, i.onclick = function (t) {
    this.isPreventingClicks && t.preventDefault();
  }, i._staticClick = function (t, e) {
    this.isIgnoringMouseUp && "mouseup" == t.type || (this.staticClick(t, e), "mouseup" != t.type && (this.isIgnoringMouseUp = !0, setTimeout(function () {
      delete this.isIgnoringMouseUp;
    }.bind(this), 400)));
  }, i.staticClick = function (t, e) {
    this.emitEvent("staticClick", [t, e]);
  }, e.getPointerPoint = t.getPointerPoint, e;
}), function (n, s) {
  "function" == typeof define && define.amd ? define("flickity/js/drag", ["./flickity", "unidragger/unidragger", "fizzy-ui-utils/utils"], function (t, e, i) {
    return s(n, t, e, i);
  }) : "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports ? module.exports = s(n, require("./flickity"), require("unidragger"), require("fizzy-ui-utils")) : n.Flickity = s(n, n.Flickity, n.Unidragger, n.fizzyUIUtils);
}(window, function (i, t, e, a) {
  a.extend(t.defaults, {
    draggable: ">1",
    dragThreshold: 3
  }), t.createMethods.push("_createDrag");
  var n = t.prototype;
  a.extend(n, e.prototype), n._touchActionValue = "pan-y";
  var s = "createTouch" in document,
      o = !1;
  n._createDrag = function () {
    this.on("activate", this.onActivateDrag), this.on("uiChange", this._uiChangeDrag), this.on("deactivate", this.onDeactivateDrag), this.on("cellChange", this.updateDraggable), s && !o && (i.addEventListener("touchmove", function () {}), o = !0);
  }, n.onActivateDrag = function () {
    this.handles = [this.viewport], this.bindHandles(), this.updateDraggable();
  }, n.onDeactivateDrag = function () {
    this.unbindHandles(), this.element.classList.remove("is-draggable");
  }, n.updateDraggable = function () {
    ">1" == this.options.draggable ? this.isDraggable = 1 < this.slides.length : this.isDraggable = this.options.draggable, this.isDraggable ? this.element.classList.add("is-draggable") : this.element.classList.remove("is-draggable");
  }, n.bindDrag = function () {
    this.options.draggable = !0, this.updateDraggable();
  }, n.unbindDrag = function () {
    this.options.draggable = !1, this.updateDraggable();
  }, n._uiChangeDrag = function () {
    delete this.isFreeScrolling;
  }, n.pointerDown = function (t, e) {
    this.isDraggable ? this.okayPointerDown(t) && (this._pointerDownPreventDefault(t), this.pointerDownFocus(t), document.activeElement != this.element && this.pointerDownBlur(), this.dragX = this.x, this.viewport.classList.add("is-pointer-down"), this.pointerDownScroll = l(), i.addEventListener("scroll", this), this._pointerDownDefault(t, e)) : this._pointerDownDefault(t, e);
  }, n._pointerDownDefault = function (t, e) {
    this.pointerDownPointer = {
      pageX: e.pageX,
      pageY: e.pageY
    }, this._bindPostStartEvents(t), this.dispatchEvent("pointerDown", t, [e]);
  };
  var r = {
    INPUT: !0,
    TEXTAREA: !0,
    SELECT: !0
  };

  function l() {
    return {
      x: i.pageXOffset,
      y: i.pageYOffset
    };
  }

  return n.pointerDownFocus = function (t) {
    r[t.target.nodeName] || this.focus();
  }, n._pointerDownPreventDefault = function (t) {
    var e = "touchstart" == t.type,
        i = "touch" == t.pointerType,
        n = r[t.target.nodeName];
    e || i || n || t.preventDefault();
  }, n.hasDragStarted = function (t) {
    return Math.abs(t.x) > this.options.dragThreshold;
  }, n.pointerUp = function (t, e) {
    delete this.isTouchScrolling, this.viewport.classList.remove("is-pointer-down"), this.dispatchEvent("pointerUp", t, [e]), this._dragPointerUp(t, e);
  }, n.pointerDone = function () {
    i.removeEventListener("scroll", this), delete this.pointerDownScroll;
  }, n.dragStart = function (t, e) {
    this.isDraggable && (this.dragStartPosition = this.x, this.startAnimation(), i.removeEventListener("scroll", this), this.dispatchEvent("dragStart", t, [e]));
  }, n.pointerMove = function (t, e) {
    var i = this._dragPointerMove(t, e);

    this.dispatchEvent("pointerMove", t, [e, i]), this._dragMove(t, e, i);
  }, n.dragMove = function (t, e, i) {
    if (this.isDraggable) {
      t.preventDefault(), this.previousDragX = this.dragX;
      var n = this.options.rightToLeft ? -1 : 1;
      this.options.wrapAround && (i.x = i.x % this.slideableWidth);
      var s = this.dragStartPosition + i.x * n;

      if (!this.options.wrapAround && this.slides.length) {
        var o = Math.max(-this.slides[0].target, this.dragStartPosition);
        s = o < s ? .5 * (s + o) : s;
        var r = Math.min(-this.getLastSlide().target, this.dragStartPosition);
        s = s < r ? .5 * (s + r) : s;
      }

      this.dragX = s, this.dragMoveTime = new Date(), this.dispatchEvent("dragMove", t, [e, i]);
    }
  }, n.dragEnd = function (t, e) {
    if (this.isDraggable) {
      this.options.freeScroll && (this.isFreeScrolling = !0);
      var i = this.dragEndRestingSelect();

      if (this.options.freeScroll && !this.options.wrapAround) {
        var n = this.getRestingPosition();
        this.isFreeScrolling = -n > this.slides[0].target && -n < this.getLastSlide().target;
      } else this.options.freeScroll || i != this.selectedIndex || (i += this.dragEndBoostSelect());

      delete this.previousDragX, this.isDragSelect = this.options.wrapAround, this.select(i), delete this.isDragSelect, this.dispatchEvent("dragEnd", t, [e]);
    }
  }, n.dragEndRestingSelect = function () {
    var t = this.getRestingPosition(),
        e = Math.abs(this.getSlideDistance(-t, this.selectedIndex)),
        i = this._getClosestResting(t, e, 1),
        n = this._getClosestResting(t, e, -1);

    return i.distance < n.distance ? i.index : n.index;
  }, n._getClosestResting = function (t, e, i) {
    for (var n = this.selectedIndex, s = 1 / 0, o = this.options.contain && !this.options.wrapAround ? function (t, e) {
      return t <= e;
    } : function (t, e) {
      return t < e;
    }; o(e, s) && (n += i, s = e, null !== (e = this.getSlideDistance(-t, n)));) {
      e = Math.abs(e);
    }

    return {
      distance: s,
      index: n - i
    };
  }, n.getSlideDistance = function (t, e) {
    var i = this.slides.length,
        n = this.options.wrapAround && 1 < i,
        s = n ? a.modulo(e, i) : e,
        o = this.slides[s];
    if (!o) return null;
    var r = n ? this.slideableWidth * Math.floor(e / i) : 0;
    return t - (o.target + r);
  }, n.dragEndBoostSelect = function () {
    if (void 0 === this.previousDragX || !this.dragMoveTime || 100 < new Date() - this.dragMoveTime) return 0;
    var t = this.getSlideDistance(-this.dragX, this.selectedIndex),
        e = this.previousDragX - this.dragX;
    return 0 < t && 0 < e ? 1 : t < 0 && e < 0 ? -1 : 0;
  }, n.staticClick = function (t, e) {
    var i = this.getParentCell(t.target),
        n = i && i.element,
        s = i && this.cells.indexOf(i);
    this.dispatchEvent("staticClick", t, [e, n, s]);
  }, n.onscroll = function () {
    var t = l(),
        e = this.pointerDownScroll.x - t.x,
        i = this.pointerDownScroll.y - t.y;
    (3 < Math.abs(e) || 3 < Math.abs(i)) && this._pointerDone();
  }, t;
}), function (n, s) {
  "function" == typeof define && define.amd ? define("flickity/js/prev-next-button", ["./flickity", "unipointer/unipointer", "fizzy-ui-utils/utils"], function (t, e, i) {
    return s(n, t, e, i);
  }) : "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports ? module.exports = s(n, require("./flickity"), require("unipointer"), require("fizzy-ui-utils")) : s(n, n.Flickity, n.Unipointer, n.fizzyUIUtils);
}(window, function (t, e, i, n) {
  "use strict";

  var s = "http://www.w3.org/2000/svg";

  function o(t, e) {
    this.direction = t, this.parent = e, this._create();
  }

  (o.prototype = Object.create(i.prototype))._create = function () {
    this.isEnabled = !0, this.isPrevious = -1 == this.direction;
    var t = this.parent.options.rightToLeft ? 1 : -1;
    this.isLeft = this.direction == t;
    var e = this.element = document.createElement("button");
    e.className = "flickity-button flickity-prev-next-button", e.className += this.isPrevious ? " previous" : " next", e.setAttribute("type", "button"), this.disable(), e.setAttribute("aria-label", this.isPrevious ? "Previous" : "Next");
    var i = this.createSVG();
    e.appendChild(i), this.parent.on("select", this.update.bind(this)), this.on("pointerDown", this.parent.childUIPointerDown.bind(this.parent));
  }, o.prototype.activate = function () {
    this.bindStartEvent(this.element), this.element.addEventListener("click", this), this.parent.element.appendChild(this.element);
  }, o.prototype.deactivate = function () {
    this.parent.element.removeChild(this.element), this.unbindStartEvent(this.element), this.element.removeEventListener("click", this);
  }, o.prototype.createSVG = function () {
    var t = document.createElementNS(s, "svg");
    t.setAttribute("class", "flickity-button-icon"), t.setAttribute("viewBox", "0 0 100 100");
    var e,
        i = document.createElementNS(s, "path"),
        n = "string" != typeof (e = this.parent.options.arrowShape) ? "M " + e.x0 + ",50 L " + e.x1 + "," + (e.y1 + 50) + " L " + e.x2 + "," + (e.y2 + 50) + " L " + e.x3 + ",50  L " + e.x2 + "," + (50 - e.y2) + " L " + e.x1 + "," + (50 - e.y1) + " Z" : e;
    return i.setAttribute("d", n), i.setAttribute("class", "arrow"), this.isLeft || i.setAttribute("transform", "translate(100, 100) rotate(180) "), t.appendChild(i), t;
  }, o.prototype.handleEvent = n.handleEvent, o.prototype.onclick = function () {
    if (this.isEnabled) {
      this.parent.uiChange();
      var t = this.isPrevious ? "previous" : "next";
      this.parent[t]();
    }
  }, o.prototype.enable = function () {
    this.isEnabled || (this.element.disabled = !1, this.isEnabled = !0);
  }, o.prototype.disable = function () {
    this.isEnabled && (this.element.disabled = !0, this.isEnabled = !1);
  }, o.prototype.update = function () {
    var t = this.parent.slides;
    if (this.parent.options.wrapAround && 1 < t.length) this.enable();else {
      var e = t.length ? t.length - 1 : 0,
          i = this.isPrevious ? 0 : e;
      this[this.parent.selectedIndex == i ? "disable" : "enable"]();
    }
  }, o.prototype.destroy = function () {
    this.deactivate(), this.allOff();
  }, n.extend(e.defaults, {
    prevNextButtons: !0,
    arrowShape: {
      x0: 10,
      x1: 60,
      y1: 50,
      x2: 70,
      y2: 40,
      x3: 30
    }
  }), e.createMethods.push("_createPrevNextButtons");
  var r = e.prototype;
  return r._createPrevNextButtons = function () {
    this.options.prevNextButtons && (this.prevButton = new o(-1, this), this.nextButton = new o(1, this), this.on("activate", this.activatePrevNextButtons));
  }, r.activatePrevNextButtons = function () {
    this.prevButton.activate(), this.nextButton.activate(), this.on("deactivate", this.deactivatePrevNextButtons);
  }, r.deactivatePrevNextButtons = function () {
    this.prevButton.deactivate(), this.nextButton.deactivate(), this.off("deactivate", this.deactivatePrevNextButtons);
  }, e.PrevNextButton = o, e;
}), function (n, s) {
  "function" == typeof define && define.amd ? define("flickity/js/page-dots", ["./flickity", "unipointer/unipointer", "fizzy-ui-utils/utils"], function (t, e, i) {
    return s(n, t, e, i);
  }) : "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports ? module.exports = s(n, require("./flickity"), require("unipointer"), require("fizzy-ui-utils")) : s(n, n.Flickity, n.Unipointer, n.fizzyUIUtils);
}(window, function (t, e, i, n) {
  function s(t) {
    this.parent = t, this._create();
  }

  (s.prototype = Object.create(i.prototype))._create = function () {
    this.holder = document.createElement("ol"), this.holder.className = "flickity-page-dots", this.dots = [], this.handleClick = this.onClick.bind(this), this.on("pointerDown", this.parent.childUIPointerDown.bind(this.parent));
  }, s.prototype.activate = function () {
    this.setDots(), this.holder.addEventListener("click", this.handleClick), this.bindStartEvent(this.holder), this.parent.element.appendChild(this.holder);
  }, s.prototype.deactivate = function () {
    this.holder.removeEventListener("click", this.handleClick), this.unbindStartEvent(this.holder), this.parent.element.removeChild(this.holder);
  }, s.prototype.setDots = function () {
    var t = this.parent.slides.length - this.dots.length;
    0 < t ? this.addDots(t) : t < 0 && this.removeDots(-t);
  }, s.prototype.addDots = function (t) {
    for (var e = document.createDocumentFragment(), i = [], n = this.dots.length, s = n + t, o = n; o < s; o++) {
      var r = document.createElement("li");
      r.className = "dot", r.setAttribute("aria-label", "Page dot " + (o + 1)), e.appendChild(r), i.push(r);
    }

    this.holder.appendChild(e), this.dots = this.dots.concat(i);
  }, s.prototype.removeDots = function (t) {
    this.dots.splice(this.dots.length - t, t).forEach(function (t) {
      this.holder.removeChild(t);
    }, this);
  }, s.prototype.updateSelected = function () {
    this.selectedDot && (this.selectedDot.className = "dot", this.selectedDot.removeAttribute("aria-current")), this.dots.length && (this.selectedDot = this.dots[this.parent.selectedIndex], this.selectedDot.className = "dot is-selected", this.selectedDot.setAttribute("aria-current", "step"));
  }, s.prototype.onTap = s.prototype.onClick = function (t) {
    var e = t.target;

    if ("LI" == e.nodeName) {
      this.parent.uiChange();
      var i = this.dots.indexOf(e);
      this.parent.select(i);
    }
  }, s.prototype.destroy = function () {
    this.deactivate(), this.allOff();
  }, e.PageDots = s, n.extend(e.defaults, {
    pageDots: !0
  }), e.createMethods.push("_createPageDots");
  var o = e.prototype;
  return o._createPageDots = function () {
    this.options.pageDots && (this.pageDots = new s(this), this.on("activate", this.activatePageDots), this.on("select", this.updateSelectedPageDots), this.on("cellChange", this.updatePageDots), this.on("resize", this.updatePageDots), this.on("deactivate", this.deactivatePageDots));
  }, o.activatePageDots = function () {
    this.pageDots.activate();
  }, o.updateSelectedPageDots = function () {
    this.pageDots.updateSelected();
  }, o.updatePageDots = function () {
    this.pageDots.setDots();
  }, o.deactivatePageDots = function () {
    this.pageDots.deactivate();
  }, e.PageDots = s, e;
}), function (t, n) {
  "function" == typeof define && define.amd ? define("flickity/js/player", ["ev-emitter/ev-emitter", "fizzy-ui-utils/utils", "./flickity"], function (t, e, i) {
    return n(t, e, i);
  }) : "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports ? module.exports = n(require("ev-emitter"), require("fizzy-ui-utils"), require("./flickity")) : n(t.EvEmitter, t.fizzyUIUtils, t.Flickity);
}(window, function (t, e, i) {
  function n(t) {
    this.parent = t, this.state = "stopped", this.onVisibilityChange = this.visibilityChange.bind(this), this.onVisibilityPlay = this.visibilityPlay.bind(this);
  }

  (n.prototype = Object.create(t.prototype)).play = function () {
    "playing" != this.state && (document.hidden ? document.addEventListener("visibilitychange", this.onVisibilityPlay) : (this.state = "playing", document.addEventListener("visibilitychange", this.onVisibilityChange), this.tick()));
  }, n.prototype.tick = function () {
    if ("playing" == this.state) {
      var t = this.parent.options.autoPlay;
      t = "number" == typeof t ? t : 3e3;
      var e = this;
      this.clear(), this.timeout = setTimeout(function () {
        e.parent.next(!0), e.tick();
      }, t);
    }
  }, n.prototype.stop = function () {
    this.state = "stopped", this.clear(), document.removeEventListener("visibilitychange", this.onVisibilityChange);
  }, n.prototype.clear = function () {
    clearTimeout(this.timeout);
  }, n.prototype.pause = function () {
    "playing" == this.state && (this.state = "paused", this.clear());
  }, n.prototype.unpause = function () {
    "paused" == this.state && this.play();
  }, n.prototype.visibilityChange = function () {
    this[document.hidden ? "pause" : "unpause"]();
  }, n.prototype.visibilityPlay = function () {
    this.play(), document.removeEventListener("visibilitychange", this.onVisibilityPlay);
  }, e.extend(i.defaults, {
    pauseAutoPlayOnHover: !0
  }), i.createMethods.push("_createPlayer");
  var s = i.prototype;
  return s._createPlayer = function () {
    this.player = new n(this), this.on("activate", this.activatePlayer), this.on("uiChange", this.stopPlayer), this.on("pointerDown", this.stopPlayer), this.on("deactivate", this.deactivatePlayer);
  }, s.activatePlayer = function () {
    this.options.autoPlay && (this.player.play(), this.element.addEventListener("mouseenter", this));
  }, s.playPlayer = function () {
    this.player.play();
  }, s.stopPlayer = function () {
    this.player.stop();
  }, s.pausePlayer = function () {
    this.player.pause();
  }, s.unpausePlayer = function () {
    this.player.unpause();
  }, s.deactivatePlayer = function () {
    this.player.stop(), this.element.removeEventListener("mouseenter", this);
  }, s.onmouseenter = function () {
    this.options.pauseAutoPlayOnHover && (this.player.pause(), this.element.addEventListener("mouseleave", this));
  }, s.onmouseleave = function () {
    this.player.unpause(), this.element.removeEventListener("mouseleave", this);
  }, i.Player = n, i;
}), function (i, n) {
  "function" == typeof define && define.amd ? define("flickity/js/add-remove-cell", ["./flickity", "fizzy-ui-utils/utils"], function (t, e) {
    return n(i, t, e);
  }) : "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports ? module.exports = n(i, require("./flickity"), require("fizzy-ui-utils")) : n(i, i.Flickity, i.fizzyUIUtils);
}(window, function (t, e, n) {
  var i = e.prototype;
  return i.insert = function (t, e) {
    var i = this._makeCells(t);

    if (i && i.length) {
      var n = this.cells.length;
      e = void 0 === e ? n : e;
      var s,
          o,
          r = (s = i, o = document.createDocumentFragment(), s.forEach(function (t) {
        o.appendChild(t.element);
      }), o),
          a = e == n;
      if (a) this.slider.appendChild(r);else {
        var l = this.cells[e].element;
        this.slider.insertBefore(r, l);
      }
      if (0 === e) this.cells = i.concat(this.cells);else if (a) this.cells = this.cells.concat(i);else {
        var h = this.cells.splice(e, n - e);
        this.cells = this.cells.concat(i).concat(h);
      }
      this._sizeCells(i), this.cellChange(e, !0);
    }
  }, i.append = function (t) {
    this.insert(t, this.cells.length);
  }, i.prepend = function (t) {
    this.insert(t, 0);
  }, i.remove = function (t) {
    var e = this.getCells(t);

    if (e && e.length) {
      var i = this.cells.length - 1;
      e.forEach(function (t) {
        t.remove();
        var e = this.cells.indexOf(t);
        i = Math.min(e, i), n.removeFrom(this.cells, t);
      }, this), this.cellChange(i, !0);
    }
  }, i.cellSizeChange = function (t) {
    var e = this.getCell(t);

    if (e) {
      e.getSize();
      var i = this.cells.indexOf(e);
      this.cellChange(i);
    }
  }, i.cellChange = function (t, e) {
    var i = this.selectedElement;
    this._positionCells(t), this._getWrapShiftCells(), this.setGallerySize();
    var n = this.getCell(i);
    n && (this.selectedIndex = this.getCellSlideIndex(n)), this.selectedIndex = Math.min(this.slides.length - 1, this.selectedIndex), this.emitEvent("cellChange", [t]), this.select(this.selectedIndex), e && this.positionSliderAtSelected();
  }, e;
}), function (i, n) {
  "function" == typeof define && define.amd ? define("flickity/js/lazyload", ["./flickity", "fizzy-ui-utils/utils"], function (t, e) {
    return n(i, t, e);
  }) : "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports ? module.exports = n(i, require("./flickity"), require("fizzy-ui-utils")) : n(i, i.Flickity, i.fizzyUIUtils);
}(window, function (t, e, o) {
  "use strict";

  e.createMethods.push("_createLazyload");
  var i = e.prototype;

  function s(t, e) {
    this.img = t, this.flickity = e, this.load();
  }

  return i._createLazyload = function () {
    this.on("select", this.lazyLoad);
  }, i.lazyLoad = function () {
    var t = this.options.lazyLoad;

    if (t) {
      var e = "number" == typeof t ? t : 0,
          i = this.getAdjacentCellElements(e),
          n = [];
      i.forEach(function (t) {
        var e = function (t) {
          if ("IMG" == t.nodeName) {
            var e = t.getAttribute("data-flickity-lazyload"),
                i = t.getAttribute("data-flickity-lazyload-src"),
                n = t.getAttribute("data-flickity-lazyload-srcset");
            if (e || i || n) return [t];
          }

          var s = t.querySelectorAll("img[data-flickity-lazyload], img[data-flickity-lazyload-src], img[data-flickity-lazyload-srcset]");
          return o.makeArray(s);
        }(t);

        n = n.concat(e);
      }), n.forEach(function (t) {
        new s(t, this);
      }, this);
    }
  }, s.prototype.handleEvent = o.handleEvent, s.prototype.load = function () {
    this.img.addEventListener("load", this), this.img.addEventListener("error", this);
    var t = this.img.getAttribute("data-flickity-lazyload") || this.img.getAttribute("data-flickity-lazyload-src"),
        e = this.img.getAttribute("data-flickity-lazyload-srcset");
    this.img.src = t, e && this.img.setAttribute("srcset", e), this.img.removeAttribute("data-flickity-lazyload"), this.img.removeAttribute("data-flickity-lazyload-src"), this.img.removeAttribute("data-flickity-lazyload-srcset");
  }, s.prototype.onload = function (t) {
    this.complete(t, "flickity-lazyloaded");
  }, s.prototype.onerror = function (t) {
    this.complete(t, "flickity-lazyerror");
  }, s.prototype.complete = function (t, e) {
    this.img.removeEventListener("load", this), this.img.removeEventListener("error", this);
    var i = this.flickity.getParentCell(this.img),
        n = i && i.element;
    this.flickity.cellSizeChange(n), this.img.classList.add(e), this.flickity.dispatchEvent("lazyLoad", t, n);
  }, e.LazyLoader = s, e;
}), function (t, e) {
  "function" == typeof define && define.amd ? define("flickity/js/index", ["./flickity", "./drag", "./prev-next-button", "./page-dots", "./player", "./add-remove-cell", "./lazyload"], e) : "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports && (module.exports = e(require("./flickity"), require("./drag"), require("./prev-next-button"), require("./page-dots"), require("./player"), require("./add-remove-cell"), require("./lazyload")));
}(window, function (t) {
  return t;
}), function (t, e) {
  "function" == typeof define && define.amd ? define("flickity-as-nav-for/as-nav-for", ["flickity/js/index", "fizzy-ui-utils/utils"], e) : "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports ? module.exports = e(require("flickity"), require("fizzy-ui-utils")) : t.Flickity = e(t.Flickity, t.fizzyUIUtils);
}(window, function (n, s) {
  n.createMethods.push("_createAsNavFor");
  var t = n.prototype;
  return t._createAsNavFor = function () {
    this.on("activate", this.activateAsNavFor), this.on("deactivate", this.deactivateAsNavFor), this.on("destroy", this.destroyAsNavFor);
    var t = this.options.asNavFor;

    if (t) {
      var e = this;
      setTimeout(function () {
        e.setNavCompanion(t);
      });
    }
  }, t.setNavCompanion = function (t) {
    t = s.getQueryElement(t);
    var e = n.data(t);

    if (e && e != this) {
      this.navCompanion = e;
      var i = this;
      this.onNavCompanionSelect = function () {
        i.navCompanionSelect();
      }, e.on("select", this.onNavCompanionSelect), this.on("staticClick", this.onNavStaticClick), this.navCompanionSelect(!0);
    }
  }, t.navCompanionSelect = function (t) {
    if (this.navCompanion) {
      var e,
          i,
          n,
          s = this.navCompanion.selectedCells[0],
          o = this.navCompanion.cells.indexOf(s),
          r = o + this.navCompanion.selectedCells.length - 1,
          a = Math.floor((e = o, i = r, n = this.navCompanion.cellAlign, (i - e) * n + e));

      if (this.selectCell(a, !1, t), this.removeNavSelectedElements(), !(a >= this.cells.length)) {
        var l = this.cells.slice(o, r + 1);
        this.navSelectedElements = l.map(function (t) {
          return t.element;
        }), this.changeNavSelectedClass("add");
      }
    }
  }, t.changeNavSelectedClass = function (e) {
    this.navSelectedElements.forEach(function (t) {
      t.classList[e]("is-nav-selected");
    });
  }, t.activateAsNavFor = function () {
    this.navCompanionSelect(!0);
  }, t.removeNavSelectedElements = function () {
    this.navSelectedElements && (this.changeNavSelectedClass("remove"), delete this.navSelectedElements);
  }, t.onNavStaticClick = function (t, e, i, n) {
    "number" == typeof n && this.navCompanion.selectCell(n);
  }, t.deactivateAsNavFor = function () {
    this.removeNavSelectedElements();
  }, t.destroyAsNavFor = function () {
    this.navCompanion && (this.navCompanion.off("select", this.onNavCompanionSelect), this.off("staticClick", this.onNavStaticClick), delete this.navCompanion);
  }, n;
}), function (e, i) {
  "use strict";

  "function" == typeof define && define.amd ? define("imagesloaded/imagesloaded", ["ev-emitter/ev-emitter"], function (t) {
    return i(e, t);
  }) : "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports ? module.exports = i(e, require("ev-emitter")) : e.imagesLoaded = i(e, e.EvEmitter);
}("undefined" != typeof window ? window : void 0, function (e, t) {
  var o = e.jQuery,
      r = e.console;

  function a(t, e) {
    for (var i in e) {
      t[i] = e[i];
    }

    return t;
  }

  var l = Array.prototype.slice;

  function h(t, e, i) {
    if (!(this instanceof h)) return new h(t, e, i);
    var n,
        s = t;
    ("string" == typeof t && (s = document.querySelectorAll(t)), s) ? (this.elements = (n = s, Array.isArray(n) ? n : "object" == _typeof(n) && "number" == typeof n.length ? l.call(n) : [n]), this.options = a({}, this.options), "function" == typeof e ? i = e : a(this.options, e), i && this.on("always", i), this.getImages(), o && (this.jqDeferred = new o.Deferred()), setTimeout(this.check.bind(this))) : r.error("Bad element for imagesLoaded " + (s || t));
  }

  (h.prototype = Object.create(t.prototype)).options = {}, h.prototype.getImages = function () {
    this.images = [], this.elements.forEach(this.addElementImages, this);
  }, h.prototype.addElementImages = function (t) {
    "IMG" == t.nodeName && this.addImage(t), !0 === this.options.background && this.addElementBackgroundImages(t);
    var e = t.nodeType;

    if (e && c[e]) {
      for (var i = t.querySelectorAll("img"), n = 0; n < i.length; n++) {
        var s = i[n];
        this.addImage(s);
      }

      if ("string" == typeof this.options.background) {
        var o = t.querySelectorAll(this.options.background);

        for (n = 0; n < o.length; n++) {
          var r = o[n];
          this.addElementBackgroundImages(r);
        }
      }
    }
  };
  var c = {
    1: !0,
    9: !0,
    11: !0
  };

  function i(t) {
    this.img = t;
  }

  function n(t, e) {
    this.url = t, this.element = e, this.img = new Image();
  }

  return h.prototype.addElementBackgroundImages = function (t) {
    var e = getComputedStyle(t);
    if (e) for (var i = /url\((['"])?(.*?)\1\)/gi, n = i.exec(e.backgroundImage); null !== n;) {
      var s = n && n[2];
      s && this.addBackground(s, t), n = i.exec(e.backgroundImage);
    }
  }, h.prototype.addImage = function (t) {
    var e = new i(t);
    this.images.push(e);
  }, h.prototype.addBackground = function (t, e) {
    var i = new n(t, e);
    this.images.push(i);
  }, h.prototype.check = function () {
    var n = this;

    function e(t, e, i) {
      setTimeout(function () {
        n.progress(t, e, i);
      });
    }

    this.progressedCount = 0, this.hasAnyBroken = !1, this.images.length ? this.images.forEach(function (t) {
      t.once("progress", e), t.check();
    }) : this.complete();
  }, h.prototype.progress = function (t, e, i) {
    this.progressedCount++, this.hasAnyBroken = this.hasAnyBroken || !t.isLoaded, this.emitEvent("progress", [this, t, e]), this.jqDeferred && this.jqDeferred.notify && this.jqDeferred.notify(this, t), this.progressedCount == this.images.length && this.complete(), this.options.debug && r && r.log("progress: " + i, t, e);
  }, h.prototype.complete = function () {
    var t = this.hasAnyBroken ? "fail" : "done";

    if (this.isComplete = !0, this.emitEvent(t, [this]), this.emitEvent("always", [this]), this.jqDeferred) {
      var e = this.hasAnyBroken ? "reject" : "resolve";
      this.jqDeferred[e](this);
    }
  }, (i.prototype = Object.create(t.prototype)).check = function () {
    this.getIsImageComplete() ? this.confirm(0 !== this.img.naturalWidth, "naturalWidth") : (this.proxyImage = new Image(), this.proxyImage.addEventListener("load", this), this.proxyImage.addEventListener("error", this), this.img.addEventListener("load", this), this.img.addEventListener("error", this), this.proxyImage.src = this.img.src);
  }, i.prototype.getIsImageComplete = function () {
    return this.img.complete && this.img.naturalWidth;
  }, i.prototype.confirm = function (t, e) {
    this.isLoaded = t, this.emitEvent("progress", [this, this.img, e]);
  }, i.prototype.handleEvent = function (t) {
    var e = "on" + t.type;
    this[e] && this[e](t);
  }, i.prototype.onload = function () {
    this.confirm(!0, "onload"), this.unbindEvents();
  }, i.prototype.onerror = function () {
    this.confirm(!1, "onerror"), this.unbindEvents();
  }, i.prototype.unbindEvents = function () {
    this.proxyImage.removeEventListener("load", this), this.proxyImage.removeEventListener("error", this), this.img.removeEventListener("load", this), this.img.removeEventListener("error", this);
  }, (n.prototype = Object.create(i.prototype)).check = function () {
    this.img.addEventListener("load", this), this.img.addEventListener("error", this), this.img.src = this.url, this.getIsImageComplete() && (this.confirm(0 !== this.img.naturalWidth, "naturalWidth"), this.unbindEvents());
  }, n.prototype.unbindEvents = function () {
    this.img.removeEventListener("load", this), this.img.removeEventListener("error", this);
  }, n.prototype.confirm = function (t, e) {
    this.isLoaded = t, this.emitEvent("progress", [this, this.element, e]);
  }, h.makeJQueryPlugin = function (t) {
    (t = t || e.jQuery) && ((o = t).fn.imagesLoaded = function (t, e) {
      return new h(this, t, e).jqDeferred.promise(o(this));
    });
  }, h.makeJQueryPlugin(), h;
}), function (i, n) {
  "function" == typeof define && define.amd ? define(["flickity/js/index", "imagesloaded/imagesloaded"], function (t, e) {
    return n(i, t, e);
  }) : "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports ? module.exports = n(i, require("flickity"), require("imagesloaded")) : i.Flickity = n(i, i.Flickity, i.imagesLoaded);
}(window, function (t, e, i) {
  "use strict";

  e.createMethods.push("_createImagesLoaded");
  var n = e.prototype;
  return n._createImagesLoaded = function () {
    this.on("activate", this.imagesLoaded);
  }, n.imagesLoaded = function () {
    if (this.options.imagesLoaded) {
      var n = this;
      i(this.slider).on("progress", function (t, e) {
        var i = n.getParentCell(e.img);
        n.cellSizeChange(i && i.element), n.options.freeScroll || n.positionSliderAtSelected();
      });
    }
  }, e;
});
"use strict";

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

!function (t, e) {
  "object" == (typeof exports === "undefined" ? "undefined" : _typeof(exports)) && "undefined" != typeof module ? module.exports = e() : "function" == typeof define && define.amd ? define(e) : (t = t || self).IMask = e();
}(window, function () {
  "use strict";

  var t = function t(_t2) {
    if (null == _t2) throw TypeError("Can't call method on  " + _t2);
    return _t2;
  },
      e = {}.hasOwnProperty,
      n = function n(t, _n) {
    return e.call(t, _n);
  },
      u = {}.toString,
      i = Object("z").propertyIsEnumerable(0) ? Object : function (t) {
    return "String" == function (t) {
      return u.call(t).slice(8, -1);
    }(t) ? t.split("") : Object(t);
  },
      r = function r(e) {
    return i(t(e));
  },
      a = Math.ceil,
      s = Math.floor,
      o = function o(t) {
    return isNaN(t = +t) ? 0 : (t > 0 ? s : a)(t);
  },
      l = Math.min,
      h = function h(t) {
    return t > 0 ? l(o(t), 9007199254740991) : 0;
  },
      c = Math.max,
      f = Math.min;

  function p(t, e) {
    return t(e = {
      exports: {}
    }, e.exports), e.exports;
  }

  var d,
      v,
      k = p(function (t) {
    var e = t.exports = {
      version: "2.6.5"
    };
    "number" == typeof __e && (__e = e);
  }),
      g = (k.version, p(function (t) {
    var e = t.exports = "undefined" != typeof window && window.Math == Math ? window : "undefined" != typeof self && self.Math == Math ? self : Function("return this")();
    "number" == typeof __g && (__g = e);
  })),
      y = p(function (t) {
    var e = g["__core-js_shared__"] || (g["__core-js_shared__"] = {});
    (t.exports = function (t, n) {
      return e[t] || (e[t] = void 0 !== n ? n : {});
    })("versions", []).push({
      version: k.version,
      mode: "global",
      copyright: "© 2019 Denis Pushkarev (zloirock.ru)"
    });
  }),
      _ = 0,
      m = Math.random(),
      A = function A(t) {
    return "Symbol(".concat(void 0 === t ? "" : t, ")_", (++_ + m).toString(36));
  },
      C = y("keys"),
      E = (d = !1, function (t, e, n) {
    var u,
        i = r(t),
        a = h(i.length),
        s = function (t, e) {
      return (t = o(t)) < 0 ? c(t + e, 0) : f(t, e);
    }(n, a);

    if (d && e != e) {
      for (; a > s;) {
        if ((u = i[s++]) != u) return !0;
      }
    } else for (; a > s; s++) {
      if ((d || s in i) && i[s] === e) return d || s || 0;
    }

    return !d && -1;
  }),
      F = C[v = "IE_PROTO"] || (C[v] = A(v)),
      b = "constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf".split(","),
      B = Object.keys || function (t) {
    return function (t, e) {
      var u,
          i = r(t),
          a = 0,
          s = [];

      for (u in i) {
        u != F && n(i, u) && s.push(u);
      }

      for (; e.length > a;) {
        n(i, u = e[a++]) && (~E(s, u) || s.push(u));
      }

      return s;
    }(t, b);
  },
      D = function D(t) {
    return "object" == _typeof(t) ? null !== t : "function" == typeof t;
  },
      S = function S(t) {
    if (!D(t)) throw TypeError(t + " is not an object!");
    return t;
  },
      T = function T(t) {
    try {
      return !!t();
    } catch (t) {
      return !0;
    }
  },
      w = !T(function () {
    return 7 != Object.defineProperty({}, "a", {
      get: function get() {
        return 7;
      }
    }).a;
  }),
      x = g.document,
      O = D(x) && D(x.createElement),
      M = !w && !T(function () {
    return 7 != Object.defineProperty((t = "div", O ? x.createElement(t) : {}), "a", {
      get: function get() {
        return 7;
      }
    }).a;
    var t;
  }),
      P = Object.defineProperty,
      I = {
    f: w ? Object.defineProperty : function (t, e, n) {
      if (S(t), e = function (t, e) {
        if (!D(t)) return t;
        var n, u;
        if (e && "function" == typeof (n = t.toString) && !D(u = n.call(t))) return u;
        if ("function" == typeof (n = t.valueOf) && !D(u = n.call(t))) return u;
        if (!e && "function" == typeof (n = t.toString) && !D(u = n.call(t))) return u;
        throw TypeError("Can't convert object to primitive value");
      }(e, !0), S(n), M) try {
        return P(t, e, n);
      } catch (t) {}
      if ("get" in n || "set" in n) throw TypeError("Accessors not supported!");
      return "value" in n && (t[e] = n.value), t;
    }
  },
      R = w ? function (t, e, n) {
    return I.f(t, e, function (t, e) {
      return {
        enumerable: !(1 & t),
        configurable: !(2 & t),
        writable: !(4 & t),
        value: e
      };
    }(1, n));
  } : function (t, e, n) {
    return t[e] = n, t;
  },
      V = y("native-function-to-string", Function.toString),
      j = p(function (t) {
    var e = A("src"),
        u = ("" + V).split("toString");
    k.inspectSource = function (t) {
      return V.call(t);
    }, (t.exports = function (t, i, r, a) {
      var s = "function" == typeof r;
      s && (n(r, "name") || R(r, "name", i)), t[i] !== r && (s && (n(r, e) || R(r, e, t[i] ? "" + t[i] : u.join(String(i)))), t === g ? t[i] = r : a ? t[i] ? t[i] = r : R(t, i, r) : (delete t[i], R(t, i, r)));
    })(Function.prototype, "toString", function () {
      return "function" == typeof this && this[e] || V.call(this);
    });
  }),
      N = function N(t, e, n) {
    if (function (t) {
      if ("function" != typeof t) throw TypeError(t + " is not a function!");
    }(t), void 0 === e) return t;

    switch (n) {
      case 1:
        return function (n) {
          return t.call(e, n);
        };

      case 2:
        return function (n, u) {
          return t.call(e, n, u);
        };

      case 3:
        return function (n, u, i) {
          return t.call(e, n, u, i);
        };
    }

    return function () {
      return t.apply(e, arguments);
    };
  },
      L = function L(t, e, n) {
    var u,
        i,
        r,
        a,
        s = t & L.F,
        o = t & L.G,
        l = t & L.S,
        h = t & L.P,
        c = t & L.B,
        f = o ? g : l ? g[e] || (g[e] = {}) : (g[e] || {}).prototype,
        p = o ? k : k[e] || (k[e] = {}),
        d = p.prototype || (p.prototype = {});

    for (u in o && (n = e), n) {
      r = ((i = !s && f && void 0 !== f[u]) ? f : n)[u], a = c && i ? N(r, g) : h && "function" == typeof r ? N(Function.call, r) : r, f && j(f, u, r, t & L.U), p[u] != r && R(p, u, a), h && d[u] != r && (d[u] = r);
    }
  };

  g.core = k, L.F = 1, L.G = 2, L.S = 4, L.P = 8, L.B = 16, L.W = 32, L.U = 64, L.R = 128;
  var H,
      G,
      U,
      z,
      Y = L;
  H = "keys", G = function G() {
    return function (e) {
      return B(function (e) {
        return Object(t(e));
      }(e));
    };
  }, U = (k.Object || {})[H] || Object[H], (z = {})[H] = G(U), Y(Y.S + Y.F * T(function () {
    U(1);
  }), "Object", z);
  k.Object.keys;

  var Z = function Z(e) {
    var n = String(t(this)),
        u = "",
        i = o(e);
    if (i < 0 || i == 1 / 0) throw RangeError("Count can't be negative");

    for (; i > 0; (i >>>= 1) && (n += n)) {
      1 & i && (u += n);
    }

    return u;
  };

  Y(Y.P, "String", {
    repeat: Z
  });
  k.String.repeat;

  var $ = function $(e, n, u, i) {
    var r = String(t(e)),
        a = r.length,
        s = void 0 === u ? " " : String(u),
        o = h(n);
    if (o <= a || "" == s) return r;
    var l = o - a,
        c = Z.call(s, Math.ceil(l / s.length));
    return c.length > l && (c = c.slice(0, l)), i ? c + r : r + c;
  },
      K = g.navigator,
      W = K && K.userAgent || "",
      q = /Version\/10\.\d+(\.\d+)?( Mobile\/\w+)? Safari\//.test(W);

  Y(Y.P + Y.F * q, "String", {
    padStart: function padStart(t) {
      return $(this, t, arguments.length > 1 ? arguments[1] : void 0, !0);
    }
  });
  k.String.padStart;
  var J = /Version\/10\.\d+(\.\d+)?( Mobile\/\w+)? Safari\//.test(W);
  Y(Y.P + Y.F * J, "String", {
    padEnd: function padEnd(t) {
      return $(this, t, arguments.length > 1 ? arguments[1] : void 0, !1);
    }
  });
  k.String.padEnd;

  function Q(t) {
    return (Q = "function" == typeof Symbol && "symbol" == _typeof(Symbol.iterator) ? function (t) {
      return _typeof(t);
    } : function (t) {
      return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : _typeof(t);
    })(t);
  }

  function X(t, e) {
    if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
  }

  function tt(t, e) {
    for (var n = 0; n < e.length; n++) {
      var u = e[n];
      u.enumerable = u.enumerable || !1, u.configurable = !0, "value" in u && (u.writable = !0), Object.defineProperty(t, u.key, u);
    }
  }

  function et(t, e, n) {
    return e && tt(t.prototype, e), n && tt(t, n), t;
  }

  function nt(t, e, n) {
    return e in t ? Object.defineProperty(t, e, {
      value: n,
      enumerable: !0,
      configurable: !0,
      writable: !0
    }) : t[e] = n, t;
  }

  function ut(t) {
    for (var e = 1; e < arguments.length; e++) {
      var n = null != arguments[e] ? arguments[e] : {},
          u = Object.keys(n);
      "function" == typeof Object.getOwnPropertySymbols && (u = u.concat(Object.getOwnPropertySymbols(n).filter(function (t) {
        return Object.getOwnPropertyDescriptor(n, t).enumerable;
      }))), u.forEach(function (e) {
        nt(t, e, n[e]);
      });
    }

    return t;
  }

  function it(t, e) {
    if ("function" != typeof e && null !== e) throw new TypeError("Super expression must either be null or a function");
    t.prototype = Object.create(e && e.prototype, {
      constructor: {
        value: t,
        writable: !0,
        configurable: !0
      }
    }), e && at(t, e);
  }

  function rt(t) {
    return (rt = Object.setPrototypeOf ? Object.getPrototypeOf : function (t) {
      return t.__proto__ || Object.getPrototypeOf(t);
    })(t);
  }

  function at(t, e) {
    return (at = Object.setPrototypeOf || function (t, e) {
      return t.__proto__ = e, t;
    })(t, e);
  }

  function st(t, e) {
    if (null == t) return {};

    var n,
        u,
        i = function (t, e) {
      if (null == t) return {};
      var n,
          u,
          i = {},
          r = Object.keys(t);

      for (u = 0; u < r.length; u++) {
        n = r[u], e.indexOf(n) >= 0 || (i[n] = t[n]);
      }

      return i;
    }(t, e);

    if (Object.getOwnPropertySymbols) {
      var r = Object.getOwnPropertySymbols(t);

      for (u = 0; u < r.length; u++) {
        n = r[u], e.indexOf(n) >= 0 || Object.prototype.propertyIsEnumerable.call(t, n) && (i[n] = t[n]);
      }
    }

    return i;
  }

  function ot(t, e) {
    return !e || "object" != _typeof(e) && "function" != typeof e ? function (t) {
      if (void 0 === t) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
      return t;
    }(t) : e;
  }

  function lt(t, e) {
    for (; !Object.prototype.hasOwnProperty.call(t, e) && null !== (t = rt(t));) {
      ;
    }

    return t;
  }

  function ht(t, e, n) {
    return (ht = "undefined" != typeof Reflect && Reflect.get ? Reflect.get : function (t, e, n) {
      var u = lt(t, e);

      if (u) {
        var i = Object.getOwnPropertyDescriptor(u, e);
        return i.get ? i.get.call(n) : i.value;
      }
    })(t, e, n || t);
  }

  function ct(t, e, n, u) {
    return (ct = "undefined" != typeof Reflect && Reflect.set ? Reflect.set : function (t, e, n, u) {
      var i,
          r = lt(t, e);

      if (r) {
        if ((i = Object.getOwnPropertyDescriptor(r, e)).set) return i.set.call(u, n), !0;
        if (!i.writable) return !1;
      }

      if (i = Object.getOwnPropertyDescriptor(u, e)) {
        if (!i.writable) return !1;
        i.value = n, Object.defineProperty(u, e, i);
      } else nt(u, e, n);

      return !0;
    })(t, e, n, u);
  }

  function ft(t, e, n, u, i) {
    if (!ct(t, e, n, u || t) && i) throw new Error("failed to set property");
    return n;
  }

  function pt(t, e) {
    return function (t) {
      if (Array.isArray(t)) return t;
    }(t) || function (t, e) {
      var n = [],
          u = !0,
          i = !1,
          r = void 0;

      try {
        for (var a, s = t[Symbol.iterator](); !(u = (a = s.next()).done) && (n.push(a.value), !e || n.length !== e); u = !0) {
          ;
        }
      } catch (t) {
        i = !0, r = t;
      } finally {
        try {
          u || null == s.return || s.return();
        } finally {
          if (i) throw r;
        }
      }

      return n;
    }(t, e) || function () {
      throw new TypeError("Invalid attempt to destructure non-iterable instance");
    }();
  }

  var dt = {
    NONE: "NONE",
    LEFT: "LEFT",
    FORCE_LEFT: "FORCE_LEFT",
    RIGHT: "RIGHT",
    FORCE_RIGHT: "FORCE_RIGHT"
  };

  function vt(t) {
    return t.replace(/([.*+?^=!:${}()|[\]\/\\])/g, "\\$1");
  }

  var kt = "undefined" != typeof window && window || "undefined" != typeof global && global.global === global && global || "undefined" != typeof self && self.self === self && self || {},
      gt = function () {
    function t(e, n, u, i) {
      for (X(this, t), this.value = e, this.cursorPos = n, this.oldValue = u, this.oldSelection = i; this.value.slice(0, this.startChangePos) !== this.oldValue.slice(0, this.startChangePos);) {
        --this.oldSelection.start;
      }
    }

    return et(t, [{
      key: "startChangePos",
      get: function get() {
        return Math.min(this.cursorPos, this.oldSelection.start);
      }
    }, {
      key: "insertedCount",
      get: function get() {
        return this.cursorPos - this.startChangePos;
      }
    }, {
      key: "inserted",
      get: function get() {
        return this.value.substr(this.startChangePos, this.insertedCount);
      }
    }, {
      key: "removedCount",
      get: function get() {
        return Math.max(this.oldSelection.end - this.startChangePos || this.oldValue.length - this.value.length, 0);
      }
    }, {
      key: "removed",
      get: function get() {
        return this.oldValue.substr(this.startChangePos, this.removedCount);
      }
    }, {
      key: "head",
      get: function get() {
        return this.value.substring(0, this.startChangePos);
      }
    }, {
      key: "tail",
      get: function get() {
        return this.value.substring(this.startChangePos + this.insertedCount);
      }
    }, {
      key: "removeDirection",
      get: function get() {
        return !this.removedCount || this.insertedCount ? dt.NONE : this.oldSelection.end === this.cursorPos || this.oldSelection.start === this.cursorPos ? dt.RIGHT : dt.LEFT;
      }
    }]), t;
  }(),
      yt = function () {
    function t(e) {
      X(this, t), _extends(this, {
        inserted: "",
        rawInserted: "",
        skip: !1,
        tailShift: 0
      }, e);
    }

    return et(t, [{
      key: "aggregate",
      value: function value(t) {
        return this.rawInserted += t.rawInserted, this.skip = this.skip || t.skip, this.inserted += t.inserted, this.tailShift += t.tailShift, this;
      }
    }, {
      key: "offset",
      get: function get() {
        return this.tailShift + this.inserted.length;
      }
    }]), t;
  }(),
      _t = function () {
    function t(e) {
      X(this, t), this._value = "", this._update(e), this.isInitialized = !0;
    }

    return et(t, [{
      key: "updateOptions",
      value: function value(t) {
        Object.keys(t).length && this.withValueRefresh(this._update.bind(this, t));
      }
    }, {
      key: "_update",
      value: function value(t) {
        _extends(this, t);
      }
    }, {
      key: "reset",
      value: function value() {
        this._value = "";
      }
    }, {
      key: "resolve",
      value: function value(t) {
        return this.reset(), this.append(t, {
          input: !0
        }, {
          value: ""
        }), this.doCommit(), this.value;
      }
    }, {
      key: "nearestInputPos",
      value: function value(t, e) {
        return t;
      }
    }, {
      key: "extractInput",
      value: function value() {
        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0,
            e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : this.value.length;
        return this.value.slice(t, e);
      }
    }, {
      key: "extractTail",
      value: function value() {
        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0,
            e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : this.value.length;
        return {
          value: this.extractInput(t, e)
        };
      }
    }, {
      key: "_storeBeforeTailState",
      value: function value() {
        this._beforeTailState = this.state;
      }
    }, {
      key: "_restoreBeforeTailState",
      value: function value() {
        this.state = this._beforeTailState;
      }
    }, {
      key: "_resetBeforeTailState",
      value: function value() {
        this._beforeTailState = null;
      }
    }, {
      key: "appendTail",
      value: function value(t) {
        return this.append(t ? t.value : "", {
          tail: !0
        });
      }
    }, {
      key: "_appendCharRaw",
      value: function value(t) {
        return this._value += t, new yt({
          inserted: t,
          rawInserted: t
        });
      }
    }, {
      key: "_appendChar",
      value: function value(t) {
        var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {},
            n = arguments.length > 2 ? arguments[2] : void 0;
        if (!(t = this.doPrepare(t, e))) return new yt();

        var u = this.state,
            i = this._appendCharRaw(t, e);

        if (i.inserted) {
          var r = !1 !== this.doValidate(e);

          if (r && null != n) {
            this._storeBeforeTailState();

            var a = this.appendTail(n);
            (r = a.rawInserted === n.value) && a.inserted && this._restoreBeforeTailState();
          }

          r || (i.rawInserted = i.inserted = "", this.state = u);
        }

        return i;
      }
    }, {
      key: "append",
      value: function value(t, e, n) {
        this.value.length;

        for (var u = new yt(), i = 0; i < t.length; ++i) {
          u.aggregate(this._appendChar(t[i], e, n));
        }

        return null != n && (this._storeBeforeTailState(), u.tailShift += this.appendTail(n).tailShift), u;
      }
    }, {
      key: "remove",
      value: function value() {
        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0,
            e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : this.value.length;
        return this._value = this.value.slice(0, t) + this.value.slice(e), new yt();
      }
    }, {
      key: "withValueRefresh",
      value: function value(t) {
        if (this._refreshing || !this.isInitialized) return t();
        this._refreshing = !0;
        var e = this.unmaskedValue,
            n = this.value,
            u = t();
        return this.resolve(n) !== n && (this.unmaskedValue = e), delete this._refreshing, u;
      }
    }, {
      key: "doPrepare",
      value: function value(t) {
        var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
        return this.prepare ? this.prepare(t, this, e) : t;
      }
    }, {
      key: "doValidate",
      value: function value(t) {
        return (!this.validate || this.validate(this.value, this, t)) && (!this.parent || this.parent.doValidate(t));
      }
    }, {
      key: "doCommit",
      value: function value() {
        this.commit && this.commit(this.value, this);
      }
    }, {
      key: "splice",
      value: function value(t, e, n, u) {
        var i = t + e,
            r = this.extractTail(i),
            a = this.nearestInputPos(t, u);
        return new yt({
          tailShift: a - t
        }).aggregate(this.remove(a)).aggregate(this.append(n, {
          input: !0
        }, r));
      }
    }, {
      key: "state",
      get: function get() {
        return {
          _value: this.value
        };
      },
      set: function set(t) {
        this._value = t._value;
      }
    }, {
      key: "value",
      get: function get() {
        return this._value;
      },
      set: function set(t) {
        this.resolve(t);
      }
    }, {
      key: "unmaskedValue",
      get: function get() {
        return this.value;
      },
      set: function set(t) {
        this.reset(), this.append(t, {}, {
          value: ""
        }), this.doCommit();
      }
    }, {
      key: "typedValue",
      get: function get() {
        return this.unmaskedValue;
      },
      set: function set(t) {
        this.unmaskedValue = t;
      }
    }, {
      key: "rawInputValue",
      get: function get() {
        return this.extractInput(0, this.value.length, {
          raw: !0
        });
      },
      set: function set(t) {
        this.reset(), this.append(t, {
          raw: !0
        }, {
          value: ""
        }), this.doCommit();
      }
    }, {
      key: "isComplete",
      get: function get() {
        return !0;
      }
    }]), t;
  }();

  function mt(t) {
    if (null == t) throw new Error("mask property should be defined");
    return t instanceof RegExp ? kt.IMask.MaskedRegExp : "string" == typeof (e = t) || e instanceof String ? kt.IMask.MaskedPattern : t instanceof Date || t === Date ? kt.IMask.MaskedDate : t instanceof Number || "number" == typeof t || t === Number ? kt.IMask.MaskedNumber : Array.isArray(t) || t === Array ? kt.IMask.MaskedDynamic : t.prototype instanceof kt.IMask.Masked ? t : t instanceof Function ? kt.IMask.MaskedFunction : (console.warn("Mask not found for mask", t), kt.IMask.Masked);
    var e;
  }

  function At(t) {
    var e = (t = ut({}, t)).mask;
    return e instanceof kt.IMask.Masked ? e : new (mt(e))(t);
  }

  var Ct = {
    0: /\d/,
    a: /[\u0041-\u005A\u0061-\u007A\u00AA\u00B5\u00BA\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u02C1\u02C6-\u02D1\u02E0-\u02E4\u02EC\u02EE\u0370-\u0374\u0376\u0377\u037A-\u037D\u0386\u0388-\u038A\u038C\u038E-\u03A1\u03A3-\u03F5\u03F7-\u0481\u048A-\u0527\u0531-\u0556\u0559\u0561-\u0587\u05D0-\u05EA\u05F0-\u05F2\u0620-\u064A\u066E\u066F\u0671-\u06D3\u06D5\u06E5\u06E6\u06EE\u06EF\u06FA-\u06FC\u06FF\u0710\u0712-\u072F\u074D-\u07A5\u07B1\u07CA-\u07EA\u07F4\u07F5\u07FA\u0800-\u0815\u081A\u0824\u0828\u0840-\u0858\u08A0\u08A2-\u08AC\u0904-\u0939\u093D\u0950\u0958-\u0961\u0971-\u0977\u0979-\u097F\u0985-\u098C\u098F\u0990\u0993-\u09A8\u09AA-\u09B0\u09B2\u09B6-\u09B9\u09BD\u09CE\u09DC\u09DD\u09DF-\u09E1\u09F0\u09F1\u0A05-\u0A0A\u0A0F\u0A10\u0A13-\u0A28\u0A2A-\u0A30\u0A32\u0A33\u0A35\u0A36\u0A38\u0A39\u0A59-\u0A5C\u0A5E\u0A72-\u0A74\u0A85-\u0A8D\u0A8F-\u0A91\u0A93-\u0AA8\u0AAA-\u0AB0\u0AB2\u0AB3\u0AB5-\u0AB9\u0ABD\u0AD0\u0AE0\u0AE1\u0B05-\u0B0C\u0B0F\u0B10\u0B13-\u0B28\u0B2A-\u0B30\u0B32\u0B33\u0B35-\u0B39\u0B3D\u0B5C\u0B5D\u0B5F-\u0B61\u0B71\u0B83\u0B85-\u0B8A\u0B8E-\u0B90\u0B92-\u0B95\u0B99\u0B9A\u0B9C\u0B9E\u0B9F\u0BA3\u0BA4\u0BA8-\u0BAA\u0BAE-\u0BB9\u0BD0\u0C05-\u0C0C\u0C0E-\u0C10\u0C12-\u0C28\u0C2A-\u0C33\u0C35-\u0C39\u0C3D\u0C58\u0C59\u0C60\u0C61\u0C85-\u0C8C\u0C8E-\u0C90\u0C92-\u0CA8\u0CAA-\u0CB3\u0CB5-\u0CB9\u0CBD\u0CDE\u0CE0\u0CE1\u0CF1\u0CF2\u0D05-\u0D0C\u0D0E-\u0D10\u0D12-\u0D3A\u0D3D\u0D4E\u0D60\u0D61\u0D7A-\u0D7F\u0D85-\u0D96\u0D9A-\u0DB1\u0DB3-\u0DBB\u0DBD\u0DC0-\u0DC6\u0E01-\u0E30\u0E32\u0E33\u0E40-\u0E46\u0E81\u0E82\u0E84\u0E87\u0E88\u0E8A\u0E8D\u0E94-\u0E97\u0E99-\u0E9F\u0EA1-\u0EA3\u0EA5\u0EA7\u0EAA\u0EAB\u0EAD-\u0EB0\u0EB2\u0EB3\u0EBD\u0EC0-\u0EC4\u0EC6\u0EDC-\u0EDF\u0F00\u0F40-\u0F47\u0F49-\u0F6C\u0F88-\u0F8C\u1000-\u102A\u103F\u1050-\u1055\u105A-\u105D\u1061\u1065\u1066\u106E-\u1070\u1075-\u1081\u108E\u10A0-\u10C5\u10C7\u10CD\u10D0-\u10FA\u10FC-\u1248\u124A-\u124D\u1250-\u1256\u1258\u125A-\u125D\u1260-\u1288\u128A-\u128D\u1290-\u12B0\u12B2-\u12B5\u12B8-\u12BE\u12C0\u12C2-\u12C5\u12C8-\u12D6\u12D8-\u1310\u1312-\u1315\u1318-\u135A\u1380-\u138F\u13A0-\u13F4\u1401-\u166C\u166F-\u167F\u1681-\u169A\u16A0-\u16EA\u1700-\u170C\u170E-\u1711\u1720-\u1731\u1740-\u1751\u1760-\u176C\u176E-\u1770\u1780-\u17B3\u17D7\u17DC\u1820-\u1877\u1880-\u18A8\u18AA\u18B0-\u18F5\u1900-\u191C\u1950-\u196D\u1970-\u1974\u1980-\u19AB\u19C1-\u19C7\u1A00-\u1A16\u1A20-\u1A54\u1AA7\u1B05-\u1B33\u1B45-\u1B4B\u1B83-\u1BA0\u1BAE\u1BAF\u1BBA-\u1BE5\u1C00-\u1C23\u1C4D-\u1C4F\u1C5A-\u1C7D\u1CE9-\u1CEC\u1CEE-\u1CF1\u1CF5\u1CF6\u1D00-\u1DBF\u1E00-\u1F15\u1F18-\u1F1D\u1F20-\u1F45\u1F48-\u1F4D\u1F50-\u1F57\u1F59\u1F5B\u1F5D\u1F5F-\u1F7D\u1F80-\u1FB4\u1FB6-\u1FBC\u1FBE\u1FC2-\u1FC4\u1FC6-\u1FCC\u1FD0-\u1FD3\u1FD6-\u1FDB\u1FE0-\u1FEC\u1FF2-\u1FF4\u1FF6-\u1FFC\u2071\u207F\u2090-\u209C\u2102\u2107\u210A-\u2113\u2115\u2119-\u211D\u2124\u2126\u2128\u212A-\u212D\u212F-\u2139\u213C-\u213F\u2145-\u2149\u214E\u2183\u2184\u2C00-\u2C2E\u2C30-\u2C5E\u2C60-\u2CE4\u2CEB-\u2CEE\u2CF2\u2CF3\u2D00-\u2D25\u2D27\u2D2D\u2D30-\u2D67\u2D6F\u2D80-\u2D96\u2DA0-\u2DA6\u2DA8-\u2DAE\u2DB0-\u2DB6\u2DB8-\u2DBE\u2DC0-\u2DC6\u2DC8-\u2DCE\u2DD0-\u2DD6\u2DD8-\u2DDE\u2E2F\u3005\u3006\u3031-\u3035\u303B\u303C\u3041-\u3096\u309D-\u309F\u30A1-\u30FA\u30FC-\u30FF\u3105-\u312D\u3131-\u318E\u31A0-\u31BA\u31F0-\u31FF\u3400-\u4DB5\u4E00-\u9FCC\uA000-\uA48C\uA4D0-\uA4FD\uA500-\uA60C\uA610-\uA61F\uA62A\uA62B\uA640-\uA66E\uA67F-\uA697\uA6A0-\uA6E5\uA717-\uA71F\uA722-\uA788\uA78B-\uA78E\uA790-\uA793\uA7A0-\uA7AA\uA7F8-\uA801\uA803-\uA805\uA807-\uA80A\uA80C-\uA822\uA840-\uA873\uA882-\uA8B3\uA8F2-\uA8F7\uA8FB\uA90A-\uA925\uA930-\uA946\uA960-\uA97C\uA984-\uA9B2\uA9CF\uAA00-\uAA28\uAA40-\uAA42\uAA44-\uAA4B\uAA60-\uAA76\uAA7A\uAA80-\uAAAF\uAAB1\uAAB5\uAAB6\uAAB9-\uAABD\uAAC0\uAAC2\uAADB-\uAADD\uAAE0-\uAAEA\uAAF2-\uAAF4\uAB01-\uAB06\uAB09-\uAB0E\uAB11-\uAB16\uAB20-\uAB26\uAB28-\uAB2E\uABC0-\uABE2\uAC00-\uD7A3\uD7B0-\uD7C6\uD7CB-\uD7FB\uF900-\uFA6D\uFA70-\uFAD9\uFB00-\uFB06\uFB13-\uFB17\uFB1D\uFB1F-\uFB28\uFB2A-\uFB36\uFB38-\uFB3C\uFB3E\uFB40\uFB41\uFB43\uFB44\uFB46-\uFBB1\uFBD3-\uFD3D\uFD50-\uFD8F\uFD92-\uFDC7\uFDF0-\uFDFB\uFE70-\uFE74\uFE76-\uFEFC\uFF21-\uFF3A\uFF41-\uFF5A\uFF66-\uFFBE\uFFC2-\uFFC7\uFFCA-\uFFCF\uFFD2-\uFFD7\uFFDA-\uFFDC]/,
    "*": /./
  },
      Et = function () {
    function t(e) {
      X(this, t);
      var n = e.mask,
          u = st(e, ["mask"]);
      this.masked = At({
        mask: n
      }), _extends(this, u);
    }

    return et(t, [{
      key: "reset",
      value: function value() {
        this._isFilled = !1, this.masked.reset();
      }
    }, {
      key: "remove",
      value: function value() {
        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0,
            e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : this.value.length;
        return 0 === t && e >= 1 ? (this._isFilled = !1, this.masked.remove(t, e)) : new yt();
      }
    }, {
      key: "_appendChar",
      value: function value(t) {
        var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
        if (this._isFilled) return new yt();

        var n = this.masked.state,
            u = this.masked._appendChar(t, e);

        return u.inserted && !1 === this.doValidate(e) && (u.inserted = u.rawInserted = "", this.masked.state = n), u.inserted || this.isOptional || this.lazy || e.input || (u.inserted = this.placeholderChar), u.skip = !u.inserted && !this.isOptional, this._isFilled = Boolean(u.inserted), u;
      }
    }, {
      key: "_appendPlaceholder",
      value: function value() {
        var t = new yt();
        return this._isFilled || this.isOptional ? t : (this._isFilled = !0, t.inserted = this.placeholderChar, t);
      }
    }, {
      key: "extractTail",
      value: function value() {
        var t;
        return (t = this.masked).extractTail.apply(t, arguments);
      }
    }, {
      key: "appendTail",
      value: function value() {
        var t;
        return (t = this.masked).appendTail.apply(t, arguments);
      }
    }, {
      key: "extractInput",
      value: function value() {
        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0,
            e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : this.value.length,
            n = arguments.length > 2 ? arguments[2] : void 0;
        return this.masked.extractInput(t, e, n);
      }
    }, {
      key: "nearestInputPos",
      value: function value(t) {
        var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : dt.NONE,
            n = this.value.length,
            u = Math.min(Math.max(t, 0), n);

        switch (e) {
          case dt.LEFT:
          case dt.FORCE_LEFT:
            return this.isComplete ? u : 0;

          case dt.RIGHT:
          case dt.FORCE_RIGHT:
            return this.isComplete ? u : n;

          case dt.NONE:
          default:
            return u;
        }
      }
    }, {
      key: "doValidate",
      value: function value() {
        var t, e;
        return (t = this.masked).doValidate.apply(t, arguments) && (!this.parent || (e = this.parent).doValidate.apply(e, arguments));
      }
    }, {
      key: "doCommit",
      value: function value() {
        this.masked.doCommit();
      }
    }, {
      key: "value",
      get: function get() {
        return this.masked.value || (this._isFilled && !this.isOptional ? this.placeholderChar : "");
      }
    }, {
      key: "unmaskedValue",
      get: function get() {
        return this.masked.unmaskedValue;
      }
    }, {
      key: "isComplete",
      get: function get() {
        return Boolean(this.masked.value) || this.isOptional;
      }
    }, {
      key: "state",
      get: function get() {
        return {
          masked: this.masked.state,
          _isFilled: this._isFilled
        };
      },
      set: function set(t) {
        this.masked.state = t.masked, this._isFilled = t._isFilled;
      }
    }]), t;
  }(),
      Ft = function () {
    function t(e) {
      X(this, t), _extends(this, e), this._value = "";
    }

    return et(t, [{
      key: "reset",
      value: function value() {
        this._isRawInput = !1, this._value = "";
      }
    }, {
      key: "remove",
      value: function value() {
        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0,
            e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : this._value.length;
        return this._value = this._value.slice(0, t) + this._value.slice(e), this._value || (this._isRawInput = !1), new yt();
      }
    }, {
      key: "nearestInputPos",
      value: function value(t) {
        var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : dt.NONE,
            n = this._value.length;

        switch (e) {
          case dt.LEFT:
          case dt.FORCE_LEFT:
            return 0;

          case dt.NONE:
          case dt.RIGHT:
          case dt.FORCE_RIGHT:
          default:
            return n;
        }
      }
    }, {
      key: "extractInput",
      value: function value() {
        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0,
            e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : this._value.length;
        return (arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {}).raw && this._isRawInput && this._value.slice(t, e) || "";
      }
    }, {
      key: "_appendChar",
      value: function value(t, e) {
        var n = new yt();
        if (this._value) return n;
        var u = this.char === t[0] && (this.isUnmasking || e.input || e.raw) && !e.tail;
        return u && (n.rawInserted = this.char), this._value = n.inserted = this.char, this._isRawInput = u && (e.raw || e.input), n;
      }
    }, {
      key: "_appendPlaceholder",
      value: function value() {
        var t = new yt();
        return this._value ? t : (this._value = t.inserted = this.char, t);
      }
    }, {
      key: "extractTail",
      value: function value() {
        arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : this.value.length;
        return {
          value: ""
        };
      }
    }, {
      key: "appendTail",
      value: function value(t) {
        return this._appendChar(t ? t.value : "", {
          tail: !0
        });
      }
    }, {
      key: "doCommit",
      value: function value() {}
    }, {
      key: "value",
      get: function get() {
        return this._value;
      }
    }, {
      key: "unmaskedValue",
      get: function get() {
        return this.isUnmasking ? this.value : "";
      }
    }, {
      key: "isComplete",
      get: function get() {
        return !0;
      }
    }, {
      key: "state",
      get: function get() {
        return {
          _value: this._value,
          _isRawInput: this._isRawInput
        };
      },
      set: function set(t) {
        _extends(this, t);
      }
    }]), t;
  }(),
      bt = function () {
    function t(e) {
      X(this, t), this.chunks = e;
    }

    return et(t, [{
      key: "value",
      get: function get() {
        return this.chunks.map(function (t) {
          return t.value;
        }).join("");
      }
    }]), t;
  }(),
      Bt = function (t) {
    function e() {
      var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
      return X(this, e), t.definitions = _extends({}, Ct, t.definitions), ot(this, rt(e).call(this, ut({}, e.DEFAULTS, t)));
    }

    return it(e, _t), et(e, [{
      key: "_update",
      value: function value() {
        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
        t.definitions = _extends({}, this.definitions, t.definitions), ht(rt(e.prototype), "_update", this).call(this, t), this._rebuildMask();
      }
    }, {
      key: "_rebuildMask",
      value: function value() {
        var t = this,
            n = this.definitions;
        this._blocks = [], this._stops = [], this._maskedBlocks = {};
        var u = this.mask;
        if (u && n) for (var i = !1, r = !1, a = 0; a < u.length; ++a) {
          if (this.blocks) if ("continue" === function () {
            var e = u.slice(a),
                n = Object.keys(t.blocks).filter(function (t) {
              return 0 === e.indexOf(t);
            });
            n.sort(function (t, e) {
              return e.length - t.length;
            });
            var i = n[0];

            if (i) {
              var r = At(ut({
                parent: t,
                lazy: t.lazy,
                placeholderChar: t.placeholderChar
              }, t.blocks[i]));
              return r && (t._blocks.push(r), t._maskedBlocks[i] || (t._maskedBlocks[i] = []), t._maskedBlocks[i].push(t._blocks.length - 1)), a += i.length - 1, "continue";
            }
          }()) continue;
          var s = u[a],
              o = s in n;
          if (s !== e.STOP_CHAR) {
            if ("{" !== s && "}" !== s) {
              if ("[" !== s && "]" !== s) {
                if (s === e.ESCAPE_CHAR) {
                  if (!(s = u[++a])) break;
                  o = !1;
                }

                var l = void 0;
                l = o ? new Et({
                  parent: this,
                  lazy: this.lazy,
                  placeholderChar: this.placeholderChar,
                  mask: n[s],
                  isOptional: r
                }) : new Ft({
                  char: s,
                  isUnmasking: i
                }), this._blocks.push(l);
              } else r = !r;
            } else i = !i;
          } else this._stops.push(this._blocks.length);
        }
      }
    }, {
      key: "_storeBeforeTailState",
      value: function value() {
        this._blocks.forEach(function (t) {
          "function" == typeof t._storeBeforeTailState && t._storeBeforeTailState();
        }), ht(rt(e.prototype), "_storeBeforeTailState", this).call(this);
      }
    }, {
      key: "_restoreBeforeTailState",
      value: function value() {
        this._blocks.forEach(function (t) {
          "function" == typeof t._restoreBeforeTailState && t._restoreBeforeTailState();
        }), ht(rt(e.prototype), "_restoreBeforeTailState", this).call(this);
      }
    }, {
      key: "_resetBeforeTailState",
      value: function value() {
        this._blocks.forEach(function (t) {
          "function" == typeof t._resetBeforeTailState && t._resetBeforeTailState();
        }), ht(rt(e.prototype), "_resetBeforeTailState", this).call(this);
      }
    }, {
      key: "reset",
      value: function value() {
        ht(rt(e.prototype), "reset", this).call(this), this._blocks.forEach(function (t) {
          return t.reset();
        });
      }
    }, {
      key: "doCommit",
      value: function value() {
        this._blocks.forEach(function (t) {
          return t.doCommit();
        }), ht(rt(e.prototype), "doCommit", this).call(this);
      }
    }, {
      key: "appendTail",
      value: function value(t) {
        var n = new yt();
        return t && n.aggregate(t instanceof bt ? this._appendTailChunks(t.chunks) : ht(rt(e.prototype), "appendTail", this).call(this, t)), n.aggregate(this._appendPlaceholder());
      }
    }, {
      key: "_appendCharRaw",
      value: function value(t) {
        var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {},
            n = this._mapPosToBlock(this.value.length),
            u = new yt();

        if (!n) return u;

        for (var i = n.index;; ++i) {
          var r = this._blocks[i];
          if (!r) break;

          var a = r._appendChar(t, e),
              s = a.skip;

          if (u.aggregate(a), s || a.rawInserted) break;
        }

        return u;
      }
    }, {
      key: "_appendTailChunks",
      value: function value(t) {
        for (var e = new yt(), n = 0; n < t.length && !e.skip; ++n) {
          var u = t[n],
              i = this._mapPosToBlock(this.value.length),
              r = u instanceof bt && null != u.index && (!i || i.index <= u.index) && this._blocks[u.index];

          if (r) {
            e.aggregate(this._appendPlaceholder(u.index));
            var a = r.appendTail(u);
            a.skip = !1, e.aggregate(a), this._value += a.inserted;
            var s = u.value.slice(a.rawInserted.length);
            s && e.aggregate(this.append(s, {
              tail: !0
            }));
          } else {
            var o = u,
                l = o.stop,
                h = o.value;
            null != l && this._stops.indexOf(l) >= 0 && e.aggregate(this._appendPlaceholder(l)), e.aggregate(this.append(h, {
              tail: !0
            }));
          }
        }

        return e;
      }
    }, {
      key: "extractTail",
      value: function value() {
        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0,
            e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : this.value.length;
        return new bt(this._extractTailChunks(t, e));
      }
    }, {
      key: "extractInput",
      value: function value() {
        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0,
            e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : this.value.length,
            n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {};
        if (t === e) return "";
        var u = "";
        return this._forEachBlocksInRange(t, e, function (t, e, i, r) {
          u += t.extractInput(i, r, n);
        }), u;
      }
    }, {
      key: "_extractTailChunks",
      value: function value() {
        var t = this,
            e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0,
            n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : this.value.length;
        if (e === n) return [];
        var u,
            i = [];
        return this._forEachBlocksInRange(e, n, function (e, n, r, a) {
          for (var s, o = e.extractTail(r, a), l = 0; l < t._stops.length; ++l) {
            var h = t._stops[l];
            if (!(h <= n)) break;
            s = h;
          }

          if (o instanceof bt) {
            if (null == s) {
              for (var c = o.chunks.length, f = 0; f < o.chunks.length; ++f) {
                if (null != o.chunks[f].stop) {
                  c = f;
                  break;
                }
              }

              o.chunks.splice(0, c).filter(function (t) {
                return t.value;
              }).forEach(function (t) {
                u ? u.value += t.value : u = {
                  value: t.value
                };
              });
            }

            o.chunks.length && (u && i.push(u), o.index = s, i.push(o), u = null);
          } else {
            if (null != s) u && i.push(u), o.stop = s;else if (u) return void (u.value += o.value);
            u = o;
          }
        }), u && u.value && i.push(u), i;
      }
    }, {
      key: "_appendPlaceholder",
      value: function value(t) {
        var e = this,
            n = new yt();
        if (this.lazy && null == t) return n;

        var u = this._mapPosToBlock(this.value.length);

        if (!u) return n;
        var i = u.index,
            r = null != t ? t : this._blocks.length;
        return this._blocks.slice(i, r).forEach(function (t) {
          if ("function" == typeof t._appendPlaceholder) {
            var u = null != t._blocks ? [t._blocks.length] : [],
                i = t._appendPlaceholder.apply(t, u);

            e._value += i.inserted, n.aggregate(i);
          }
        }), n;
      }
    }, {
      key: "_mapPosToBlock",
      value: function value(t) {
        for (var e = "", n = 0; n < this._blocks.length; ++n) {
          var u = this._blocks[n],
              i = e.length;
          if (t <= (e += u.value).length) return {
            index: n,
            offset: t - i
          };
        }
      }
    }, {
      key: "_blockStartPos",
      value: function value(t) {
        return this._blocks.slice(0, t).reduce(function (t, e) {
          return t + e.value.length;
        }, 0);
      }
    }, {
      key: "_forEachBlocksInRange",
      value: function value(t) {
        var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : this.value.length,
            n = arguments.length > 2 ? arguments[2] : void 0,
            u = this._mapPosToBlock(t);

        if (u) {
          var i = this._mapPosToBlock(e),
              r = i && u.index === i.index,
              a = u.offset,
              s = i && r ? i.offset : void 0;

          if (n(this._blocks[u.index], u.index, a, s), i && !r) {
            for (var o = u.index + 1; o < i.index; ++o) {
              n(this._blocks[o], o);
            }

            n(this._blocks[i.index], i.index, 0, i.offset);
          }
        }
      }
    }, {
      key: "remove",
      value: function value() {
        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0,
            n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : this.value.length,
            u = ht(rt(e.prototype), "remove", this).call(this, t, n);
        return this._forEachBlocksInRange(t, n, function (t, e, n, i) {
          u.aggregate(t.remove(n, i));
        }), u;
      }
    }, {
      key: "nearestInputPos",
      value: function value(t) {
        var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : dt.NONE,
            n = this._mapPosToBlock(t) || {
          index: 0,
          offset: 0
        },
            u = n.offset,
            i = n.index,
            r = this._blocks[i];
        if (!r) return t;
        var a = u;
        0 !== a && a < r.value.length && (a = r.nearestInputPos(u, function (t) {
          switch (t) {
            case dt.LEFT:
              return dt.FORCE_LEFT;

            case dt.RIGHT:
              return dt.FORCE_RIGHT;

            default:
              return t;
          }
        }(e)));
        var s = a === r.value.length;
        if (!(0 === a) && !s) return this._blockStartPos(i) + a;
        var o = s ? i + 1 : i;

        if (e === dt.NONE) {
          if (o > 0) {
            var l = o - 1,
                h = this._blocks[l],
                c = h.nearestInputPos(0, dt.NONE);
            if (!h.value.length || c !== h.value.length) return this._blockStartPos(o);
          }

          for (var f = o; f < this._blocks.length; ++f) {
            var p = this._blocks[f],
                d = p.nearestInputPos(0, dt.NONE);
            if (d !== p.value.length) return this._blockStartPos(f) + d;
          }

          return this.value.length;
        }

        if (e === dt.LEFT || e === dt.FORCE_LEFT) {
          for (var v, k = o; k < this._blocks.length; ++k) {
            if (this._blocks[k].value) {
              v = k;
              break;
            }
          }

          if (null != v) {
            var g = this._blocks[v],
                y = g.nearestInputPos(0, dt.RIGHT);
            if (0 === y && g.unmaskedValue.length) return this._blockStartPos(v) + y;
          }

          for (var _, m = -1, A = o - 1; A >= 0; --A) {
            var C = this._blocks[A],
                E = C.nearestInputPos(C.value.length, dt.FORCE_LEFT);

            if (null != _ || C.value && 0 === E || (_ = A), 0 !== E) {
              if (E !== C.value.length) return this._blockStartPos(A) + E;
              m = A;
              break;
            }
          }

          if (e === dt.LEFT) for (var F = m + 1; F <= Math.min(o, this._blocks.length - 1); ++F) {
            var b = this._blocks[F],
                B = b.nearestInputPos(0, dt.NONE),
                D = this._blockStartPos(F) + B;
            if ((!b.value.length && D === this.value.length || B !== b.value.length) && D <= t) return D;
          }
          if (m >= 0) return this._blockStartPos(m) + this._blocks[m].value.length;
          if (e === dt.FORCE_LEFT || this.lazy && !this.extractInput() && !function (t) {
            if (!t) return !1;
            var e = t.value;
            return !e || t.nearestInputPos(0, dt.NONE) !== e.length;
          }(this._blocks[o])) return 0;
          if (null != _) return this._blockStartPos(_);

          for (var S = o; S < this._blocks.length; ++S) {
            var T = this._blocks[S],
                w = T.nearestInputPos(0, dt.NONE);
            if (!T.value.length || w !== T.value.length) return this._blockStartPos(S) + w;
          }

          return 0;
        }

        if (e === dt.RIGHT || e === dt.FORCE_RIGHT) {
          for (var x, O, M = o; M < this._blocks.length; ++M) {
            var P = this._blocks[M],
                I = P.nearestInputPos(0, dt.NONE);

            if (I !== P.value.length) {
              O = this._blockStartPos(M) + I, x = M;
              break;
            }
          }

          if (null != x && null != O) {
            for (var R = x; R < this._blocks.length; ++R) {
              var V = this._blocks[R],
                  j = V.nearestInputPos(0, dt.FORCE_RIGHT);
              if (j !== V.value.length) return this._blockStartPos(R) + j;
            }

            return e === dt.FORCE_RIGHT ? this.value.length : O;
          }

          for (var N = Math.min(o, this._blocks.length - 1); N >= 0; --N) {
            var L = this._blocks[N],
                H = L.nearestInputPos(L.value.length, dt.LEFT);

            if (0 !== H) {
              var G = this._blockStartPos(N) + H;
              if (G >= t) return G;
              break;
            }
          }
        }

        return t;
      }
    }, {
      key: "maskedBlock",
      value: function value(t) {
        return this.maskedBlocks(t)[0];
      }
    }, {
      key: "maskedBlocks",
      value: function value(t) {
        var e = this,
            n = this._maskedBlocks[t];
        return n ? n.map(function (t) {
          return e._blocks[t];
        }) : [];
      }
    }, {
      key: "state",
      get: function get() {
        return ut({}, ht(rt(e.prototype), "state", this), {
          _blocks: this._blocks.map(function (t) {
            return t.state;
          })
        });
      },
      set: function set(t) {
        var n = t._blocks,
            u = st(t, ["_blocks"]);
        this._blocks.forEach(function (t, e) {
          return t.state = n[e];
        }), ft(rt(e.prototype), "state", u, this, !0);
      }
    }, {
      key: "isComplete",
      get: function get() {
        return this._blocks.every(function (t) {
          return t.isComplete;
        });
      }
    }, {
      key: "unmaskedValue",
      get: function get() {
        return this._blocks.reduce(function (t, e) {
          return t + e.unmaskedValue;
        }, "");
      },
      set: function set(t) {
        ft(rt(e.prototype), "unmaskedValue", t, this, !0);
      }
    }, {
      key: "value",
      get: function get() {
        return this._blocks.reduce(function (t, e) {
          return t + e.value;
        }, "");
      },
      set: function set(t) {
        ft(rt(e.prototype), "value", t, this, !0);
      }
    }]), e;
  }();

  Bt.DEFAULTS = {
    lazy: !0,
    placeholderChar: "_"
  }, Bt.STOP_CHAR = "`", Bt.ESCAPE_CHAR = "\\", Bt.InputDefinition = Et, Bt.FixedDefinition = Ft;

  var Dt = function (t) {
    function e() {
      return X(this, e), ot(this, rt(e).apply(this, arguments));
    }

    return it(e, Bt), et(e, [{
      key: "_update",
      value: function value(t) {
        t = ut({
          to: this.to || 0,
          from: this.from || 0
        }, t);
        var n = String(t.to).length;
        null != t.maxLength && (n = Math.max(n, t.maxLength)), t.maxLength = n;

        for (var u = String(t.to).padStart(n, "0"), i = String(t.from).padStart(n, "0"), r = 0; r < u.length && u[r] === i[r];) {
          ++r;
        }

        t.mask = u.slice(0, r).replace(/0/g, "\\0") + "0".repeat(n - r), ht(rt(e.prototype), "_update", this).call(this, t);
      }
    }, {
      key: "doValidate",
      value: function value() {
        var t,
            n = this.value,
            u = "",
            i = "",
            r = pt(n.match(/^(\D*)(\d*)(\D*)/) || [], 3),
            a = r[1],
            s = r[2];
        if (s && (u = "0".repeat(a.length) + s, i = "9".repeat(a.length) + s), -1 === n.search(/[^0]/) && n.length <= this._matchFrom) return !0;
        u = u.padEnd(this.maxLength, "0"), i = i.padEnd(this.maxLength, "9");

        for (var o = arguments.length, l = new Array(o), h = 0; h < o; h++) {
          l[h] = arguments[h];
        }

        return this.from <= Number(i) && Number(u) <= this.to && (t = ht(rt(e.prototype), "doValidate", this)).call.apply(t, [this].concat(l));
      }
    }, {
      key: "_matchFrom",
      get: function get() {
        return this.maxLength - String(this.from).length;
      }
    }, {
      key: "isComplete",
      get: function get() {
        return ht(rt(e.prototype), "isComplete", this) && Boolean(this.value);
      }
    }]), e;
  }(),
      St = function (t) {
    function e(t) {
      return X(this, e), ot(this, rt(e).call(this, ut({}, e.DEFAULTS, t)));
    }

    return it(e, Bt), et(e, [{
      key: "_update",
      value: function value(t) {
        t.mask === Date && delete t.mask, t.pattern && (t.mask = t.pattern, delete t.pattern);
        var n = t.blocks;
        t.blocks = _extends({}, e.GET_DEFAULT_BLOCKS()), t.min && (t.blocks.Y.from = t.min.getFullYear()), t.max && (t.blocks.Y.to = t.max.getFullYear()), t.min && t.max && t.blocks.Y.from === t.blocks.Y.to && (t.blocks.m.from = t.min.getMonth() + 1, t.blocks.m.to = t.max.getMonth() + 1, t.blocks.m.from === t.blocks.m.to && (t.blocks.d.from = t.min.getDate(), t.blocks.d.to = t.max.getDate())), _extends(t.blocks, n), ht(rt(e.prototype), "_update", this).call(this, t);
      }
    }, {
      key: "doValidate",
      value: function value() {
        for (var t, n = this.date, u = arguments.length, i = new Array(u), r = 0; r < u; r++) {
          i[r] = arguments[r];
        }

        return (t = ht(rt(e.prototype), "doValidate", this)).call.apply(t, [this].concat(i)) && (!this.isComplete || this.isDateExist(this.value) && null != n && (null == this.min || this.min <= n) && (null == this.max || n <= this.max));
      }
    }, {
      key: "isDateExist",
      value: function value(t) {
        return this.format(this.parse(t)) === t;
      }
    }, {
      key: "date",
      get: function get() {
        return this.isComplete ? this.parse(this.value) : null;
      },
      set: function set(t) {
        this.value = this.format(t);
      }
    }, {
      key: "typedValue",
      get: function get() {
        return this.date;
      },
      set: function set(t) {
        this.date = t;
      }
    }]), e;
  }();

  St.DEFAULTS = {
    pattern: "d{.}`m{.}`Y",
    format: function format(t) {
      return [String(t.getDate()).padStart(2, "0"), String(t.getMonth() + 1).padStart(2, "0"), t.getFullYear()].join(".");
    },
    parse: function parse(t) {
      var e = pt(t.split("."), 3),
          n = e[0],
          u = e[1],
          i = e[2];
      return new Date(i, u - 1, n);
    }
  }, St.GET_DEFAULT_BLOCKS = function () {
    return {
      d: {
        mask: Dt,
        from: 1,
        to: 31,
        maxLength: 2
      },
      m: {
        mask: Dt,
        from: 1,
        to: 12,
        maxLength: 2
      },
      Y: {
        mask: Dt,
        from: 1900,
        to: 9999
      }
    };
  };

  var Tt = function () {
    function t() {
      X(this, t);
    }

    return et(t, [{
      key: "select",
      value: function value(t, e) {
        if (null != t && null != e && (t !== this.selectionStart || e !== this.selectionEnd)) try {
          this._unsafeSelect(t, e);
        } catch (t) {}
      }
    }, {
      key: "_unsafeSelect",
      value: function value(t, e) {}
    }, {
      key: "bindEvents",
      value: function value(t) {}
    }, {
      key: "unbindEvents",
      value: function value() {}
    }, {
      key: "selectionStart",
      get: function get() {
        var t;

        try {
          t = this._unsafeSelectionStart;
        } catch (t) {}

        return null != t ? t : this.value.length;
      }
    }, {
      key: "selectionEnd",
      get: function get() {
        var t;

        try {
          t = this._unsafeSelectionEnd;
        } catch (t) {}

        return null != t ? t : this.value.length;
      }
    }, {
      key: "isActive",
      get: function get() {
        return !1;
      }
    }]), t;
  }(),
      wt = function (t) {
    function e(t) {
      var n;
      return X(this, e), (n = ot(this, rt(e).call(this))).input = t, n._handlers = {}, n;
    }

    return it(e, Tt), et(e, [{
      key: "_unsafeSelect",
      value: function value(t, e) {
        this.input.setSelectionRange(t, e);
      }
    }, {
      key: "bindEvents",
      value: function value(t) {
        var n = this;
        Object.keys(t).forEach(function (u) {
          return n._toggleEventHandler(e.EVENTS_MAP[u], t[u]);
        });
      }
    }, {
      key: "unbindEvents",
      value: function value() {
        var t = this;
        Object.keys(this._handlers).forEach(function (e) {
          return t._toggleEventHandler(e);
        });
      }
    }, {
      key: "_toggleEventHandler",
      value: function value(t, e) {
        this._handlers[t] && (this.input.removeEventListener(t, this._handlers[t]), delete this._handlers[t]), e && (this.input.addEventListener(t, e), this._handlers[t] = e);
      }
    }, {
      key: "isActive",
      get: function get() {
        return this.input === document.activeElement;
      }
    }, {
      key: "_unsafeSelectionStart",
      get: function get() {
        return this.input.selectionStart;
      }
    }, {
      key: "_unsafeSelectionEnd",
      get: function get() {
        return this.input.selectionEnd;
      }
    }, {
      key: "value",
      get: function get() {
        return this.input.value;
      },
      set: function set(t) {
        this.input.value = t;
      }
    }]), e;
  }();

  wt.EVENTS_MAP = {
    selectionChange: "keydown",
    input: "input",
    drop: "drop",
    click: "click",
    focus: "focus",
    commit: "change"
  };

  var xt = function () {
    function t(e, n) {
      X(this, t), this.el = e instanceof Tt ? e : new wt(e), this.masked = At(n), this._listeners = {}, this._value = "", this._unmaskedValue = "", this._saveSelection = this._saveSelection.bind(this), this._onInput = this._onInput.bind(this), this._onChange = this._onChange.bind(this), this._onDrop = this._onDrop.bind(this), this.alignCursor = this.alignCursor.bind(this), this.alignCursorFriendly = this.alignCursorFriendly.bind(this), this._bindEvents(), this.updateValue(), this._onChange();
    }

    return et(t, [{
      key: "_bindEvents",
      value: function value() {
        this.el.bindEvents({
          selectionChange: this._saveSelection,
          input: this._onInput,
          drop: this._onDrop,
          click: this.alignCursorFriendly,
          focus: this.alignCursorFriendly,
          commit: this._onChange
        });
      }
    }, {
      key: "_unbindEvents",
      value: function value() {
        this.el.unbindEvents();
      }
    }, {
      key: "_fireEvent",
      value: function value(t) {
        var e = this._listeners[t];
        e && e.forEach(function (t) {
          return t();
        });
      }
    }, {
      key: "_saveSelection",
      value: function value() {
        this.value !== this.el.value && console.warn("Element value was changed outside of mask. Syncronize mask using `mask.updateValue()` to work properly."), this._selection = {
          start: this.selectionStart,
          end: this.cursorPos
        };
      }
    }, {
      key: "updateValue",
      value: function value() {
        this.masked.value = this.el.value, this._value = this.masked.value;
      }
    }, {
      key: "updateControl",
      value: function value() {
        var t = this.masked.unmaskedValue,
            e = this.masked.value,
            n = this.unmaskedValue !== t || this.value !== e;
        this._unmaskedValue = t, this._value = e, this.el.value !== e && (this.el.value = e), n && this._fireChangeEvents();
      }
    }, {
      key: "updateOptions",
      value: function value(t) {
        if (!function t(e, n) {
          if (n === e) return !0;
          var u,
              i = Array.isArray(n),
              r = Array.isArray(e);

          if (i && r) {
            if (n.length != e.length) return !1;

            for (u = 0; u < n.length; u++) {
              if (!t(n[u], e[u])) return !1;
            }

            return !0;
          }

          if (i != r) return !1;

          if (n && e && "object" === Q(n) && "object" === Q(e)) {
            var a = n instanceof Date,
                s = e instanceof Date;
            if (a && s) return n.getTime() == e.getTime();
            if (a != s) return !1;
            var o = n instanceof RegExp,
                l = e instanceof RegExp;
            if (o && l) return n.toString() == e.toString();
            if (o != l) return !1;
            var h = Object.keys(n);

            for (u = 0; u < h.length; u++) {
              if (!Object.prototype.hasOwnProperty.call(e, h[u])) return !1;
            }

            for (u = 0; u < h.length; u++) {
              if (!t(e[h[u]], n[h[u]])) return !1;
            }

            return !0;
          }

          return !1;
        }(this.masked, t)) {
          var e = t.mask,
              n = st(t, ["mask"]);
          this.mask = e, this.masked.updateOptions(n), this.updateControl();
        }
      }
    }, {
      key: "updateCursor",
      value: function value(t) {
        null != t && (this.cursorPos = t, this._delayUpdateCursor(t));
      }
    }, {
      key: "_delayUpdateCursor",
      value: function value(t) {
        var e = this;
        this._abortUpdateCursor(), this._changingCursorPos = t, this._cursorChanging = setTimeout(function () {
          e.el && (e.cursorPos = e._changingCursorPos, e._abortUpdateCursor());
        }, 10);
      }
    }, {
      key: "_fireChangeEvents",
      value: function value() {
        this._fireEvent("accept"), this.masked.isComplete && this._fireEvent("complete");
      }
    }, {
      key: "_abortUpdateCursor",
      value: function value() {
        this._cursorChanging && (clearTimeout(this._cursorChanging), delete this._cursorChanging);
      }
    }, {
      key: "alignCursor",
      value: function value() {
        this.cursorPos = this.masked.nearestInputPos(this.cursorPos, dt.LEFT);
      }
    }, {
      key: "alignCursorFriendly",
      value: function value() {
        this.selectionStart === this.cursorPos && this.alignCursor();
      }
    }, {
      key: "on",
      value: function value(t, e) {
        return this._listeners[t] || (this._listeners[t] = []), this._listeners[t].push(e), this;
      }
    }, {
      key: "off",
      value: function value(t, e) {
        if (this._listeners[t]) {
          if (e) {
            var n = this._listeners[t].indexOf(e);

            return n >= 0 && this._listeners[t].splice(n, 1), this;
          }

          delete this._listeners[t];
        }
      }
    }, {
      key: "_onInput",
      value: function value() {
        if (this._abortUpdateCursor(), !this._selection) return this.updateValue();
        var t = new gt(this.el.value, this.cursorPos, this.value, this._selection),
            e = this.masked.rawInputValue,
            n = this.masked.splice(t.startChangePos, t.removed.length, t.inserted, t.removeDirection).offset,
            u = e === this.masked.rawInputValue ? t.removeDirection : dt.NONE,
            i = this.masked.nearestInputPos(t.startChangePos + n, u);
        this.updateControl(), this.updateCursor(i);
      }
    }, {
      key: "_onChange",
      value: function value() {
        this.value !== this.el.value && this.updateValue(), this.masked.doCommit(), this.updateControl();
      }
    }, {
      key: "_onDrop",
      value: function value(t) {
        t.preventDefault(), t.stopPropagation();
      }
    }, {
      key: "destroy",
      value: function value() {
        this._unbindEvents(), this._listeners.length = 0, delete this.el;
      }
    }, {
      key: "mask",
      get: function get() {
        return this.masked.mask;
      },
      set: function set(t) {
        if (!(null == t || t === this.masked.mask || t === Date && this.masked instanceof St)) if (this.masked.constructor !== mt(t)) {
          var e = At({
            mask: t
          });
          e.unmaskedValue = this.masked.unmaskedValue, this.masked = e;
        } else this.masked.updateOptions({
          mask: t
        });
      }
    }, {
      key: "value",
      get: function get() {
        return this._value;
      },
      set: function set(t) {
        this.masked.value = t, this.updateControl(), this.alignCursor();
      }
    }, {
      key: "unmaskedValue",
      get: function get() {
        return this._unmaskedValue;
      },
      set: function set(t) {
        this.masked.unmaskedValue = t, this.updateControl(), this.alignCursor();
      }
    }, {
      key: "typedValue",
      get: function get() {
        return this.masked.typedValue;
      },
      set: function set(t) {
        this.masked.typedValue = t, this.updateControl(), this.alignCursor();
      }
    }, {
      key: "selectionStart",
      get: function get() {
        return this._cursorChanging ? this._changingCursorPos : this.el.selectionStart;
      }
    }, {
      key: "cursorPos",
      get: function get() {
        return this._cursorChanging ? this._changingCursorPos : this.el.selectionEnd;
      },
      set: function set(t) {
        this.el.isActive && (this.el.select(t, t), this._saveSelection());
      }
    }]), t;
  }(),
      Ot = function (t) {
    function e() {
      return X(this, e), ot(this, rt(e).apply(this, arguments));
    }

    return it(e, Bt), et(e, [{
      key: "_update",
      value: function value(t) {
        t.enum && (t.mask = "*".repeat(t.enum[0].length)), ht(rt(e.prototype), "_update", this).call(this, t);
      }
    }, {
      key: "doValidate",
      value: function value() {
        for (var t, n = this, u = arguments.length, i = new Array(u), r = 0; r < u; r++) {
          i[r] = arguments[r];
        }

        return this.enum.some(function (t) {
          return t.indexOf(n.unmaskedValue) >= 0;
        }) && (t = ht(rt(e.prototype), "doValidate", this)).call.apply(t, [this].concat(i));
      }
    }]), e;
  }(),
      Mt = function (t) {
    function e(t) {
      return X(this, e), ot(this, rt(e).call(this, ut({}, e.DEFAULTS, t)));
    }

    return it(e, _t), et(e, [{
      key: "_update",
      value: function value(t) {
        ht(rt(e.prototype), "_update", this).call(this, t), this._updateRegExps();
      }
    }, {
      key: "_updateRegExps",
      value: function value() {
        var t = "",
            e = "";
        this.allowNegative ? (t += "([+|\\-]?|([+|\\-]?(0|([1-9]+\\d*))))", e += "[+|\\-]?") : t += "(0|([1-9]+\\d*))", e += "\\d*";
        var n = (this.scale ? "(" + vt(this.radix) + "\\d{0," + this.scale + "})?" : "") + "$";
        this._numberRegExpInput = new RegExp("^" + t + n), this._numberRegExp = new RegExp("^" + e + n), this._mapToRadixRegExp = new RegExp("[" + this.mapToRadix.map(vt).join("") + "]", "g"), this._thousandsSeparatorRegExp = new RegExp(vt(this.thousandsSeparator), "g");
      }
    }, {
      key: "extractTail",
      value: function value() {
        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0,
            n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : this.value.length,
            u = ht(rt(e.prototype), "extractTail", this).call(this, t, n);
        return ut({}, u, {
          value: this._removeThousandsSeparators(u.value)
        });
      }
    }, {
      key: "_removeThousandsSeparators",
      value: function value(t) {
        return t.replace(this._thousandsSeparatorRegExp, "");
      }
    }, {
      key: "_insertThousandsSeparators",
      value: function value(t) {
        var e = t.split(this.radix);
        return e[0] = e[0].replace(/\B(?=(\d{3})+(?!\d))/g, this.thousandsSeparator), e.join(this.radix);
      }
    }, {
      key: "doPrepare",
      value: function value(t) {
        for (var n, u = arguments.length, i = new Array(u > 1 ? u - 1 : 0), r = 1; r < u; r++) {
          i[r - 1] = arguments[r];
        }

        return (n = ht(rt(e.prototype), "doPrepare", this)).call.apply(n, [this, this._removeThousandsSeparators(t.replace(this._mapToRadixRegExp, this.radix))].concat(i));
      }
    }, {
      key: "_separatorsCount",
      value: function value() {
        for (var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : this._value, e = this._removeThousandsSeparators(t).length, n = e, u = 0; u <= n; ++u) {
          this._value[u] === this.thousandsSeparator && ++n;
        }

        return n - e;
      }
    }, {
      key: "extractInput",
      value: function value() {
        for (var t, n = arguments.length, u = new Array(n), i = 0; i < n; i++) {
          u[i] = arguments[i];
        }

        return this._removeThousandsSeparators((t = ht(rt(e.prototype), "extractInput", this)).call.apply(t, [this].concat(u)));
      }
    }, {
      key: "_appendCharRaw",
      value: function value(t) {
        var n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
        if (!this.thousandsSeparator) return ht(rt(e.prototype), "_appendCharRaw", this).call(this, t, n);

        var u = this._separatorsCount(n.tail && this._beforeTailState ? this._beforeTailState._value : this._value);

        this._value = this._removeThousandsSeparators(this.value);
        var i = ht(rt(e.prototype), "_appendCharRaw", this).call(this, t, n);
        this._value = this._insertThousandsSeparators(this._value);

        var r = this._separatorsCount(n.tail && this._beforeTailState ? this._beforeTailState._value : this._value);

        return i.tailShift += r - u, i;
      }
    }, {
      key: "remove",
      value: function value() {
        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0,
            e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : this.value.length,
            n = this.value.slice(0, t),
            u = this.value.slice(e),
            i = this._separatorsCount(n);

        this._value = this._insertThousandsSeparators(this._removeThousandsSeparators(n + u));

        var r = this._separatorsCount(n);

        return new yt({
          tailShift: r - i
        });
      }
    }, {
      key: "nearestInputPos",
      value: function value(t, e) {
        if (!e || e === dt.LEFT) return t;

        var n = function (t, e) {
          return e === dt.LEFT && --t, t;
        }(t, e);

        return this.value[n] === this.thousandsSeparator && (t = function (t, e) {
          switch (e) {
            case dt.LEFT:
            case dt.FORCE_LEFT:
              return --t;

            case dt.RIGHT:
            case dt.FORCE_RIGHT:
              return ++t;

            default:
              return t;
          }
        }(t, e)), t;
      }
    }, {
      key: "doValidate",
      value: function value(t) {
        var n = (t.input ? this._numberRegExpInput : this._numberRegExp).test(this._removeThousandsSeparators(this.value));

        if (n) {
          var u = this.number;
          n = n && !isNaN(u) && (null == this.min || this.min >= 0 || this.min <= this.number) && (null == this.max || this.max <= 0 || this.number <= this.max);
        }

        return n && ht(rt(e.prototype), "doValidate", this).call(this, t);
      }
    }, {
      key: "doCommit",
      value: function value() {
        var t = this.number,
            n = t;
        null != this.min && (n = Math.max(n, this.min)), null != this.max && (n = Math.min(n, this.max)), n !== t && (this.unmaskedValue = String(n));
        var u = this.value;
        this.normalizeZeros && (u = this._normalizeZeros(u)), this.padFractionalZeros && (u = this._padFractionalZeros(u)), this._value = this._insertThousandsSeparators(u), ht(rt(e.prototype), "doCommit", this).call(this);
      }
    }, {
      key: "_normalizeZeros",
      value: function value(t) {
        var e = this._removeThousandsSeparators(t).split(this.radix);

        return e[0] = e[0].replace(/^(\D*)(0*)(\d*)/, function (t, e, n, u) {
          return e + u;
        }), t.length && !/\d$/.test(e[0]) && (e[0] = e[0] + "0"), e.length > 1 && (e[1] = e[1].replace(/0*$/, ""), e[1].length || (e.length = 1)), this._insertThousandsSeparators(e.join(this.radix));
      }
    }, {
      key: "_padFractionalZeros",
      value: function value(t) {
        if (!t) return t;
        var e = t.split(this.radix);
        return e.length < 2 && e.push(""), e[1] = e[1].padEnd(this.scale, "0"), e.join(this.radix);
      }
    }, {
      key: "unmaskedValue",
      get: function get() {
        return this._removeThousandsSeparators(this._normalizeZeros(this.value)).replace(this.radix, ".");
      },
      set: function set(t) {
        ft(rt(e.prototype), "unmaskedValue", t.replace(".", this.radix), this, !0);
      }
    }, {
      key: "number",
      get: function get() {
        return Number(this.unmaskedValue);
      },
      set: function set(t) {
        this.unmaskedValue = String(t);
      }
    }, {
      key: "typedValue",
      get: function get() {
        return this.number;
      },
      set: function set(t) {
        this.number = t;
      }
    }, {
      key: "allowNegative",
      get: function get() {
        return this.signed || null != this.min && this.min < 0 || null != this.max && this.max < 0;
      }
    }]), e;
  }();

  Mt.DEFAULTS = {
    radix: ",",
    thousandsSeparator: "",
    mapToRadix: ["."],
    scale: 2,
    signed: !1,
    normalizeZeros: !0,
    padFractionalZeros: !1
  };

  var Pt = function (t) {
    function e() {
      return X(this, e), ot(this, rt(e).apply(this, arguments));
    }

    return it(e, _t), et(e, [{
      key: "_update",
      value: function value(t) {
        t.mask && (t.validate = function (e) {
          return e.search(t.mask) >= 0;
        }), ht(rt(e.prototype), "_update", this).call(this, t);
      }
    }]), e;
  }(),
      It = function (t) {
    function e() {
      return X(this, e), ot(this, rt(e).apply(this, arguments));
    }

    return it(e, _t), et(e, [{
      key: "_update",
      value: function value(t) {
        t.mask && (t.validate = t.mask), ht(rt(e.prototype), "_update", this).call(this, t);
      }
    }]), e;
  }(),
      Rt = function (t) {
    function e(t) {
      var n;
      return X(this, e), (n = ot(this, rt(e).call(this, ut({}, e.DEFAULTS, t)))).currentMask = null, n;
    }

    return it(e, _t), et(e, [{
      key: "_update",
      value: function value(t) {
        ht(rt(e.prototype), "_update", this).call(this, t), "mask" in t && (this.compiledMasks = Array.isArray(t.mask) ? t.mask.map(function (t) {
          return At(t);
        }) : []);
      }
    }, {
      key: "_appendCharRaw",
      value: function value() {
        var t,
            e = this._applyDispatch.apply(this, arguments);

        this.currentMask && e.aggregate((t = this.currentMask)._appendChar.apply(t, arguments));
        return e;
      }
    }, {
      key: "_applyDispatch",
      value: function value() {
        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "",
            e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {},
            n = e.tail && this._beforeTailState ? this._beforeTailState._value : this.value,
            u = this.rawInputValue,
            i = e.tail && this._beforeTailState ? this._beforeTailState._rawInputValue : u,
            r = u.slice(i.length),
            a = this.currentMask,
            s = new yt(),
            o = a && a.state,
            l = a && a._beforeTailState;
        if (this.currentMask = this.doDispatch(t, e), this.currentMask) if (this.currentMask !== a) {
          this.currentMask.reset();
          var h = this.currentMask.append(i, {
            raw: !0
          });
          s.tailShift = h.inserted.length - n.length, r && (s.tailShift += this.currentMask.append(r, {
            raw: !0,
            tail: !0
          }).tailShift);
        } else this.currentMask.state = o, this.currentMask._beforeTailState = l;
        return s;
      }
    }, {
      key: "doDispatch",
      value: function value(t) {
        var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
        return this.dispatch(t, this, e);
      }
    }, {
      key: "doValidate",
      value: function value() {
        for (var t, n, u = arguments.length, i = new Array(u), r = 0; r < u; r++) {
          i[r] = arguments[r];
        }

        return (t = ht(rt(e.prototype), "doValidate", this)).call.apply(t, [this].concat(i)) && (!this.currentMask || (n = this.currentMask).doValidate.apply(n, i));
      }
    }, {
      key: "reset",
      value: function value() {
        this.currentMask && this.currentMask.reset(), this.compiledMasks.forEach(function (t) {
          return t.reset();
        });
      }
    }, {
      key: "remove",
      value: function value() {
        var t,
            e = new yt();
        this.currentMask && e.aggregate((t = this.currentMask).remove.apply(t, arguments)).aggregate(this._applyDispatch());
        return e;
      }
    }, {
      key: "extractInput",
      value: function value() {
        var t;
        return this.currentMask ? (t = this.currentMask).extractInput.apply(t, arguments) : "";
      }
    }, {
      key: "extractTail",
      value: function value() {
        for (var t, n, u = arguments.length, i = new Array(u), r = 0; r < u; r++) {
          i[r] = arguments[r];
        }

        return this.currentMask ? (t = this.currentMask).extractTail.apply(t, i) : (n = ht(rt(e.prototype), "extractTail", this)).call.apply(n, [this].concat(i));
      }
    }, {
      key: "doCommit",
      value: function value() {
        this.currentMask && this.currentMask.doCommit(), ht(rt(e.prototype), "doCommit", this).call(this);
      }
    }, {
      key: "nearestInputPos",
      value: function value() {
        for (var t, n, u = arguments.length, i = new Array(u), r = 0; r < u; r++) {
          i[r] = arguments[r];
        }

        return this.currentMask ? (t = this.currentMask).nearestInputPos.apply(t, i) : (n = ht(rt(e.prototype), "nearestInputPos", this)).call.apply(n, [this].concat(i));
      }
    }, {
      key: "value",
      get: function get() {
        return this.currentMask ? this.currentMask.value : "";
      },
      set: function set(t) {
        ft(rt(e.prototype), "value", t, this, !0);
      }
    }, {
      key: "unmaskedValue",
      get: function get() {
        return this.currentMask ? this.currentMask.unmaskedValue : "";
      },
      set: function set(t) {
        ft(rt(e.prototype), "unmaskedValue", t, this, !0);
      }
    }, {
      key: "typedValue",
      get: function get() {
        return this.currentMask ? this.currentMask.typedValue : "";
      },
      set: function set(t) {
        var e = String(t);
        this.currentMask && (this.currentMask.typedValue = t, e = this.currentMask.unmaskedValue), this.unmaskedValue = e;
      }
    }, {
      key: "isComplete",
      get: function get() {
        return !!this.currentMask && this.currentMask.isComplete;
      }
    }, {
      key: "state",
      get: function get() {
        return ut({}, ht(rt(e.prototype), "state", this), {
          _rawInputValue: this.rawInputValue,
          compiledMasks: this.compiledMasks.map(function (t) {
            return t.state;
          }),
          currentMaskRef: this.currentMask,
          currentMask: this.currentMask && this.currentMask.state
        });
      },
      set: function set(t) {
        var n = t.compiledMasks,
            u = t.currentMaskRef,
            i = t.currentMask,
            r = st(t, ["compiledMasks", "currentMaskRef", "currentMask"]);
        this.compiledMasks.forEach(function (t, e) {
          return t.state = n[e];
        }), null != u && (this.currentMask = u, this.currentMask.state = i), ft(rt(e.prototype), "state", r, this, !0);
      }
    }]), e;
  }();

  function Vt(t) {
    var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
    return new xt(t, e);
  }

  return Rt.DEFAULTS = {
    dispatch: function dispatch(t, e, n) {
      if (e.compiledMasks.length) {
        var u = e.rawInputValue,
            i = e.compiledMasks.map(function (e, i) {
          return e.rawInputValue = u, e.append(t, n), {
            weight: e.rawInputValue.length,
            index: i
          };
        });
        return i.sort(function (t, e) {
          return e.weight - t.weight;
        }), e.compiledMasks[i[0].index];
      }
    }
  }, Vt.InputMask = xt, Vt.Masked = _t, Vt.MaskedPattern = Bt, Vt.MaskedEnum = Ot, Vt.MaskedRange = Dt, Vt.MaskedNumber = Mt, Vt.MaskedDate = St, Vt.MaskedRegExp = Pt, Vt.MaskedFunction = It, Vt.MaskedDynamic = Rt, Vt.createMask = At, Vt.MaskElement = Tt, Vt.HTMLMaskElement = wt, kt.IMask = Vt, Vt;
});
"use strict";

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/**
 * SimpleBar.js - v4.0.0-alpha.5
 * Scrollbars, simpler.
 * https://grsmto.github.io/simplebar/
 *
 * Made by Adrien Denat from a fork by Jonathan Nicol
 * Under MIT License
 */
!function (t, e) {
  "object" == (typeof exports === "undefined" ? "undefined" : _typeof(exports)) && "undefined" != typeof module ? module.exports = e() : "function" == typeof define && define.amd ? define(e) : (t = t || self).SimpleBar = e();
}(void 0, function () {
  "use strict";

  var t = function t(_t2) {
    if ("function" != typeof _t2) throw TypeError(String(_t2) + " is not a function");
    return _t2;
  },
      e = function e(t) {
    try {
      return !!t();
    } catch (t) {
      return !0;
    }
  },
      i = {}.toString,
      r = function r(t) {
    return i.call(t).slice(8, -1);
  },
      n = "".split,
      s = e(function () {
    return !Object("z").propertyIsEnumerable(0);
  }) ? function (t) {
    return "String" == r(t) ? n.call(t, "") : Object(t);
  } : Object,
      o = function o(t) {
    if (null == t) throw TypeError("Can't call method on " + t);
    return t;
  },
      a = function a(t) {
    return Object(o(t));
  },
      l = Math.ceil,
      c = Math.floor,
      u = function u(t) {
    return isNaN(t = +t) ? 0 : (t > 0 ? c : l)(t);
  },
      h = Math.min,
      f = function f(t) {
    return t > 0 ? h(u(t), 9007199254740991) : 0;
  },
      d = function d(t) {
    return "object" == _typeof(t) ? null !== t : "function" == typeof t;
  },
      p = Array.isArray || function (t) {
    return "Array" == r(t);
  },
      v = "undefined" != typeof globalThis ? globalThis : "undefined" != typeof window ? window : "undefined" != typeof global ? global : "undefined" != typeof self ? self : {};

  function g(t, e) {
    return t(e = {
      exports: {}
    }, e.exports), e.exports;
  }

  var b,
      m,
      y,
      x,
      E = "object" == (typeof window === "undefined" ? "undefined" : _typeof(window)) && window && window.Math == Math ? window : "object" == (typeof self === "undefined" ? "undefined" : _typeof(self)) && self && self.Math == Math ? self : Function("return this")(),
      w = !e(function () {
    return 7 != Object.defineProperty({}, "a", {
      get: function get() {
        return 7;
      }
    }).a;
  }),
      O = E.document,
      _ = d(O) && d(O.createElement),
      S = !w && !e(function () {
    return 7 != Object.defineProperty((t = "div", _ ? O.createElement(t) : {}), "a", {
      get: function get() {
        return 7;
      }
    }).a;
    var t;
  }),
      A = function A(t) {
    if (!d(t)) throw TypeError(String(t) + " is not an object");
    return t;
  },
      L = function L(t, e) {
    if (!d(t)) return t;
    var i, r;
    if (e && "function" == typeof (i = t.toString) && !d(r = i.call(t))) return r;
    if ("function" == typeof (i = t.valueOf) && !d(r = i.call(t))) return r;
    if (!e && "function" == typeof (i = t.toString) && !d(r = i.call(t))) return r;
    throw TypeError("Can't convert object to primitive value");
  },
      M = Object.defineProperty,
      k = {
    f: w ? M : function (t, e, i) {
      if (A(t), e = L(e, !0), A(i), S) try {
        return M(t, e, i);
      } catch (t) {}
      if ("get" in i || "set" in i) throw TypeError("Accessors not supported");
      return "value" in i && (t[e] = i.value), t;
    }
  },
      W = function W(t, e) {
    return {
      enumerable: !(1 & t),
      configurable: !(2 & t),
      writable: !(4 & t),
      value: e
    };
  },
      T = w ? function (t, e, i) {
    return k.f(t, e, W(1, i));
  } : function (t, e, i) {
    return t[e] = i, t;
  },
      j = function j(t, e) {
    try {
      T(E, t, e);
    } catch (i) {
      E[t] = e;
    }

    return e;
  },
      R = g(function (t) {
    var e = E["__core-js_shared__"] || j("__core-js_shared__", {});
    (t.exports = function (t, i) {
      return e[t] || (e[t] = void 0 !== i ? i : {});
    })("versions", []).push({
      version: "3.0.1",
      mode: "global",
      copyright: "© 2019 Denis Pushkarev (zloirock.ru)"
    });
  }),
      N = 0,
      z = Math.random(),
      C = function C(t) {
    return "Symbol(".concat(void 0 === t ? "" : t, ")_", (++N + z).toString(36));
  },
      V = !e(function () {
    return !String(Symbol());
  }),
      D = R("wks"),
      B = E.Symbol,
      I = function I(t) {
    return D[t] || (D[t] = V && B[t] || (V ? B : C)("Symbol." + t));
  },
      P = I("species"),
      H = function H(t, e) {
    var i;
    return p(t) && ("function" != typeof (i = t.constructor) || i !== Array && !p(i.prototype) ? d(i) && null === (i = i[P]) && (i = void 0) : i = void 0), new (void 0 === i ? Array : i)(0 === e ? 0 : e);
  },
      q = function q(e, i) {
    var r = 1 == e,
        n = 2 == e,
        o = 3 == e,
        l = 4 == e,
        c = 6 == e,
        u = 5 == e || c,
        h = i || H;
    return function (i, d, p) {
      for (var v, g, b = a(i), m = s(b), y = function (e, i, r) {
        if (t(e), void 0 === i) return e;

        switch (r) {
          case 0:
            return function () {
              return e.call(i);
            };

          case 1:
            return function (t) {
              return e.call(i, t);
            };

          case 2:
            return function (t, r) {
              return e.call(i, t, r);
            };

          case 3:
            return function (t, r, n) {
              return e.call(i, t, r, n);
            };
        }

        return function () {
          return e.apply(i, arguments);
        };
      }(d, p, 3), x = f(m.length), E = 0, w = r ? h(i, x) : n ? h(i, 0) : void 0; x > E; E++) {
        if ((u || E in m) && (g = y(v = m[E], E, b), e)) if (r) w[E] = g;else if (g) switch (e) {
          case 3:
            return !0;

          case 5:
            return v;

          case 6:
            return E;

          case 2:
            w.push(v);
        } else if (l) return !1;
      }

      return c ? -1 : o || l ? l : w;
    };
  },
      F = I("species"),
      $ = {}.propertyIsEnumerable,
      X = Object.getOwnPropertyDescriptor,
      Y = {
    f: X && !$.call({
      1: 2
    }, 1) ? function (t) {
      var e = X(this, t);
      return !!e && e.enumerable;
    } : $
  },
      G = function G(t) {
    return s(o(t));
  },
      K = {}.hasOwnProperty,
      U = function U(t, e) {
    return K.call(t, e);
  },
      J = Object.getOwnPropertyDescriptor,
      Q = {
    f: w ? J : function (t, e) {
      if (t = G(t), e = L(e, !0), S) try {
        return J(t, e);
      } catch (t) {}
      if (U(t, e)) return W(!Y.f.call(t, e), t[e]);
    }
  },
      Z = R("native-function-to-string", Function.toString),
      tt = E.WeakMap,
      et = "function" == typeof tt && /native code/.test(Z.call(tt)),
      it = R("keys"),
      rt = {},
      nt = E.WeakMap;

  if (et) {
    var st = new nt(),
        ot = st.get,
        at = st.has,
        lt = st.set;
    b = function b(t, e) {
      return lt.call(st, t, e), e;
    }, m = function m(t) {
      return ot.call(st, t) || {};
    }, y = function y(t) {
      return at.call(st, t);
    };
  } else {
    var ct = it[x = "state"] || (it[x] = C(x));
    rt[ct] = !0, b = function b(t, e) {
      return T(t, ct, e), e;
    }, m = function m(t) {
      return U(t, ct) ? t[ct] : {};
    }, y = function y(t) {
      return U(t, ct);
    };
  }

  var ut,
      ht,
      ft = {
    set: b,
    get: m,
    has: y,
    enforce: function enforce(t) {
      return y(t) ? m(t) : b(t, {});
    },
    getterFor: function getterFor(t) {
      return function (e) {
        var i;
        if (!d(e) || (i = m(e)).type !== t) throw TypeError("Incompatible receiver, " + t + " required");
        return i;
      };
    }
  },
      dt = g(function (t) {
    var e = ft.get,
        i = ft.enforce,
        r = String(Z).split("toString");
    R("inspectSource", function (t) {
      return Z.call(t);
    }), (t.exports = function (t, e, n, s) {
      var o = !!s && !!s.unsafe,
          a = !!s && !!s.enumerable,
          l = !!s && !!s.noTargetGet;
      "function" == typeof n && ("string" != typeof e || U(n, "name") || T(n, "name", e), i(n).source = r.join("string" == typeof e ? e : "")), t !== E ? (o ? !l && t[e] && (a = !0) : delete t[e], a ? t[e] = n : T(t, e, n)) : a ? t[e] = n : j(e, n);
    })(Function.prototype, "toString", function () {
      return "function" == typeof this && e(this).source || Z.call(this);
    });
  }),
      pt = Math.max,
      vt = Math.min,
      gt = (ut = !1, function (t, e, i) {
    var r,
        n = G(t),
        s = f(n.length),
        o = function (t, e) {
      var i = u(t);
      return i < 0 ? pt(i + e, 0) : vt(i, e);
    }(i, s);

    if (ut && e != e) {
      for (; s > o;) {
        if ((r = n[o++]) != r) return !0;
      }
    } else for (; s > o; o++) {
      if ((ut || o in n) && n[o] === e) return ut || o || 0;
    }

    return !ut && -1;
  }),
      bt = function bt(t, e) {
    var i,
        r = G(t),
        n = 0,
        s = [];

    for (i in r) {
      !U(rt, i) && U(r, i) && s.push(i);
    }

    for (; e.length > n;) {
      U(r, i = e[n++]) && (~gt(s, i) || s.push(i));
    }

    return s;
  },
      mt = ["constructor", "hasOwnProperty", "isPrototypeOf", "propertyIsEnumerable", "toLocaleString", "toString", "valueOf"],
      yt = mt.concat("length", "prototype"),
      xt = {
    f: Object.getOwnPropertyNames || function (t) {
      return bt(t, yt);
    }
  },
      Et = {
    f: Object.getOwnPropertySymbols
  },
      wt = E.Reflect,
      Ot = wt && wt.ownKeys || function (t) {
    var e = xt.f(A(t)),
        i = Et.f;
    return i ? e.concat(i(t)) : e;
  },
      _t = function _t(t, e) {
    for (var i = Ot(e), r = k.f, n = Q.f, s = 0; s < i.length; s++) {
      var o = i[s];
      U(t, o) || r(t, o, n(e, o));
    }
  },
      St = /#|\.prototype\./,
      At = function At(t, i) {
    var r = Mt[Lt(t)];
    return r == Wt || r != kt && ("function" == typeof i ? e(i) : !!i);
  },
      Lt = At.normalize = function (t) {
    return String(t).replace(St, ".").toLowerCase();
  },
      Mt = At.data = {},
      kt = At.NATIVE = "N",
      Wt = At.POLYFILL = "P",
      Tt = At,
      jt = Q.f,
      Rt = function Rt(t, e) {
    var i,
        r,
        n,
        s,
        o,
        a = t.target,
        l = t.global,
        c = t.stat;
    if (i = l ? E : c ? E[a] || j(a, {}) : (E[a] || {}).prototype) for (r in e) {
      if (s = e[r], n = t.noTargetGet ? (o = jt(i, r)) && o.value : i[r], !Tt(l ? r : a + (c ? "." : "#") + r, t.forced) && void 0 !== n) {
        if (_typeof(s) == _typeof(n)) continue;

        _t(s, n);
      }

      (t.sham || n && n.sham) && T(s, "sham", !0), dt(i, r, s, t);
    }
  },
      Nt = q(2);

  Rt({
    target: "Array",
    proto: !0,
    forced: !(ht = "filter", !e(function () {
      var t = [];
      return (t.constructor = {})[F] = function () {
        return {
          foo: 1
        };
      }, 1 !== t[ht](Boolean).foo;
    }))
  }, {
    filter: function filter(t) {
      return Nt(this, t, arguments[1]);
    }
  });

  var zt = function zt(t, i) {
    var r = [][t];
    return !r || !e(function () {
      r.call(null, i || function () {
        throw 1;
      }, 1);
    });
  },
      Ct = [].forEach,
      Vt = q(0),
      Dt = zt("forEach") ? function (t) {
    return Vt(this, t, arguments[1]);
  } : Ct;

  Rt({
    target: "Array",
    proto: !0,
    forced: [].forEach != Dt
  }, {
    forEach: Dt
  });
  Rt({
    target: "Array",
    proto: !0,
    forced: zt("reduce")
  }, {
    reduce: function reduce(e) {
      return function (e, i, r, n, o) {
        t(i);
        var l = a(e),
            c = s(l),
            u = f(l.length),
            h = o ? u - 1 : 0,
            d = o ? -1 : 1;
        if (r < 2) for (;;) {
          if (h in c) {
            n = c[h], h += d;
            break;
          }

          if (h += d, o ? h < 0 : u <= h) throw TypeError("Reduce of empty array with no initial value");
        }

        for (; o ? h >= 0 : u > h; h += d) {
          h in c && (n = i(n, c[h], h, l));
        }

        return n;
      }(this, e, arguments.length, arguments[1], !1);
    }
  });
  var Bt = k.f,
      It = Function.prototype,
      Pt = It.toString,
      Ht = /^\s*function ([^ (]*)/;
  !w || "name" in It || Bt(It, "name", {
    configurable: !0,
    get: function get() {
      try {
        return Pt.call(this).match(Ht)[1];
      } catch (t) {
        return "";
      }
    }
  });

  var qt = Object.keys || function (t) {
    return bt(t, mt);
  },
      Ft = Object.assign,
      $t = !Ft || e(function () {
    var t = {},
        e = {},
        i = Symbol();
    return t[i] = 7, "abcdefghijklmnopqrst".split("").forEach(function (t) {
      e[t] = t;
    }), 7 != Ft({}, t)[i] || "abcdefghijklmnopqrst" != qt(Ft({}, e)).join("");
  }) ? function (t, e) {
    for (var i = a(t), r = arguments.length, n = 1, o = Et.f, l = Y.f; r > n;) {
      for (var c, u = s(arguments[n++]), h = o ? qt(u).concat(o(u)) : qt(u), f = h.length, d = 0; f > d;) {
        l.call(u, c = h[d++]) && (i[c] = u[c]);
      }
    }

    return i;
  } : Ft;

  Rt({
    target: "Object",
    stat: !0,
    forced: Object.assign !== $t
  }, {
    assign: $t
  });
  var Xt = "\t\n\x0B\f\r  \u1680\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u202F\u205F\u3000\u2028\u2029\uFEFF",
      Yt = "[" + Xt + "]",
      Gt = RegExp("^" + Yt + Yt + "*"),
      Kt = RegExp(Yt + Yt + "*$"),
      Ut = E.parseInt,
      Jt = /^[-+]?0[xX]/,
      Qt = 8 !== Ut(Xt + "08") || 22 !== Ut(Xt + "0x16") ? function (t, e) {
    var i = function (t, e) {
      return t = String(o(t)), 1 & e && (t = t.replace(Gt, "")), 2 & e && (t = t.replace(Kt, "")), t;
    }(String(t), 3);

    return Ut(i, e >>> 0 || (Jt.test(i) ? 16 : 10));
  } : Ut;
  Rt({
    global: !0,
    forced: parseInt != Qt
  }, {
    parseInt: Qt
  });
  var Zt,
      te,
      ee = RegExp.prototype.exec,
      ie = String.prototype.replace,
      re = ee,
      ne = (Zt = /a/, te = /b*/g, ee.call(Zt, "a"), ee.call(te, "a"), 0 !== Zt.lastIndex || 0 !== te.lastIndex),
      se = void 0 !== /()??/.exec("")[1];
  (ne || se) && (re = function re(t) {
    var e,
        i,
        r,
        n,
        s = this;
    return se && (i = new RegExp("^" + s.source + "$(?!\\s)", function () {
      var t = A(this),
          e = "";
      return t.global && (e += "g"), t.ignoreCase && (e += "i"), t.multiline && (e += "m"), t.unicode && (e += "u"), t.sticky && (e += "y"), e;
    }.call(s))), ne && (e = s.lastIndex), r = ee.call(s, t), ne && r && (s.lastIndex = s.global ? r.index + r[0].length : e), se && r && r.length > 1 && ie.call(r[0], i, function () {
      for (n = 1; n < arguments.length - 2; n++) {
        void 0 === arguments[n] && (r[n] = void 0);
      }
    }), r;
  });
  var oe = re;
  Rt({
    target: "RegExp",
    proto: !0,
    forced: /./.exec !== oe
  }, {
    exec: oe
  });

  var ae = function ae(t, e, i) {
    return e + (i ? function (t, e, i) {
      var r,
          n,
          s = String(o(t)),
          a = u(e),
          l = s.length;
      return a < 0 || a >= l ? i ? "" : void 0 : (r = s.charCodeAt(a)) < 55296 || r > 56319 || a + 1 === l || (n = s.charCodeAt(a + 1)) < 56320 || n > 57343 ? i ? s.charAt(a) : r : i ? s.slice(a, a + 2) : n - 56320 + (r - 55296 << 10) + 65536;
    }(t, e, !0).length : 1);
  },
      le = function le(t, e) {
    var i = t.exec;

    if ("function" == typeof i) {
      var n = i.call(t, e);
      if ("object" != _typeof(n)) throw TypeError("RegExp exec method returned something other than an Object or null");
      return n;
    }

    if ("RegExp" !== r(t)) throw TypeError("RegExp#exec called on incompatible receiver");
    return oe.call(t, e);
  },
      ce = I("species"),
      ue = !e(function () {
    var t = /./;
    return t.exec = function () {
      var t = [];
      return t.groups = {
        a: "7"
      }, t;
    }, "7" !== "".replace(t, "$<a>");
  }),
      he = !e(function () {
    var t = /(?:)/,
        e = t.exec;

    t.exec = function () {
      return e.apply(this, arguments);
    };

    var i = "ab".split(t);
    return 2 !== i.length || "a" !== i[0] || "b" !== i[1];
  }),
      fe = function fe(t, i, r, n) {
    var s = I(t),
        o = !e(function () {
      var e = {};
      return e[s] = function () {
        return 7;
      }, 7 != ""[t](e);
    }),
        a = o && !e(function () {
      var e = !1,
          i = /a/;
      return i.exec = function () {
        return e = !0, null;
      }, "split" === t && (i.constructor = {}, i.constructor[ce] = function () {
        return i;
      }), i[s](""), !e;
    });

    if (!o || !a || "replace" === t && !ue || "split" === t && !he) {
      var l = /./[s],
          c = r(s, ""[t], function (t, e, i, r, n) {
        return e.exec === oe ? o && !n ? {
          done: !0,
          value: l.call(e, i, r)
        } : {
          done: !0,
          value: t.call(i, e, r)
        } : {
          done: !1
        };
      }),
          u = c[0],
          h = c[1];
      dt(String.prototype, t, u), dt(RegExp.prototype, s, 2 == i ? function (t, e) {
        return h.call(t, this, e);
      } : function (t) {
        return h.call(t, this);
      }), n && T(RegExp.prototype[s], "sham", !0);
    }
  };

  fe("match", 1, function (t, e, i) {
    return [function (e) {
      var i = o(this),
          r = null == e ? void 0 : e[t];
      return void 0 !== r ? r.call(e, i) : new RegExp(e)[t](String(i));
    }, function (t) {
      var r = i(e, t, this);
      if (r.done) return r.value;
      var n = A(t),
          s = String(this);
      if (!n.global) return le(n, s);
      var o = n.unicode;
      n.lastIndex = 0;

      for (var a, l = [], c = 0; null !== (a = le(n, s));) {
        var u = String(a[0]);
        l[c] = u, "" === u && (n.lastIndex = ae(s, f(n.lastIndex), o)), c++;
      }

      return 0 === c ? null : l;
    }];
  });
  var de = Math.max,
      pe = Math.min,
      ve = Math.floor,
      ge = /\$([$&`']|\d\d?|<[^>]*>)/g,
      be = /\$([$&`']|\d\d?)/g;
  fe("replace", 2, function (t, e, i) {
    return [function (i, r) {
      var n = o(this),
          s = null == i ? void 0 : i[t];
      return void 0 !== s ? s.call(i, n, r) : e.call(String(n), i, r);
    }, function (t, n) {
      var s = i(e, t, this, n);
      if (s.done) return s.value;
      var o = A(t),
          a = String(this),
          l = "function" == typeof n;
      l || (n = String(n));
      var c = o.global;

      if (c) {
        var h = o.unicode;
        o.lastIndex = 0;
      }

      for (var d = [];;) {
        var p = le(o, a);
        if (null === p) break;
        if (d.push(p), !c) break;
        "" === String(p[0]) && (o.lastIndex = ae(a, f(o.lastIndex), h));
      }

      for (var v, g = "", b = 0, m = 0; m < d.length; m++) {
        p = d[m];

        for (var y = String(p[0]), x = de(pe(u(p.index), a.length), 0), E = [], w = 1; w < p.length; w++) {
          E.push(void 0 === (v = p[w]) ? v : String(v));
        }

        var O = p.groups;

        if (l) {
          var _ = [y].concat(E, x, a);

          void 0 !== O && _.push(O);
          var S = String(n.apply(void 0, _));
        } else S = r(y, a, x, E, O, n);

        x >= b && (g += a.slice(b, x) + S, b = x + y.length);
      }

      return g + a.slice(b);
    }];

    function r(t, i, r, n, s, o) {
      var l = r + t.length,
          c = n.length,
          u = be;
      return void 0 !== s && (s = a(s), u = ge), e.call(o, u, function (e, o) {
        var a;

        switch (o.charAt(0)) {
          case "$":
            return "$";

          case "&":
            return t;

          case "`":
            return i.slice(0, r);

          case "'":
            return i.slice(l);

          case "<":
            a = s[o.slice(1, -1)];
            break;

          default:
            var u = +o;
            if (0 === u) return e;

            if (u > c) {
              var h = ve(u / 10);
              return 0 === h ? e : h <= c ? void 0 === n[h - 1] ? o.charAt(1) : n[h - 1] + o.charAt(1) : e;
            }

            a = n[u - 1];
        }

        return void 0 === a ? "" : a;
      });
    }
  });

  for (var me in {
    CSSRuleList: 0,
    CSSStyleDeclaration: 0,
    CSSValueList: 0,
    ClientRectList: 0,
    DOMRectList: 0,
    DOMStringList: 0,
    DOMTokenList: 1,
    DataTransferItemList: 0,
    FileList: 0,
    HTMLAllCollection: 0,
    HTMLCollection: 0,
    HTMLFormElement: 0,
    HTMLSelectElement: 0,
    MediaList: 0,
    MimeTypeArray: 0,
    NamedNodeMap: 0,
    NodeList: 1,
    PaintRequestList: 0,
    Plugin: 0,
    PluginArray: 0,
    SVGLengthList: 0,
    SVGNumberList: 0,
    SVGPathSegList: 0,
    SVGPointList: 0,
    SVGStringList: 0,
    SVGTransformList: 0,
    SourceBufferList: 0,
    StyleSheetList: 0,
    TextTrackCueList: 0,
    TextTrackList: 0,
    TouchList: 0
  }) {
    var ye = E[me],
        xe = ye && ye.prototype;
    if (xe && xe.forEach !== Dt) try {
      T(xe, "forEach", Dt);
    } catch (t) {
      xe.forEach = Dt;
    }
  }

  var Ee = g(function (t, e) {
    t.exports = function () {
      if ("undefined" == typeof document) return 0;
      var t,
          e = document.body,
          i = document.createElement("div"),
          r = i.style;
      return r.position = "absolute", r.top = r.left = "-9999px", r.width = r.height = "100px", r.overflow = "scroll", e.appendChild(i), t = i.offsetWidth - i.clientWidth, e.removeChild(i), t;
    };
  }),
      we = "Expected a function",
      Oe = NaN,
      _e = "[object Symbol]",
      Se = /^\s+|\s+$/g,
      Ae = /^[-+]0x[0-9a-f]+$/i,
      Le = /^0b[01]+$/i,
      Me = /^0o[0-7]+$/i,
      ke = parseInt,
      We = "object" == _typeof(v) && v && v.Object === Object && v,
      Te = "object" == (typeof self === "undefined" ? "undefined" : _typeof(self)) && self && self.Object === Object && self,
      je = We || Te || Function("return this")(),
      Re = Object.prototype.toString,
      Ne = Math.max,
      ze = Math.min,
      Ce = function Ce() {
    return je.Date.now();
  };

  function Ve(t, e, i) {
    var r,
        n,
        s,
        o,
        a,
        l,
        c = 0,
        u = !1,
        h = !1,
        f = !0;
    if ("function" != typeof t) throw new TypeError(we);

    function d(e) {
      var i = r,
          s = n;
      return r = n = void 0, c = e, o = t.apply(s, i);
    }

    function p(t) {
      var i = t - l;
      return void 0 === l || i >= e || i < 0 || h && t - c >= s;
    }

    function v() {
      var t = Ce();
      if (p(t)) return g(t);
      a = setTimeout(v, function (t) {
        var i = e - (t - l);
        return h ? ze(i, s - (t - c)) : i;
      }(t));
    }

    function g(t) {
      return a = void 0, f && r ? d(t) : (r = n = void 0, o);
    }

    function b() {
      var t = Ce(),
          i = p(t);

      if (r = arguments, n = this, l = t, i) {
        if (void 0 === a) return function (t) {
          return c = t, a = setTimeout(v, e), u ? d(t) : o;
        }(l);
        if (h) return a = setTimeout(v, e), d(l);
      }

      return void 0 === a && (a = setTimeout(v, e)), o;
    }

    return e = Be(e) || 0, De(i) && (u = !!i.leading, s = (h = "maxWait" in i) ? Ne(Be(i.maxWait) || 0, e) : s, f = "trailing" in i ? !!i.trailing : f), b.cancel = function () {
      void 0 !== a && clearTimeout(a), c = 0, r = l = n = a = void 0;
    }, b.flush = function () {
      return void 0 === a ? o : g(Ce());
    }, b;
  }

  function De(t) {
    var e = _typeof(t);

    return !!t && ("object" == e || "function" == e);
  }

  function Be(t) {
    if ("number" == typeof t) return t;
    if (function (t) {
      return "symbol" == _typeof(t) || function (t) {
        return !!t && "object" == _typeof(t);
      }(t) && Re.call(t) == _e;
    }(t)) return Oe;

    if (De(t)) {
      var e = "function" == typeof t.valueOf ? t.valueOf() : t;
      t = De(e) ? e + "" : e;
    }

    if ("string" != typeof t) return 0 === t ? t : +t;
    t = t.replace(Se, "");
    var i = Le.test(t);
    return i || Me.test(t) ? ke(t.slice(2), i ? 2 : 8) : Ae.test(t) ? Oe : +t;
  }

  var Ie = function Ie(t, e, i) {
    var r = !0,
        n = !0;
    if ("function" != typeof t) throw new TypeError(we);
    return De(i) && (r = "leading" in i ? !!i.leading : r, n = "trailing" in i ? !!i.trailing : n), Ve(t, e, {
      leading: r,
      maxWait: e,
      trailing: n
    });
  },
      Pe = "Expected a function",
      He = NaN,
      qe = "[object Symbol]",
      Fe = /^\s+|\s+$/g,
      $e = /^[-+]0x[0-9a-f]+$/i,
      Xe = /^0b[01]+$/i,
      Ye = /^0o[0-7]+$/i,
      Ge = parseInt,
      Ke = "object" == _typeof(v) && v && v.Object === Object && v,
      Ue = "object" == (typeof self === "undefined" ? "undefined" : _typeof(self)) && self && self.Object === Object && self,
      Je = Ke || Ue || Function("return this")(),
      Qe = Object.prototype.toString,
      Ze = Math.max,
      ti = Math.min,
      ei = function ei() {
    return Je.Date.now();
  };

  function ii(t) {
    var e = _typeof(t);

    return !!t && ("object" == e || "function" == e);
  }

  function ri(t) {
    if ("number" == typeof t) return t;
    if (function (t) {
      return "symbol" == _typeof(t) || function (t) {
        return !!t && "object" == _typeof(t);
      }(t) && Qe.call(t) == qe;
    }(t)) return He;

    if (ii(t)) {
      var e = "function" == typeof t.valueOf ? t.valueOf() : t;
      t = ii(e) ? e + "" : e;
    }

    if ("string" != typeof t) return 0 === t ? t : +t;
    t = t.replace(Fe, "");
    var i = Xe.test(t);
    return i || Ye.test(t) ? Ge(t.slice(2), i ? 2 : 8) : $e.test(t) ? He : +t;
  }

  var ni = function ni(t, e, i) {
    var r,
        n,
        s,
        o,
        a,
        l,
        c = 0,
        u = !1,
        h = !1,
        f = !0;
    if ("function" != typeof t) throw new TypeError(Pe);

    function d(e) {
      var i = r,
          s = n;
      return r = n = void 0, c = e, o = t.apply(s, i);
    }

    function p(t) {
      var i = t - l;
      return void 0 === l || i >= e || i < 0 || h && t - c >= s;
    }

    function v() {
      var t = ei();
      if (p(t)) return g(t);
      a = setTimeout(v, function (t) {
        var i = e - (t - l);
        return h ? ti(i, s - (t - c)) : i;
      }(t));
    }

    function g(t) {
      return a = void 0, f && r ? d(t) : (r = n = void 0, o);
    }

    function b() {
      var t = ei(),
          i = p(t);

      if (r = arguments, n = this, l = t, i) {
        if (void 0 === a) return function (t) {
          return c = t, a = setTimeout(v, e), u ? d(t) : o;
        }(l);
        if (h) return a = setTimeout(v, e), d(l);
      }

      return void 0 === a && (a = setTimeout(v, e)), o;
    }

    return e = ri(e) || 0, ii(i) && (u = !!i.leading, s = (h = "maxWait" in i) ? Ze(ri(i.maxWait) || 0, e) : s, f = "trailing" in i ? !!i.trailing : f), b.cancel = function () {
      void 0 !== a && clearTimeout(a), c = 0, r = l = n = a = void 0;
    }, b.flush = function () {
      return void 0 === a ? o : g(ei());
    }, b;
  },
      si = "Expected a function",
      oi = "__lodash_hash_undefined__",
      ai = "[object Function]",
      li = "[object GeneratorFunction]",
      ci = /^\[object .+?Constructor\]$/,
      ui = "object" == _typeof(v) && v && v.Object === Object && v,
      hi = "object" == (typeof self === "undefined" ? "undefined" : _typeof(self)) && self && self.Object === Object && self,
      fi = ui || hi || Function("return this")();

  var di = Array.prototype,
      pi = Function.prototype,
      vi = Object.prototype,
      gi = fi["__core-js_shared__"],
      bi = function () {
    var t = /[^.]+$/.exec(gi && gi.keys && gi.keys.IE_PROTO || "");
    return t ? "Symbol(src)_1." + t : "";
  }(),
      mi = pi.toString,
      yi = vi.hasOwnProperty,
      xi = vi.toString,
      Ei = RegExp("^" + mi.call(yi).replace(/[\\^$.*+?()[\]{}|]/g, "\\$&").replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, "$1.*?") + "$"),
      wi = di.splice,
      Oi = Ti(fi, "Map"),
      _i = Ti(Object, "create");

  function Si(t) {
    var e = -1,
        i = t ? t.length : 0;

    for (this.clear(); ++e < i;) {
      var r = t[e];
      this.set(r[0], r[1]);
    }
  }

  function Ai(t) {
    var e = -1,
        i = t ? t.length : 0;

    for (this.clear(); ++e < i;) {
      var r = t[e];
      this.set(r[0], r[1]);
    }
  }

  function Li(t) {
    var e = -1,
        i = t ? t.length : 0;

    for (this.clear(); ++e < i;) {
      var r = t[e];
      this.set(r[0], r[1]);
    }
  }

  function Mi(t, e) {
    for (var i, r, n = t.length; n--;) {
      if ((i = t[n][0]) === (r = e) || i != i && r != r) return n;
    }

    return -1;
  }

  function ki(t) {
    return !(!Ri(t) || (e = t, bi && bi in e)) && (function (t) {
      var e = Ri(t) ? xi.call(t) : "";
      return e == ai || e == li;
    }(t) || function (t) {
      var e = !1;
      if (null != t && "function" != typeof t.toString) try {
        e = !!(t + "");
      } catch (t) {}
      return e;
    }(t) ? Ei : ci).test(function (t) {
      if (null != t) {
        try {
          return mi.call(t);
        } catch (t) {}

        try {
          return t + "";
        } catch (t) {}
      }

      return "";
    }(t));
    var e;
  }

  function Wi(t, e) {
    var i,
        r,
        n = t.__data__;
    return ("string" == (r = _typeof(i = e)) || "number" == r || "symbol" == r || "boolean" == r ? "__proto__" !== i : null === i) ? n["string" == typeof e ? "string" : "hash"] : n.map;
  }

  function Ti(t, e) {
    var i = function (t, e) {
      return null == t ? void 0 : t[e];
    }(t, e);

    return ki(i) ? i : void 0;
  }

  function ji(t, e) {
    if ("function" != typeof t || e && "function" != typeof e) throw new TypeError(si);

    var i = function i() {
      var r = arguments,
          n = e ? e.apply(this, r) : r[0],
          s = i.cache;
      if (s.has(n)) return s.get(n);
      var o = t.apply(this, r);
      return i.cache = s.set(n, o), o;
    };

    return i.cache = new (ji.Cache || Li)(), i;
  }

  function Ri(t) {
    var e = _typeof(t);

    return !!t && ("object" == e || "function" == e);
  }

  Si.prototype.clear = function () {
    this.__data__ = _i ? _i(null) : {};
  }, Si.prototype.delete = function (t) {
    return this.has(t) && delete this.__data__[t];
  }, Si.prototype.get = function (t) {
    var e = this.__data__;

    if (_i) {
      var i = e[t];
      return i === oi ? void 0 : i;
    }

    return yi.call(e, t) ? e[t] : void 0;
  }, Si.prototype.has = function (t) {
    var e = this.__data__;
    return _i ? void 0 !== e[t] : yi.call(e, t);
  }, Si.prototype.set = function (t, e) {
    return this.__data__[t] = _i && void 0 === e ? oi : e, this;
  }, Ai.prototype.clear = function () {
    this.__data__ = [];
  }, Ai.prototype.delete = function (t) {
    var e = this.__data__,
        i = Mi(e, t);
    return !(i < 0 || (i == e.length - 1 ? e.pop() : wi.call(e, i, 1), 0));
  }, Ai.prototype.get = function (t) {
    var e = this.__data__,
        i = Mi(e, t);
    return i < 0 ? void 0 : e[i][1];
  }, Ai.prototype.has = function (t) {
    return Mi(this.__data__, t) > -1;
  }, Ai.prototype.set = function (t, e) {
    var i = this.__data__,
        r = Mi(i, t);
    return r < 0 ? i.push([t, e]) : i[r][1] = e, this;
  }, Li.prototype.clear = function () {
    this.__data__ = {
      hash: new Si(),
      map: new (Oi || Ai)(),
      string: new Si()
    };
  }, Li.prototype.delete = function (t) {
    return Wi(this, t).delete(t);
  }, Li.prototype.get = function (t) {
    return Wi(this, t).get(t);
  }, Li.prototype.has = function (t) {
    return Wi(this, t).has(t);
  }, Li.prototype.set = function (t, e) {
    return Wi(this, t).set(t, e), this;
  }, ji.Cache = Li;

  var Ni = ji,
      zi = function () {
    if ("undefined" != typeof Map) return Map;

    function t(t, e) {
      var i = -1;
      return t.some(function (t, r) {
        return t[0] === e && (i = r, !0);
      }), i;
    }

    return function () {
      function e() {
        this.__entries__ = [];
      }

      return Object.defineProperty(e.prototype, "size", {
        get: function get() {
          return this.__entries__.length;
        },
        enumerable: !0,
        configurable: !0
      }), e.prototype.get = function (e) {
        var i = t(this.__entries__, e),
            r = this.__entries__[i];
        return r && r[1];
      }, e.prototype.set = function (e, i) {
        var r = t(this.__entries__, e);
        ~r ? this.__entries__[r][1] = i : this.__entries__.push([e, i]);
      }, e.prototype.delete = function (e) {
        var i = this.__entries__,
            r = t(i, e);
        ~r && i.splice(r, 1);
      }, e.prototype.has = function (e) {
        return !!~t(this.__entries__, e);
      }, e.prototype.clear = function () {
        this.__entries__.splice(0);
      }, e.prototype.forEach = function (t, e) {
        void 0 === e && (e = null);

        for (var i = 0, r = this.__entries__; i < r.length; i++) {
          var n = r[i];
          t.call(e, n[1], n[0]);
        }
      }, e;
    }();
  }(),
      Ci = "undefined" != typeof window && "undefined" != typeof document && window.document === document,
      Vi = "undefined" != typeof global && global.Math === Math ? global : "undefined" != typeof self && self.Math === Math ? self : "undefined" != typeof window && window.Math === Math ? window : Function("return this")(),
      Di = "function" == typeof requestAnimationFrame ? requestAnimationFrame.bind(Vi) : function (t) {
    return setTimeout(function () {
      return t(Date.now());
    }, 1e3 / 60);
  },
      Bi = 2;

  var Ii = 20,
      Pi = ["top", "right", "bottom", "left", "width", "height", "size", "weight"],
      Hi = "undefined" != typeof MutationObserver,
      qi = function () {
    function t() {
      this.connected_ = !1, this.mutationEventsAdded_ = !1, this.mutationsObserver_ = null, this.observers_ = [], this.onTransitionEnd_ = this.onTransitionEnd_.bind(this), this.refresh = function (t, e) {
        var i = !1,
            r = !1,
            n = 0;

        function s() {
          i && (i = !1, t()), r && a();
        }

        function o() {
          Di(s);
        }

        function a() {
          var t = Date.now();

          if (i) {
            if (t - n < Bi) return;
            r = !0;
          } else i = !0, r = !1, setTimeout(o, e);

          n = t;
        }

        return a;
      }(this.refresh.bind(this), Ii);
    }

    return t.prototype.addObserver = function (t) {
      ~this.observers_.indexOf(t) || this.observers_.push(t), this.connected_ || this.connect_();
    }, t.prototype.removeObserver = function (t) {
      var e = this.observers_,
          i = e.indexOf(t);
      ~i && e.splice(i, 1), !e.length && this.connected_ && this.disconnect_();
    }, t.prototype.refresh = function () {
      this.updateObservers_() && this.refresh();
    }, t.prototype.updateObservers_ = function () {
      var t = this.observers_.filter(function (t) {
        return t.gatherActive(), t.hasActive();
      });
      return t.forEach(function (t) {
        return t.broadcastActive();
      }), t.length > 0;
    }, t.prototype.connect_ = function () {
      Ci && !this.connected_ && (document.addEventListener("transitionend", this.onTransitionEnd_), window.addEventListener("resize", this.refresh), Hi ? (this.mutationsObserver_ = new MutationObserver(this.refresh), this.mutationsObserver_.observe(document, {
        attributes: !0,
        childList: !0,
        characterData: !0,
        subtree: !0
      })) : (document.addEventListener("DOMSubtreeModified", this.refresh), this.mutationEventsAdded_ = !0), this.connected_ = !0);
    }, t.prototype.disconnect_ = function () {
      Ci && this.connected_ && (document.removeEventListener("transitionend", this.onTransitionEnd_), window.removeEventListener("resize", this.refresh), this.mutationsObserver_ && this.mutationsObserver_.disconnect(), this.mutationEventsAdded_ && document.removeEventListener("DOMSubtreeModified", this.refresh), this.mutationsObserver_ = null, this.mutationEventsAdded_ = !1, this.connected_ = !1);
    }, t.prototype.onTransitionEnd_ = function (t) {
      var e = t.propertyName,
          i = void 0 === e ? "" : e;
      Pi.some(function (t) {
        return !!~i.indexOf(t);
      }) && this.refresh();
    }, t.getInstance = function () {
      return this.instance_ || (this.instance_ = new t()), this.instance_;
    }, t.instance_ = null, t;
  }(),
      Fi = function Fi(t, e) {
    for (var i = 0, r = Object.keys(e); i < r.length; i++) {
      var n = r[i];
      Object.defineProperty(t, n, {
        value: e[n],
        enumerable: !1,
        writable: !1,
        configurable: !0
      });
    }

    return t;
  },
      $i = function $i(t) {
    return t && t.ownerDocument && t.ownerDocument.defaultView || Vi;
  },
      Xi = Qi(0, 0, 0, 0);

  function Yi(t) {
    return parseFloat(t) || 0;
  }

  function Gi(t) {
    for (var e = [], i = 1; i < arguments.length; i++) {
      e[i - 1] = arguments[i];
    }

    return e.reduce(function (e, i) {
      return e + Yi(t["border-" + i + "-width"]);
    }, 0);
  }

  function Ki(t) {
    var e = t.clientWidth,
        i = t.clientHeight;
    if (!e && !i) return Xi;

    var r = $i(t).getComputedStyle(t),
        n = function (t) {
      for (var e = {}, i = 0, r = ["top", "right", "bottom", "left"]; i < r.length; i++) {
        var n = r[i],
            s = t["padding-" + n];
        e[n] = Yi(s);
      }

      return e;
    }(r),
        s = n.left + n.right,
        o = n.top + n.bottom,
        a = Yi(r.width),
        l = Yi(r.height);

    if ("border-box" === r.boxSizing && (Math.round(a + s) !== e && (a -= Gi(r, "left", "right") + s), Math.round(l + o) !== i && (l -= Gi(r, "top", "bottom") + o)), !function (t) {
      return t === $i(t).document.documentElement;
    }(t)) {
      var c = Math.round(a + s) - e,
          u = Math.round(l + o) - i;
      1 !== Math.abs(c) && (a -= c), 1 !== Math.abs(u) && (l -= u);
    }

    return Qi(n.left, n.top, a, l);
  }

  var Ui = "undefined" != typeof SVGGraphicsElement ? function (t) {
    return t instanceof $i(t).SVGGraphicsElement;
  } : function (t) {
    return t instanceof $i(t).SVGElement && "function" == typeof t.getBBox;
  };

  function Ji(t) {
    return Ci ? Ui(t) ? function (t) {
      var e = t.getBBox();
      return Qi(0, 0, e.width, e.height);
    }(t) : Ki(t) : Xi;
  }

  function Qi(t, e, i, r) {
    return {
      x: t,
      y: e,
      width: i,
      height: r
    };
  }

  var Zi = function () {
    function t(t) {
      this.broadcastWidth = 0, this.broadcastHeight = 0, this.contentRect_ = Qi(0, 0, 0, 0), this.target = t;
    }

    return t.prototype.isActive = function () {
      var t = Ji(this.target);
      return this.contentRect_ = t, t.width !== this.broadcastWidth || t.height !== this.broadcastHeight;
    }, t.prototype.broadcastRect = function () {
      var t = this.contentRect_;
      return this.broadcastWidth = t.width, this.broadcastHeight = t.height, t;
    }, t;
  }(),
      tr = function () {
    return function (t, e) {
      var i,
          r,
          n,
          s,
          o,
          a,
          l,
          c = (r = (i = e).x, n = i.y, s = i.width, o = i.height, a = "undefined" != typeof DOMRectReadOnly ? DOMRectReadOnly : Object, l = Object.create(a.prototype), Fi(l, {
        x: r,
        y: n,
        width: s,
        height: o,
        top: n,
        right: r + s,
        bottom: o + n,
        left: r
      }), l);
      Fi(this, {
        target: t,
        contentRect: c
      });
    };
  }(),
      er = function () {
    function t(t, e, i) {
      if (this.activeObservations_ = [], this.observations_ = new zi(), "function" != typeof t) throw new TypeError("The callback provided as parameter 1 is not a function.");
      this.callback_ = t, this.controller_ = e, this.callbackCtx_ = i;
    }

    return t.prototype.observe = function (t) {
      if (!arguments.length) throw new TypeError("1 argument required, but only 0 present.");

      if ("undefined" != typeof Element && Element instanceof Object) {
        if (!(t instanceof $i(t).Element)) throw new TypeError('parameter 1 is not of type "Element".');
        var e = this.observations_;
        e.has(t) || (e.set(t, new Zi(t)), this.controller_.addObserver(this), this.controller_.refresh());
      }
    }, t.prototype.unobserve = function (t) {
      if (!arguments.length) throw new TypeError("1 argument required, but only 0 present.");

      if ("undefined" != typeof Element && Element instanceof Object) {
        if (!(t instanceof $i(t).Element)) throw new TypeError('parameter 1 is not of type "Element".');
        var e = this.observations_;
        e.has(t) && (e.delete(t), e.size || this.controller_.removeObserver(this));
      }
    }, t.prototype.disconnect = function () {
      this.clearActive(), this.observations_.clear(), this.controller_.removeObserver(this);
    }, t.prototype.gatherActive = function () {
      var t = this;
      this.clearActive(), this.observations_.forEach(function (e) {
        e.isActive() && t.activeObservations_.push(e);
      });
    }, t.prototype.broadcastActive = function () {
      if (this.hasActive()) {
        var t = this.callbackCtx_,
            e = this.activeObservations_.map(function (t) {
          return new tr(t.target, t.broadcastRect());
        });
        this.callback_.call(t, e, t), this.clearActive();
      }
    }, t.prototype.clearActive = function () {
      this.activeObservations_.splice(0);
    }, t.prototype.hasActive = function () {
      return this.activeObservations_.length > 0;
    }, t;
  }(),
      ir = "undefined" != typeof WeakMap ? new WeakMap() : new zi(),
      rr = function () {
    return function t(e) {
      if (!(this instanceof t)) throw new TypeError("Cannot call a class as a function.");
      if (!arguments.length) throw new TypeError("1 argument required, but only 0 present.");
      var i = qi.getInstance(),
          r = new er(e, i, this);
      ir.set(this, r);
    };
  }();

  ["observe", "unobserve", "disconnect"].forEach(function (t) {
    rr.prototype[t] = function () {
      var e;
      return (e = ir.get(this))[t].apply(e, arguments);
    };
  });

  var nr = void 0 !== Vi.ResizeObserver ? Vi.ResizeObserver : rr,
      sr = !("undefined" == typeof window || !window.document || !window.document.createElement),
      or = function () {
    function t(e, i) {
      var r = this;
      this.onScroll = function () {
        r.scrollXTicking || (window.requestAnimationFrame(r.scrollX), r.scrollXTicking = !0), r.scrollYTicking || (window.requestAnimationFrame(r.scrollY), r.scrollYTicking = !0);
      }, this.scrollX = function () {
        r.axis.x.isOverflowing && (r.showScrollbar("x"), r.positionScrollbar("x")), r.scrollXTicking = !1;
      }, this.scrollY = function () {
        r.axis.y.isOverflowing && (r.showScrollbar("y"), r.positionScrollbar("y")), r.scrollYTicking = !1;
      }, this.onMouseEnter = function () {
        r.showScrollbar("x"), r.showScrollbar("y");
      }, this.onMouseMove = function (t) {
        r.mouseX = t.clientX, r.mouseY = t.clientY, (r.axis.x.isOverflowing || r.axis.x.forceVisible) && r.onMouseMoveForAxis("x"), (r.axis.y.isOverflowing || r.axis.y.forceVisible) && r.onMouseMoveForAxis("y");
      }, this.onMouseLeave = function () {
        r.onMouseMove.cancel(), (r.axis.x.isOverflowing || r.axis.x.forceVisible) && r.onMouseLeaveForAxis("x"), (r.axis.y.isOverflowing || r.axis.y.forceVisible) && r.onMouseLeaveForAxis("y"), r.mouseX = -1, r.mouseY = -1;
      }, this.onWindowResize = function () {
        r.scrollbarWidth = Ee(), r.hideNativeScrollbar();
      }, this.hideScrollbars = function () {
        r.axis.x.track.rect = r.axis.x.track.el.getBoundingClientRect(), r.axis.y.track.rect = r.axis.y.track.el.getBoundingClientRect(), r.isWithinBounds(r.axis.y.track.rect) || (r.axis.y.scrollbar.el.classList.remove(r.classNames.visible), r.axis.y.isVisible = !1), r.isWithinBounds(r.axis.x.track.rect) || (r.axis.x.scrollbar.el.classList.remove(r.classNames.visible), r.axis.x.isVisible = !1);
      }, this.onPointerEvent = function (t) {
        var e, i;
        r.axis.x.scrollbar.rect = r.axis.x.scrollbar.el.getBoundingClientRect(), r.axis.y.scrollbar.rect = r.axis.y.scrollbar.el.getBoundingClientRect(), (r.axis.x.isOverflowing || r.axis.x.forceVisible) && (i = r.isWithinBounds(r.axis.x.scrollbar.rect)), (r.axis.y.isOverflowing || r.axis.y.forceVisible) && (e = r.isWithinBounds(r.axis.y.scrollbar.rect)), (e || i) && (t.preventDefault(), t.stopPropagation(), "mousedown" === t.type && (e && r.onDragStart(t, "y"), i && r.onDragStart(t, "x")));
      }, this.drag = function (e) {
        var i = r.axis[r.draggedAxis].track,
            n = i.rect[r.axis[r.draggedAxis].sizeAttr],
            s = r.axis[r.draggedAxis].scrollbar;
        e.preventDefault(), e.stopPropagation();
        var o = (("y" === r.draggedAxis ? e.pageY : e.pageX) - i.rect[r.axis[r.draggedAxis].offsetAttr] - r.axis[r.draggedAxis].dragOffset) / i.rect[r.axis[r.draggedAxis].sizeAttr] * r.contentWrapperEl[r.axis[r.draggedAxis].scrollSizeAttr];
        "x" === r.draggedAxis && (o = r.isRtl && t.getRtlHelpers().isRtlScrollbarInverted ? o - (n + s.size) : o, o = r.isRtl && t.getRtlHelpers().isRtlScrollingInverted ? -o : o), r.contentWrapperEl[r.axis[r.draggedAxis].scrollOffsetAttr] = o;
      }, this.onEndDrag = function (t) {
        t.preventDefault(), t.stopPropagation(), r.el.classList.remove(r.classNames.dragging), document.removeEventListener("mousemove", r.drag), document.removeEventListener("mouseup", r.onEndDrag);
      }, this.el = e, this.flashTimeout, this.contentEl, this.contentWrapperEl, this.offsetEl, this.maskEl, this.globalObserver, this.mutationObserver, this.resizeObserver, this.scrollbarWidth, this.minScrollbarWidth = 20, this.options = _extends({}, t.defaultOptions, i), this.classNames = _extends({}, t.defaultOptions.classNames, this.options.classNames), this.isRtl, this.axis = {
        x: {
          scrollOffsetAttr: "scrollLeft",
          sizeAttr: "width",
          scrollSizeAttr: "scrollWidth",
          offsetAttr: "left",
          overflowAttr: "overflowX",
          dragOffset: 0,
          isOverflowing: !0,
          isVisible: !1,
          forceVisible: !1,
          track: {},
          scrollbar: {}
        },
        y: {
          scrollOffsetAttr: "scrollTop",
          sizeAttr: "height",
          scrollSizeAttr: "scrollHeight",
          offsetAttr: "top",
          overflowAttr: "overflowY",
          dragOffset: 0,
          isOverflowing: !0,
          isVisible: !1,
          forceVisible: !1,
          track: {},
          scrollbar: {}
        }
      }, this.el.SimpleBar || (this.recalculate = Ie(this.recalculate.bind(this), 64), this.onMouseMove = Ie(this.onMouseMove.bind(this), 64), this.hideScrollbars = ni(this.hideScrollbars.bind(this), this.options.timeout), this.onWindowResize = ni(this.onWindowResize.bind(this), 64, {
        leading: !0
      }), t.getRtlHelpers = Ni(t.getRtlHelpers), this.init());
    }

    t.getRtlHelpers = function () {
      var e = document.createElement("div");
      e.innerHTML = '<div class="hs-dummy-scrollbar-size"><div style="height: 200%; width: 200%; margin: 10px 0;"></div></div>';
      var i = e.firstElementChild;
      document.body.appendChild(i);
      var r = i.firstElementChild;
      i.scrollLeft = 0;
      var n = t.getOffset(i),
          s = t.getOffset(r);
      i.scrollLeft = 999;
      var o = t.getOffset(r);
      return {
        isRtlScrollingInverted: n.left !== s.left && s.left - o.left != 0,
        isRtlScrollbarInverted: n.left !== s.left
      };
    }, t.initHtmlApi = function () {
      this.initDOMLoadedElements = this.initDOMLoadedElements.bind(this), "undefined" != typeof MutationObserver && (this.globalObserver = new MutationObserver(function (e) {
        e.forEach(function (e) {
          Array.prototype.forEach.call(e.addedNodes, function (e) {
            1 === e.nodeType && (e.hasAttribute("data-simplebar") ? !e.SimpleBar && new t(e, t.getElOptions(e)) : Array.prototype.forEach.call(e.querySelectorAll("[data-simplebar]"), function (e) {
              !e.SimpleBar && new t(e, t.getElOptions(e));
            }));
          }), Array.prototype.forEach.call(e.removedNodes, function (t) {
            1 === t.nodeType && (t.hasAttribute("data-simplebar") ? t.SimpleBar && t.SimpleBar.unMount() : Array.prototype.forEach.call(t.querySelectorAll("[data-simplebar]"), function (t) {
              t.SimpleBar && t.SimpleBar.unMount();
            }));
          });
        });
      }), this.globalObserver.observe(document, {
        childList: !0,
        subtree: !0
      })), "complete" === document.readyState || "loading" !== document.readyState && !document.documentElement.doScroll ? window.setTimeout(this.initDOMLoadedElements) : (document.addEventListener("DOMContentLoaded", this.initDOMLoadedElements), window.addEventListener("load", this.initDOMLoadedElements));
    }, t.getElOptions = function (t) {
      return Array.prototype.reduce.call(t.attributes, function (t, e) {
        var i = e.name.match(/data-simplebar-(.+)/);

        if (i) {
          var r = i[1].replace(/\W+(.)/g, function (t, e) {
            return e.toUpperCase();
          });

          switch (e.value) {
            case "true":
              t[r] = !0;
              break;

            case "false":
              t[r] = !1;
              break;

            case void 0:
              t[r] = !0;
              break;

            default:
              t[r] = e.value;
          }
        }

        return t;
      }, {});
    }, t.removeObserver = function () {
      this.globalObserver.disconnect();
    }, t.initDOMLoadedElements = function () {
      document.removeEventListener("DOMContentLoaded", this.initDOMLoadedElements), window.removeEventListener("load", this.initDOMLoadedElements), Array.prototype.forEach.call(document.querySelectorAll("[data-simplebar]"), function (e) {
        e.SimpleBar || new t(e, t.getElOptions(e));
      });
    }, t.getOffset = function (t) {
      var e = t.getBoundingClientRect();
      return {
        top: e.top + (window.pageYOffset || document.documentElement.scrollTop),
        left: e.left + (window.pageXOffset || document.documentElement.scrollLeft)
      };
    };
    var e = t.prototype;
    return e.init = function () {
      this.el.SimpleBar = this, sr && (this.initDOM(), this.scrollbarWidth = Ee(), this.recalculate(), this.initListeners());
    }, e.initDOM = function () {
      var t = this;
      if (Array.prototype.filter.call(this.el.children, function (e) {
        return e.classList.contains(t.classNames.wrapper);
      }).length) this.wrapperEl = this.el.querySelector("." + this.classNames.wrapper), this.contentWrapperEl = this.el.querySelector("." + this.classNames.contentWrapper), this.offsetEl = this.el.querySelector("." + this.classNames.offset), this.maskEl = this.el.querySelector("." + this.classNames.mask), this.contentEl = this.el.querySelector("." + this.classNames.contentEl), this.placeholderEl = this.el.querySelector("." + this.classNames.placeholder), this.heightAutoObserverWrapperEl = this.el.querySelector("." + this.classNames.heightAutoObserverWrapperEl), this.heightAutoObserverEl = this.el.querySelector("." + this.classNames.heightAutoObserverEl), this.axis.x.track.el = this.el.querySelector("." + this.classNames.track + "." + this.classNames.horizontal), this.axis.y.track.el = this.el.querySelector("." + this.classNames.track + "." + this.classNames.vertical);else {
        for (this.wrapperEl = document.createElement("div"), this.contentWrapperEl = document.createElement("div"), this.offsetEl = document.createElement("div"), this.maskEl = document.createElement("div"), this.contentEl = document.createElement("div"), this.placeholderEl = document.createElement("div"), this.heightAutoObserverWrapperEl = document.createElement("div"), this.heightAutoObserverEl = document.createElement("div"), this.wrapperEl.classList.add(this.classNames.wrapper), this.contentWrapperEl.classList.add(this.classNames.contentWrapper), this.offsetEl.classList.add(this.classNames.offset), this.maskEl.classList.add(this.classNames.mask), this.contentEl.classList.add(this.classNames.contentEl), this.placeholderEl.classList.add(this.classNames.placeholder), this.heightAutoObserverWrapperEl.classList.add(this.classNames.heightAutoObserverWrapperEl), this.heightAutoObserverEl.classList.add(this.classNames.heightAutoObserverEl); this.el.firstChild;) {
          this.contentEl.appendChild(this.el.firstChild);
        }

        this.contentWrapperEl.appendChild(this.contentEl), this.offsetEl.appendChild(this.contentWrapperEl), this.maskEl.appendChild(this.offsetEl), this.heightAutoObserverWrapperEl.appendChild(this.heightAutoObserverEl), this.wrapperEl.appendChild(this.heightAutoObserverWrapperEl), this.wrapperEl.appendChild(this.maskEl), this.wrapperEl.appendChild(this.placeholderEl), this.el.appendChild(this.wrapperEl);
      }

      if (!this.axis.x.track.el || !this.axis.y.track.el) {
        var e = document.createElement("div"),
            i = document.createElement("div");
        e.classList.add(this.classNames.track), i.classList.add(this.classNames.scrollbar), e.appendChild(i), this.axis.x.track.el = e.cloneNode(!0), this.axis.x.track.el.classList.add(this.classNames.horizontal), this.axis.y.track.el = e.cloneNode(!0), this.axis.y.track.el.classList.add(this.classNames.vertical), this.el.appendChild(this.axis.x.track.el), this.el.appendChild(this.axis.y.track.el);
      }

      this.axis.x.scrollbar.el = this.axis.x.track.el.querySelector("." + this.classNames.scrollbar), this.axis.y.scrollbar.el = this.axis.y.track.el.querySelector("." + this.classNames.scrollbar), this.options.autoHide || (this.axis.x.scrollbar.el.classList.add(this.classNames.visible), this.axis.y.scrollbar.el.classList.add(this.classNames.visible)), this.el.setAttribute("data-simplebar", "init");
    }, e.initListeners = function () {
      var t = this;
      this.options.autoHide && this.el.addEventListener("mouseenter", this.onMouseEnter), ["mousedown", "click", "dblclick", "touchstart", "touchend", "touchmove"].forEach(function (e) {
        t.el.addEventListener(e, t.onPointerEvent, !0);
      }), this.el.addEventListener("mousemove", this.onMouseMove), this.el.addEventListener("mouseleave", this.onMouseLeave), this.contentWrapperEl.addEventListener("scroll", this.onScroll), window.addEventListener("resize", this.onWindowResize), this.resizeObserver = new nr(this.recalculate), this.resizeObserver.observe(this.el), this.resizeObserver.observe(this.contentEl);
    }, e.recalculate = function () {
      var t = this.heightAutoObserverEl.offsetHeight <= 1,
          e = this.heightAutoObserverEl.offsetWidth <= 1;
      this.elStyles = window.getComputedStyle(this.el), this.isRtl = "rtl" === this.elStyles.direction, this.contentEl.style.padding = this.elStyles.paddingTop + " " + this.elStyles.paddingRight + " " + this.elStyles.paddingBottom + " " + this.elStyles.paddingLeft, this.wrapperEl.style.margin = "-" + this.elStyles.paddingTop + " -" + this.elStyles.paddingRight + " -" + this.elStyles.paddingBottom + " -" + this.elStyles.paddingLeft, this.contentWrapperEl.style.height = t ? "auto" : "100%", this.placeholderEl.style.width = e ? this.contentEl.offsetWidth + "px" : "auto", this.placeholderEl.style.height = this.contentEl.scrollHeight + "px", this.axis.x.isOverflowing = this.contentWrapperEl.scrollWidth > this.contentWrapperEl.offsetWidth, this.axis.y.isOverflowing = this.contentWrapperEl.scrollHeight > this.contentWrapperEl.offsetHeight, this.axis.x.isOverflowing = "hidden" !== this.elStyles.overflowX && this.axis.x.isOverflowing, this.axis.y.isOverflowing = "hidden" !== this.elStyles.overflowY && this.axis.y.isOverflowing, this.axis.x.forceVisible = "x" === this.options.forceVisible || !0 === this.options.forceVisible, this.axis.y.forceVisible = "y" === this.options.forceVisible || !0 === this.options.forceVisible, this.hideNativeScrollbar(), this.axis.x.track.rect = this.axis.x.track.el.getBoundingClientRect(), this.axis.y.track.rect = this.axis.y.track.el.getBoundingClientRect(), this.axis.x.scrollbar.size = this.getScrollbarSize("x"), this.axis.y.scrollbar.size = this.getScrollbarSize("y"), this.axis.x.scrollbar.el.style.width = this.axis.x.scrollbar.size + "px", this.axis.y.scrollbar.el.style.height = this.axis.y.scrollbar.size + "px", this.positionScrollbar("x"), this.positionScrollbar("y"), this.toggleTrackVisibility("x"), this.toggleTrackVisibility("y");
    }, e.getScrollbarSize = function (t) {
      void 0 === t && (t = "y");
      var e,
          i = this.scrollbarWidth ? this.contentWrapperEl[this.axis[t].scrollSizeAttr] : this.contentWrapperEl[this.axis[t].scrollSizeAttr] - this.minScrollbarWidth,
          r = this.axis[t].track.rect[this.axis[t].sizeAttr];

      if (this.axis[t].isOverflowing) {
        var n = r / i;
        return e = Math.max(~~(n * r), this.options.scrollbarMinSize), this.options.scrollbarMaxSize && (e = Math.min(e, this.options.scrollbarMaxSize)), e;
      }
    }, e.positionScrollbar = function (e) {
      void 0 === e && (e = "y");
      var i = this.contentWrapperEl[this.axis[e].scrollSizeAttr],
          r = this.axis[e].track.rect[this.axis[e].sizeAttr],
          n = parseInt(this.elStyles[this.axis[e].sizeAttr], 10),
          s = this.axis[e].scrollbar,
          o = this.contentWrapperEl[this.axis[e].scrollOffsetAttr],
          a = (o = "x" === e && this.isRtl && t.getRtlHelpers().isRtlScrollingInverted ? -o : o) / (i - n),
          l = ~~((r - s.size) * a);
      l = "x" === e && this.isRtl && t.getRtlHelpers().isRtlScrollbarInverted ? l + (r - s.size) : l, s.el.style.transform = "x" === e ? "translate3d(" + l + "px, 0, 0)" : "translate3d(0, " + l + "px, 0)";
    }, e.toggleTrackVisibility = function (t) {
      void 0 === t && (t = "y");
      var e = this.axis[t].track.el,
          i = this.axis[t].scrollbar.el;
      this.axis[t].isOverflowing || this.axis[t].forceVisible ? (e.style.visibility = "visible", this.contentWrapperEl.style[this.axis[t].overflowAttr] = "scroll") : (e.style.visibility = "hidden", this.contentWrapperEl.style[this.axis[t].overflowAttr] = "hidden"), this.axis[t].isOverflowing ? i.style.display = "block" : i.style.display = "none";
    }, e.hideNativeScrollbar = function () {
      if (this.offsetEl.style[this.isRtl ? "left" : "right"] = this.axis.y.isOverflowing || this.axis.y.forceVisible ? "-" + (this.scrollbarWidth || this.minScrollbarWidth) + "px" : 0, this.offsetEl.style.bottom = this.axis.x.isOverflowing || this.axis.x.forceVisible ? "-" + (this.scrollbarWidth || this.minScrollbarWidth) + "px" : 0, !this.scrollbarWidth) {
        var t = [this.isRtl ? "paddingLeft" : "paddingRight"];
        this.contentWrapperEl.style[t] = this.axis.y.isOverflowing || this.axis.y.forceVisible ? this.minScrollbarWidth + "px" : 0, this.contentWrapperEl.style.paddingBottom = this.axis.x.isOverflowing || this.axis.x.forceVisible ? this.minScrollbarWidth + "px" : 0;
      }
    }, e.onMouseMoveForAxis = function (t) {
      void 0 === t && (t = "y"), this.axis[t].track.rect = this.axis[t].track.el.getBoundingClientRect(), this.axis[t].scrollbar.rect = this.axis[t].scrollbar.el.getBoundingClientRect(), this.isWithinBounds(this.axis[t].scrollbar.rect) ? this.axis[t].scrollbar.el.classList.add(this.classNames.hover) : this.axis[t].scrollbar.el.classList.remove(this.classNames.hover), this.isWithinBounds(this.axis[t].track.rect) ? (this.showScrollbar(t), this.axis[t].track.el.classList.add(this.classNames.hover)) : this.axis[t].track.el.classList.remove(this.classNames.hover);
    }, e.onMouseLeaveForAxis = function (t) {
      void 0 === t && (t = "y"), this.axis[t].track.el.classList.remove(this.classNames.hover), this.axis[t].scrollbar.el.classList.remove(this.classNames.hover);
    }, e.showScrollbar = function (t) {
      void 0 === t && (t = "y");
      var e = this.axis[t].scrollbar.el;
      this.axis[t].isVisible || (e.classList.add(this.classNames.visible), this.axis[t].isVisible = !0), this.options.autoHide && this.hideScrollbars();
    }, e.onDragStart = function (t, e) {
      void 0 === e && (e = "y");
      var i = this.axis[e].scrollbar.el,
          r = "y" === e ? t.pageY : t.pageX;
      this.axis[e].dragOffset = r - i.getBoundingClientRect()[this.axis[e].offsetAttr], this.draggedAxis = e, this.el.classList.add(this.classNames.dragging), document.addEventListener("mousemove", this.drag), document.addEventListener("mouseup", this.onEndDrag);
    }, e.getContentElement = function () {
      return this.contentEl;
    }, e.getScrollElement = function () {
      return this.contentWrapperEl;
    }, e.removeListeners = function () {
      var t = this;
      this.options.autoHide && this.el.removeEventListener("mouseenter", this.onMouseEnter), ["mousedown", "click", "dblclick", "touchstart", "touchend", "touchmove"].forEach(function (e) {
        t.el.removeEventListener(e, t.onPointerEvent);
      }), this.el.removeEventListener("mousemove", this.onMouseMove), this.el.removeEventListener("mouseleave", this.onMouseLeave), this.contentWrapperEl.removeEventListener("scroll", this.onScroll), window.removeEventListener("resize", this.onWindowResize), this.mutationObserver && this.mutationObserver.disconnect(), this.resizeObserver.disconnect(), this.recalculate.cancel(), this.onMouseMove.cancel(), this.hideScrollbars.cancel(), this.onWindowResize.cancel();
    }, e.unMount = function () {
      this.removeListeners(), this.el.SimpleBar = null;
    }, e.isChildNode = function (t) {
      return null !== t && (t === this.el || this.isChildNode(t.parentNode));
    }, e.isWithinBounds = function (t) {
      return this.mouseX >= t.left && this.mouseX <= t.left + t.width && this.mouseY >= t.top && this.mouseY <= t.top + t.height;
    }, t;
  }();

  return or.defaultOptions = {
    autoHide: !0,
    forceVisible: !1,
    classNames: {
      contentEl: "simplebar-content",
      contentWrapper: "simplebar-content-wrapper",
      offset: "simplebar-offset",
      mask: "simplebar-mask",
      wrapper: "simplebar-wrapper",
      placeholder: "simplebar-placeholder",
      scrollbar: "simplebar-scrollbar",
      track: "simplebar-track",
      heightAutoObserverWrapperEl: "simplebar-height-auto-observer-wrapper",
      heightAutoObserverEl: "simplebar-height-auto-observer",
      visible: "simplebar-visible",
      horizontal: "simplebar-horizontal",
      vertical: "simplebar-vertical",
      hover: "simplebar-hover",
      dragging: "simplebar-dragging"
    },
    scrollbarMinSize: 25,
    scrollbarMaxSize: 0,
    timeout: 1e3
  }, sr && or.initHtmlApi(), or;
});
"use strict";

if (window.NodeList && !NodeList.prototype.forEach) {
  NodeList.prototype.forEach = Array.prototype.forEach;
}

(function () {
  // проверяем поддержку
  if (!Element.prototype.matches) {
    // определяем свойство
    Element.prototype.matches = Element.prototype.matchesSelector || Element.prototype.webkitMatchesSelector || Element.prototype.mozMatchesSelector || Element.prototype.msMatchesSelector;
  }
})();

(function () {
  // проверяем поддержку
  if (!Element.prototype.closest) {
    // реализуем
    Element.prototype.closest = function (css) {
      var node = this;

      while (node) {
        if (node.matches(css)) return node;else node = node.parentElement;
      }

      return null;
    };
  }
})(); // ie polyphill new Event('event')


(function () {
  if (typeof window.CustomEvent === "function") return false; //If not IE

  function CustomEvent(event, params) {
    params = params || {
      bubbles: false,
      cancelable: false,
      detail: undefined
    };
    var evt = document.createEvent('CustomEvent');
    evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
    return evt;
  }

  CustomEvent.prototype = window.Event.prototype;
  window.CustomEvent = CustomEvent;
})();
"use strict";

(function () {
  var timeout = false,
      html = document.querySelector('html');
  window.addEventListener('resize', function () {
    html.classList.add('disable-anim');
    if (timeout != false) clearTimeout(timeout);
    timeout = setTimeout(function () {
      html.classList.remove('disable-anim');
    }, 1000);
  });
})();
// (function() {
//     var item = document.querySelector('.soc');
//     item.addEventListener('mouseover', () => {
//         item.classList.add('soc--hover');
//     });
//     item.addEventListener('mouseout', () => {
//         item.classList.remove('soc--hover');
//     }); 
// })();
"use strict";
"use strict";

(function () {
  if (window.innerWidth < 768) return;
  var elems = document.querySelectorAll('.drop-down__link');
  if (elems.length < 1) return; // toggle parts

  elems.forEach(function (el) {
    el.addEventListener('mouseover', function () {
      elems.forEach(function (el) {
        el.classList.remove('drop-down__link--active');
      });
      this.classList.add('drop-down__link--active');
    });
  }); // open drop-down menu

  document.querySelector('.list__button').addEventListener('click', function () {
    this.classList.toggle('list__button--open');
    document.querySelector('.list__drop-down').classList.toggle('drop-down--open');
    document.querySelector('menu.menu').classList.toggle('menu--shadow');
  }); // hide the drop-down menu if the press was not on it

  document.addEventListener('click', function (e) {
    if (e.target.closest('.list__button')) return;

    if (!e.target.closest('.list__drop-down') && document.querySelector('.list__drop-down').classList.contains('drop-down--open')) {
      document.querySelector('.list__button').classList.remove('list__button--open');
      document.querySelector('.list__drop-down').classList.remove('drop-down--open');
      document.querySelector('menu.menu').classList.toggle('menu--shadow');
    }
  }); // open desktop search form

  document.querySelector('.menu__search-button').addEventListener('click', function () {
    document.querySelector('.menu__search-form').classList.toggle('search-form--visible');
  });
})(); //mobille


(function () {
  if (window.innerWidth > 767) return;
  document.querySelector('.list__button').addEventListener('click', function () {
    this.classList.toggle('list__button--mob-open');
  });
  document.querySelectorAll('.drop-down__link').forEach(function (el) {
    el.addEventListener('click', function (e) {
      e.preventDefault();
      el.classList.toggle('drop-down__link--mob-open');
    });
  });
  document.querySelector('.header__mobille-button-open-menu').addEventListener('click', function () {
    document.querySelector('.menu').classList.toggle('menu--mob-visible');
    this.classList.toggle('header__mobille-button-open-menu--open');

    if (document.querySelector('.menu').classList.contains('menu--mob-visible')) {
      bodyScrollLock.clearAllBodyScrollLocks();
      bodyScrollLock.disableBodyScroll(document.querySelector('.menu__inner'));
    } else {
      bodyScrollLock.clearAllBodyScrollLocks();
    }
  });
  document.querySelector('.header__mobille-button-search-toggler').addEventListener('click', function () {
    document.querySelector('.header__search-form').classList.toggle('search-form--visible');
    document.querySelector('.page__wrapper').classList.toggle('page__wrapper--search-form-visible');
  }); // menu from footer

  var menuFromFooterItems = document.querySelectorAll('.menu-from-footer__button');
  menuFromFooterItems.forEach(function (el) {
    el.addEventListener('click', function () {
      el.classList.toggle('menu-from-footer__button--open');
    });
  });
})();

document.querySelectorAll('.search-form__close').forEach(function (el) {
  el.addEventListener('click', function (e) {
    e.preventDefault();
    this.closest('.search-form').classList.toggle('search-form--visible');
    document.querySelector('.page__wrapper').classList.toggle('page__wrapper--search-form-visible');
  });
});
"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

// ВАЛИДАЦИЯ ФОРМЫ
var forms = [];

(function () {
  var Form =
  /*#__PURE__*/
  function () {
    function Form(form) {
      var _this = this;

      _classCallCheck(this, Form);

      this.FORM_NODE_INSTANCE = form;

      this.INPUTS = function () {
        var itemsArray = [];

        _this.FORM_NODE_INSTANCE.querySelectorAll('input, select').forEach(function (el, index) {
          itemsArray[index] = {
            node: el,
            name: el.name,
            valid: !(el.dataset.required == 'true' ? true : false),
            alwaysValid: el.dataset.required == 'true' ? false : true
          };

          if (itemsArray[index].name === 'tel') {
            itemsArray[index].imask = new IMask(el, {
              mask: '+{7} (000) 000-00-00',
              lazy: false
            });
          }
        });

        return itemsArray;
      }();

      this.SUBMIT_BUTTON = form.querySelector('button[type="submit"]');
      this.hangEvents();
    }

    _createClass(Form, [{
      key: "hangEvents",
      value: function hangEvents() {
        var _this2 = this;

        this.INPUTS.forEach(function (el) {
          var eventName = el.node.tagName === 'SELECT' ? 'change' : 'keyup';
          el.node.addEventListener(eventName, function () {
            _this2.checkValidElement(el);
          });
        });
      }
    }, {
      key: "checkValidElement",
      value: function checkValidElement(el) {
        // Если это какое-то текстовое поле, и оно не должно быть всегда заполнено(input(requare)) тогда оно всегда валидно
        if (el.alwaysValid == true) {
          if (el.node.value.length > 0) {
            el.node.classList.add('input-field__tag-instance--not-empty');
          } else {
            el.node.classList.remove('input-field__tag-instance--not-empty');
          }

          return;
        }

        if (el.node.tagName === 'SELECT') {
          var valueWasSelected = !el.node.closest('.select').querySelector('.select__label-text').classList.contains('select__label-text--placeholder');

          if (valueWasSelected) {
            el.valid = true;
            el.node.closest('.select').classList.add('select--is-valid');
          } else {
            el.valid = false;
            el.node.closest('.select').classList.remove('select--is-valid');
          }
        } else {
          switch (el.name) {
            case 'tel':
              if (el.node.value == '+7 (___) ___-__-__') {
                el.node.classList.remove('input-field__tag-instance--not-empty');
              } else {
                el.node.classList.add('input-field__tag-instance--not-empty');
              }

              if (el.imask._unmaskedValue.length == 11) {
                el.node.classList.add('input-field__tag-instance--is-valid');
                el.valid = true;
              } else {
                el.node.classList.remove('input-field__tag-instance--is-valid');
                el.valid = false;
              }

              break;

            case 'mail':
              if (validateEmail(el.node.value)) {
                el.valid = true;
                el.node.classList.add('input-field__tag-instance--is-valid');
              } else {
                el.valid = false;
                el.node.classList.remove('input-field__tag-instance--is-valid');
              }

              if (el.node.value.length > 0) {
                el.node.classList.add('input-field__tag-instance--not-empty');
              } else {
                el.node.classList.remove('input-field__tag-instance--not-empty');
              }

              break;

            default:
              if (el.node.value.length > 0) {
                el.valid = true;
                el.node.classList.add('input-field__tag-instance--is-valid');
              } else {
                el.valid = false;
                el.node.classList.remove('input-field__tag-instance--is-valid');
              }

          }
        }

        this.checkFormValidation();
      }
    }, {
      key: "checkFormValidation",
      value: function checkFormValidation(form) {
        var allValid = true;
        this.INPUTS.forEach(function (el) {
          if (el.valid == false) {
            allValid = false;
          }
        });

        if (allValid) {
          // Форма валидна
          this.SUBMIT_BUTTON.classList.add('button--can-be-sent');
          this.SUBMIT_BUTTON.disabled = false; // Сюда можно вписать отправлку формы
        } else {
          // Форма не валидна
          this.SUBMIT_BUTTON.classList.remove('button--can-be-sent');
          this.SUBMIT_BUTTON.disabled = true;
        }
      }
    }]);

    return Form;
  }();

  document.querySelectorAll('form[data-validation="true"]').forEach(function (el, index) {
    forms[index] = new Form(el);
  });

  function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }
})();
"use strict";

(function () {
  // open modals
  var buttons = document.querySelectorAll('[data-toggle_modal]');
  buttons.forEach(function (el) {
    el.addEventListener('click', function (e) {
      e.preventDefault();
      var modal = document.querySelector('#' + el.dataset.toggle_modal);
      modal.style = '';
      setTimeout(function () {
        bodyScrollLock.clearAllBodyScrollLocks();

        if (modal.querySelectorAll('.modal__table-wrapper').length < 1) {
          bodyScrollLock.disableBodyScroll(modal.querySelector('.modal__body'));
        } else if (window.innerWidth > 1023) {
          bodyScrollLock.disableBodyScroll(modal.querySelector('.modal__body'));
        }

        modal.classList.toggle('modal--visible');
      }, 10);
    });
  }); // close modal

  var closeButtons = document.querySelectorAll('.modal__header__close');
  closeButtons.forEach(function (el) {
    el.addEventListener('click', function () {
      bodyScrollLock.clearAllBodyScrollLocks();
      el.closest('.modal').classList.remove('modal--visible');
    });
  }); // background click close modal

  var modals = document.querySelectorAll('.modal');
  modals.forEach(function (el) {
    el.addEventListener('click', function (e) {
      if (e.target == el) {
        bodyScrollLock.clearAllBodyScrollLocks();
        el.classList.remove('modal--visible');
      }
    });
  });
})();
// (function() {
//     var map = document.querySelectorAll('.contacts');
//     if (map.length < 1 || window.innerWidth < 768) return;
//     // append tag
//     var script_tag = document.createElement('script');
//     script_tag.setAttribute('type', 'text/javascript');
//     script_tag.setAttribute('src', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCPlay5BjenmeoWIRe9oPWKFX64SLvqaUQ&callback=initContactsPageGoogleMap');
//     (document.getElementsByTagName('head')[0] || document.documentElement).appendChild(script_tag);
//     // append tag END
//     window.initContactsPageGoogleMap = function() {
//         var scaleble = window.innerWidth / 2;
//         var posMarks = window.externalMarksArray || [{ lat: 55.720158, lng: 37.6084665 }, { lat: 55.750258, lng: 37.6014665 }, { lat: 55.750258, lng: 37.6314665 }];
//         var pos =  window.externalWindowPos || { lat: 55.720158, lng: 37.6084665 };
//         var tag = document.querySelector('.contacts__map');
//         var zoom = 11;
//         var image_marker = {
//             url: window.externalMarkerUrl || 'static/img/general/map.svg',
//             scaledSize: new google.maps.Size(20, 28)
//         };
//         var map = new google.maps.Map(tag, {
//             zoom: zoom,
//             center: pos
//         });
//         var markers = [];
//         posMarks.forEach((el, index) => {
//             markers[index] = new google.maps.Marker({
//                 position: el,
//                 map: map,
//                 icon: image_marker,
//                 optimized: false,
//                 id: 'asdf'
//             });
//         });
//     };
// })();
"use strict";
"use strict";

(function () {
  if (document.querySelectorAll('.write-size__length--left').length < 1) return;
  if (document.querySelectorAll('.write-size__length--right').length < 1) return;
  var inputRowLeft = document.querySelector('.write-size__length--left');
  var inputRowRight = document.querySelector('.write-size__length--right');
  document.querySelector('.select-form__type--single').addEventListener('click', function () {
    inputRowLeft.classList.add('write-size__length--hide');
    inputRowRight.classList.add('write-size__length--hide');
  });
  document.querySelector('.select-form__type--left').addEventListener('click', function () {
    inputRowLeft.classList.remove('write-size__length--hide');
    inputRowRight.classList.add('write-size__length--hide');
  });
  document.querySelector('.select-form__type--right').addEventListener('click', function () {
    inputRowLeft.classList.add('write-size__length--hide');
    inputRowRight.classList.remove('write-size__length--hide');
  });
  document.querySelector('.select-form__type--all').addEventListener('click', function () {
    inputRowLeft.classList.remove('write-size__length--hide');
    inputRowRight.classList.remove('write-size__length--hide');
  });
})();
"use strict";

(function () {
  var slider = document.querySelectorAll('.p-home__slider');
  if (slider.length < 1) return;
  var homeSliderInstance = new Flickity(slider[0], {
    // cellAlign: 'left',
    // contain: true,
    autoPlay: true,
    wrapAround: true // adaptiveHeight: true

  });
  var timeoutID = false;
  window.addEventListener('resize', function () {
    slider[0].classList.add('p-home__slider--moving');
    console.log('resize');
    if (timeoutID != false) clearTimeout(timeoutID);
    timeoutID = setTimeout(function () {
      console.log('homeSliderInstance');
      homeSliderInstance.reloadCells();
      slider[0].classList.remove('p-home__slider--moving');
    }, 1000);
  });
})();

(function () {
  var map = document.querySelectorAll('.p-home__map, .p-saloni__map, .contacts');
  if (map.length < 1 || window.innerWidth < 768) return; // append tag

  var script_tag = document.createElement('script');
  script_tag.setAttribute('type', 'text/javascript');
  script_tag.setAttribute('src', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCPlay5BjenmeoWIRe9oPWKFX64SLvqaUQ&callback=initContactsPageGoogleMap');
  (document.getElementsByTagName('head')[0] || document.documentElement).appendChild(script_tag); // append tag END

  window.initContactsPageGoogleMap = function () {
    var scaleble = window.innerWidth / 2;
    var posMarks = window.externalMarksArray || [{
      lat: 55.720158,
      lng: 37.6084665,
      infoWindow: "<div class=\"info-window\">\n                  <div class=\"info-window__title\">\u0426\u0435\u043D\u0442\u0440\u0430\u043B\u044C\u043D\u044B\u0439 \u0441\u0430\u043B\u043E\u043D \u041C\u043E\u0441\u043A\u0432\u044B \"\u0422\u043E\u0440\u0433\u043E\u0432\u044B\u0439 \u0414\u043E\u043C \u041A\u0443\u0445\u043D\u0438 \u0417\u041E\u0412\"</div>\n                  <div class=\"info-window__adress\">\n                      <svg class=\"info-window__adress-metro\" width=\"18\" height=\"12\" viewBox=\"0 0 18 12\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n                      <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M12.3425 0.166748L8.94784 6.42035L5.5532 0.166748L1.69196 10.3238H0.666016V11.7181H6.04131V10.3238H5.13906L6.25645 7.15543L8.94784 11.8334L11.6392 7.15543L12.7566 10.3238H11.8544V11.7181H17.2297V10.3238H16.2037L12.3425 0.166748Z\" fill=\"#F5203A\"></path></svg>\n                      <div class=\"info-window__adress-text\">\u0423\u043D\u0438\u0432\u0435\u0440\u0441\u0438\u0442\u0435\u0442, \u041B\u0435\u043D\u0438\u043D\u0441\u043A\u0438\u0439 \u043F\u0440\u043E\u0441\u043F\u0435\u043A\u0442 \u0434.83</div>\n                  </div>\n              </div>"
    }, {
      lat: 55.750258,
      lng: 37.6014665,
      infoWindow: "<div class=\"info-window\">\n                  <div class=\"info-window__title\">\u0426\u0435\u043D\u0442\u0440\u0430\u043B\u044C\u043D\u044B\u0439 \u0441\u0430\u043B\u043E\u043D \u041C\u043E\u0441\u043A\u0432\u044B 2</div>\n                  <div class=\"info-window__adress\">\n                      <svg class=\"info-window__adress-metro\" width=\"18\" height=\"12\" viewBox=\"0 0 18 12\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n                      <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M12.3425 0.166748L8.94784 6.42035L5.5532 0.166748L1.69196 10.3238H0.666016V11.7181H6.04131V10.3238H5.13906L6.25645 7.15543L8.94784 11.8334L11.6392 7.15543L12.7566 10.3238H11.8544V11.7181H17.2297V10.3238H16.2037L12.3425 0.166748Z\" fill=\"#F5203A\"></path></svg>\n                      <div class=\"info-window__adress-text\">\u0423\u043D\u0438\u0432\u0435\u0440\u0441\u0438\u0442\u0435\u0442, \u041B\u0435\u043D\u0438\u043D\u0441\u043A\u0438\u0439 \u043F\u0440\u043E\u0441\u043F\u0435\u043A\u0442 \u0434.83</div>\n                  </div>\n              </div>"
    }, {
      lat: 55.750258,
      lng: 37.6314665,
      infoWindow: "<div class=\"info-window\">\n                  <div class=\"info-window__title\">\u0426\u0435\u043D\u0442\u0440\u0430\u043B\u044C\u043D\u044B\u0439 \u0441\u0430\u043B\u043E\u043D \u041C\u043E\u0441\u043A\u0432\u044B 3</div>\n                  <div class=\"info-window__adress\">\n                      <svg class=\"info-window__adress-metro\" width=\"18\" height=\"12\" viewBox=\"0 0 18 12\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n                      <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M12.3425 0.166748L8.94784 6.42035L5.5532 0.166748L1.69196 10.3238H0.666016V11.7181H6.04131V10.3238H5.13906L6.25645 7.15543L8.94784 11.8334L11.6392 7.15543L12.7566 10.3238H11.8544V11.7181H17.2297V10.3238H16.2037L12.3425 0.166748Z\" fill=\"#F5203A\"></path></svg>\n                      <div class=\"info-window__adress-text\">\u0423\u043D\u0438\u0432\u0435\u0440\u0441\u0438\u0442\u0435\u0442, \u041B\u0435\u043D\u0438\u043D\u0441\u043A\u0438\u0439 \u043F\u0440\u043E\u0441\u043F\u0435\u043A\u0442 \u0434.83</div>\n                  </div>\n              </div>"
    }];
    var pos = window.externalWindowPos || {
      lat: 55.720158,
      lng: 37.6084665
    };
    var tag = document.querySelector('.p-home__map-google') || document.querySelector('.p-saloni__map') || document.querySelector('.contacts__map');
    var zoom = 11;
    var image_marker = {
      url: window.externalMarkerUrl || 'static/img/general/map.svg',
      scaledSize: new google.maps.Size(20, 28)
    };
    var map = new google.maps.Map(tag, {
      zoom: zoom,
      center: pos
    });
    var markers = [];
    var infowindows = [];
    posMarks.forEach(function (el, index) {
      markers[index] = new google.maps.Marker({
        position: el,
        map: map,
        icon: image_marker,
        optimized: false,
        id: 'asdf'
      });
      infowindows[index] = new google.maps.InfoWindow({
        content: el.infoWindow
      });
      markers[index].addListener('click', function () {
        infowindows.forEach(function (el) {
          el.close(map, markers[index]);
        });
        infowindows[index].open(map, markers[index]);
      });
    });
  };
})();
"use strict";

// type = file
(function () {
  var items = document.querySelectorAll('.input-file__tag-instance');
  if (items.length < 1) return;
  items.forEach(function (el) {
    el.addEventListener('change', function (e) {
      var name = this.files[0].name.split('.').slice(0, -1).join('.');
      var razr = this.files[0].name.split('.')[this.files[0].name.split('.').length - 1];

      if (name.length > 30) {
        name = name.substring(0, 30);
        name += '..';
      }

      this.closest('label').querySelector('.input-file__text').innerHTML = name + '.' + razr;
      this.closest('.input-file').classList.add('input-file--file-selected');
    });
  });
})();

(function () {
  var items = document.querySelectorAll('.input-file__delete');
  items.forEach(function (el) {
    el.addEventListener('click', function () {
      el.closest('.input-file').classList.remove('input-file--file-selected');
      el.closest('.input-file').querySelector('input').value = '';
      el.closest('.input-file').querySelector('.input-file__text').innerHTML = 'Выбрать файл';
    });
  });
})();
"use strict";

(function () {
  var items = document.querySelectorAll('.product__button');
  if (items.length < 1) return;
  items.forEach(function (el) {
    el.addEventListener('mouseover', function () {
      this.closest('.product').classList.add('product--hover-disable');
    });
  });
  items.forEach(function (el) {
    el.addEventListener('mouseout', function () {
      this.closest('.product').classList.remove('product--hover-disable');
    });
  });
})();
"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var SelectsArray = [];

(function () {
  var Select = function Select(select) {
    var _this = this;

    _classCallCheck(this, Select);

    this.SELECT = select;
    this.SELECT_OPTIONS = this.SELECT.querySelector('.select__options');
    this.SELECT_LABEL = this.SELECT.querySelector('.select__label');
    this.SELECT_LABEL_TEXT = this.SELECT.querySelector('.select__label-text');
    this.SELECT_NODE = this.SELECT.querySelector('select');
    var tempVariable = '';

    for (var index = 0; index < this.SELECT_NODE.length; index++) {
      tempVariable += "<div class=\"select__item\" data-value=\"".concat(this.SELECT_NODE.options[index].value, "\" data-index=\"").concat(index, "\">").concat(this.SELECT_NODE.options[index].text, "</div>");
    }

    this.SELECT_OPTIONS.innerHTML = tempVariable;
    this.SIMPLE_BAR = new SimpleBar(this.SELECT_OPTIONS);
    this.SELECT.addEventListener('click', function (e) {
      if (e.target == _this.SELECT_LABEL) {
        _this.SELECT.classList.toggle('select--open');
      }

      if (e.target.classList.contains('select__item')) {
        _this.SELECT_OPTIONS.querySelectorAll('.select__item--selected').forEach(function (el) {
          el.classList.remove('select__item--selected');
        });

        _this.SELECT_NODE.selectedIndex = e.target.dataset.index;
        e.target.classList.add('select__item--selected');

        _this.SELECT.classList.remove('select--open');

        _this.SELECT_LABEL.classList.remove('select__label--placeholder');

        _this.SELECT_LABEL_TEXT.innerHTML = e.target.innerHTML;
        var event;

        if (typeof Event === 'function') {
          event = new Event('change');
        } else {
          event = document.createEvent('Event');
          event.initEvent('change', true, true);
        }

        _this.SELECT_NODE.dispatchEvent(event);
      }
    });
    this.SELECT_NODE.addEventListener('change', function () {
      _this.SELECT_OPTIONS.querySelectorAll('.select__item--selected').forEach(function (el) {
        el.classList.remove('select__item--selected');
      });

      _this.SELECT_OPTIONS.querySelectorAll('.select__item')[_this.SELECT_NODE.selectedIndex].classList.add('select__item--selected');

      _this.SELECT_LABEL.classList.remove('select__label--placeholder');

      _this.SELECT_LABEL_TEXT.innerHTML = _this.SELECT_OPTIONS.querySelectorAll('.select__item')[_this.SELECT_NODE.selectedIndex].innerHTML; // this.SELECT_NODE.selectedIndex
    });
  };

  document.querySelectorAll('.select').forEach(function (el, index) {
    SelectsArray[index] = new Select(el);
  }); // Закрываем все селекты если клик произошол не по ним

  document.addEventListener('click', function (e) {
    if (!e.target.closest('.select')) {
      document.querySelectorAll('.select').forEach(function (el) {
        el.classList.remove('select--open');
      });
    }
  });
  document.addEventListener('scroll', function (e) {
    var windowHeight = window.innerHeight;
    SelectsArray.forEach(function (el) {
      var pos = el.SELECT.getBoundingClientRect().bottom + el.SELECT_OPTIONS.getBoundingClientRect().height;

      if (windowHeight - pos < 10) {
        el.SELECT.classList.add('select--top');
      } else {
        el.SELECT.classList.remove('select--top');
      }
    });
  });
})();
"use strict";

(function () {
  if (document.querySelectorAll('.filter').length < 1) return;
  var label = document.querySelectorAll('.filter__label');
  label.forEach(function (el) {
    el.addEventListener('click', function () {
      var curModal = el.closest('.filter__item').querySelector('.filter__modal');

      if (curModal.classList.contains('filter__modal--is-show')) {
        document.querySelectorAll('.filter__modal').forEach(function (el) {
          el.classList.remove('filter__modal--is-show');
        });
        document.querySelectorAll('.filter__label').forEach(function (el) {
          el.classList.remove('filter__label--is-show');
        });
      } else {
        document.querySelectorAll('.filter__modal').forEach(function (el) {
          el.classList.remove('filter__modal--is-show');
        });
        curModal.classList.toggle('filter__modal--is-show');
        document.querySelectorAll('.filter__label').forEach(function (el) {
          el.classList.remove('filter__label--is-show');
        });
        el.closest('.filter__item').querySelector('.filter__label').classList.toggle('filter__label--is-show');
      } // if (window.innerWidth > 767) return;

    });
  }); // close all filters if click event was not on modals body

  document.addEventListener('click', function (e) {
    if (e.target.closest('.filter__item') == null) {
      document.querySelectorAll('.filter__modal').forEach(function (el) {
        el.classList.remove('filter__modal--is-show');
      });
      document.querySelectorAll('.filter__label').forEach(function (el) {
        el.classList.remove('filter__label--is-show');
      });
    }
  }); // mobile versition filter togglers

  var mobCloseFilterButton = document.querySelector('.filter__mob-header-close');
  mobCloseFilterButton.addEventListener('click', function () {
    bodyScrollLock.clearAllBodyScrollLocks();
    document.querySelector('.filter').classList.remove('filter--is-show');
  });
  var mobOpenFilterButton = document.querySelector('.p-catalog__shown-mob-filter-button');
  mobOpenFilterButton.addEventListener('click', function () {
    bodyScrollLock.disableBodyScroll(document.querySelector('.filter'));
    document.querySelector('.filter').classList.add('filter--is-show');
  });
})();
"use strict";

(function () {
  if (document.querySelectorAll('.write-size__length-input').length < 1) return;

  function declOfNum(number, titles) {
    var cases = [2, 0, 1, 1, 1, 2];
    return titles[number % 100 > 4 && number % 100 < 20 ? 2 : cases[number % 10 < 5 ? number % 10 : 5]];
  }

  var items = document.querySelectorAll('.write-size__length-input');
  items.forEach(function (el) {
    new IMask(el, {
      mask: Number,
      min: 1,
      max: 20,
      thousandsSeparator: ' '
    }).on('accept', function () {
      el.closest('.write-size__length').querySelector('.write-size__length-text').innerHTML = declOfNum(el.value, ['метр', 'метра', 'метров']);
    });
  });
})();
"use strict";

/// добавляет всем блокам с атрибутом data-background-image фоновое изображение
for (var elements = document.querySelectorAll('[data-background-image]'), i = 0; i < elements.length; i++) {
  elements[i].style.backgroundImage = 'url(' + elements[i].getAttribute('data-background-image') + ')';
}